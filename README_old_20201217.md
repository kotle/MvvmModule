Mvvm库介绍
=
库代码基于androidx和kotlin，因为我用这个库已经做了很多项目，方便项目开发的功能也在慢慢完善，不好用的地方也慢慢摒弃。
[码云](https://gitee.com/kotle/MvvmLib)

支持功能
===

 - mvvm使用起来很方便，高度解耦
 - 基于OkHttp封装了网络框架
 - 以360dp的宽度进行了屏幕适配（基于今日头条方案）
 - 支持右滑返回结束activity
 - 基于统一接口实现的activity，fragment，dialog，BottomSheetDialog，popupWindow子类转换很方便
 - 动态权限请求很方便
 - 有效的防止快速点击按钮
 - ~~支持类似于EventBus的事件传递~~0
 - 封装了websocket和socketIo
 - 封装了下载框架和协程
 - 各种扩展方法和自定义view
 - 支持swagger生成sdk

详细介绍
===
mvvm的使用
=====
建议在项目再封装一层抽象的activity，fragment，dialog，viewModel,Model。
[封装的Demo](https://gitee.com/kotle/MvvmLib/tree/mvvm2/app/src/main/java/com/yizisu/mvvmlib/baselib/base)可以直接拷贝，推荐参考。

**使用Activity**

需要继承[MvvmActivity](https://gitee.com/kotle/MvvmLib/blob/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/mvvm/MvvmActivity.kt)
```kotlin
class BaseActivity : MvvmActivity() {
    /**
     * 最后会被setContent()调用
     */
    override fun getLayoutResOrView(): Any? = FrameLayout(this)

    /**
     * 最后会被setContent()调用
     */
//    override fun getLayoutResOrView(): Any? =R.layout.activity


    /**
     * 在onCreate()中被回调
     * 做一些与viewModel相关的事件
     * 可以注册一些liveData监听
     */
    override fun initViewModel() {
    }

    /**
     * 在onCreate()中被回调
     * 此时已经执行完setContent()方法
     * 做一些与Ui相关的事情
     */
    override fun initUi(savedInstanceState: Bundle?) {
    }

    /**
     * 在onCreate()中被回调
     * 可以处理一些数据
     */
    override fun initData() {
    }

    /**
     * 注册需要点击的view
     * 在onSingleClick(view: View)会被回调
     */
    override fun getClickView(): List<View?>? = null

    /**
     * 在getClickView(): List<View?>?注册的view
     * 点击会在这这个方法回调
     */
    override fun onSingleClick(view: View) {
    }

    /**
     * 是否支持屏幕适配，默认支持
     */
    override fun isNeedScreenAdaptation(): Boolean {
        return super.isNeedScreenAdaptation()
    }

    /**
     * activty是否是全屏，默认不是
     *  这里全屏是支持内容绘制在刘海区的
     */
    override fun isFullScreen(): Boolean {
        return super.isFullScreen()
    }

    /**
     * 是否支持右滑返回结束activty
     */
    override fun isCanSwipeBack(): Boolean {
        return super.isCanSwipeBack()
    }
	
	  /**
     * MessageBus发送的消息会在这里回调
     */
    override fun onMessageBus(event: Any?, code: Int) {
        super.onMessageBus(event, code)
    }

    /**
     * 获取两次view的是点击时间间隔
     * 少于这个时间多次点击只相应一次
     * 默认300毫秒
     */
    override fun getDoubleClickSpace(): Long {
        return super.getDoubleClickSpace()
    }

    /**
     * 这里列举所有可以调用的方法
     */
    fun otherFun() {
        //创建viewModel,可以多次调用，也可以传入不同泛型
        val viewModel = getViewModel<BaseViewModel>()
        //对liveData进行监听，每次赋值这里会回调
        viewModel.testLiveBean.register {
            when (it.status) {
                LiveBeanStatus.START -> {
                    //调用 testLiveBean.start() 这里会回调
                }
                LiveBeanStatus.SUCCESS -> {
                    //调用 testLiveBean.success("这里给liveData赋值") 这里会回调
                }
                LiveBeanStatus.FAIL -> {
                    //调用 testLiveBean.fail("错误信息")这里会回调
                }
            }
        }
        
        //对liveData进行监听，只有调用 success("这里给liveData赋值") 才会回调
        viewModel.testMediatorLiveBean.registerOnSuccess {
            //调用 testMediatorLiveBean.success("这里给liveData赋值") 这里会回调
        }
    }
}
```

**使用Fragment**

需要继承[MvvmFragment](https://gitee.com/kotle/MvvmLib/blob/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/mvvm/MvvmFragment.kt)
```kotlin
class BaseFragment : MvvmFragment() {
    /**
     * 最后会被setContent()调用
     */
    override fun getLayoutResOrView(): Any? = FrameLayout(appCompatActivity!!)

    /**
     * 最后会被setContent()调用
     */
//    override fun getLayoutResOrView(): Any? =R.layout.activity


    /**
     * 在onCreate()中被回调
     * 做一些与viewModel相关的事件
     * 可以注册一些liveData监听
     */
    override fun initViewModel() {
    }

    /**
     * 在onCreate()中被回调
     * 此时已经执行完setContent()方法
     * 做一些与Ui相关的事情
     */
    override fun initUi(savedInstanceState: Bundle?) {
    }

    /**
     * 在onCreate()中被回调
     * 可以处理一些数据
     */
    override fun initData() {
    }

    /**
     * 注册需要点击的view
     * 在onSingleClick(view: View)会被回调
     */
    override fun getClickView(): List<View?>? = null

    /**
     * 在getClickView(): List<View?>?注册的view
     * 点击会在这这个方法回调
     */
    override fun onSingleClick(view: View) {
    }
	
	  /**
     * MessageBus发送的消息会在这里回调
     */
    override fun onMessageBus(event: Any?, code: Int) {
        super.onMessageBus(event, code)
    }

    /**
     * 获取两次view的是点击时间间隔
     * 少于这个时间多次点击只相应一次
     * 默认300毫秒
     */
    override fun getDoubleClickSpace(): Long {
        return super.getDoubleClickSpace()
    }

    /**
     * 这里列举所有可以调用的方法
     */
    fun otherFun() {
        //获取当前的activity
        val currentActivity = appCompatActivity
        
        //获取当前activity创建的viewModel实例，与activity共享
        val activityViewModel = getActivityViewModel<BaseViewModel>()
        
        //创建自己的viewModel,可以多次调用，也可以传入不同泛型
        val viewModel = getViewModel<BaseViewModel>()
        
        //对liveData进行监听，每次赋值这里会回调
        viewModel.testLiveBean.register {
            when (it.status) {
                LiveBeanStatus.START -> {
                    //调用 testLiveBean.start() 这里会回调
                }
                LiveBeanStatus.SUCCESS -> {
                    //调用 testLiveBean.success("这里给liveData赋值") 这里会回调
                }
                LiveBeanStatus.FAIL -> {
                    //调用 testLiveBean.fail("错误信息")这里会回调
                }
            }
        }

        //对liveData进行监听，只有调用 success("这里给liveData赋值") 才会回调
        viewModel.testMediatorLiveBean.registerOnSuccess {
            //调用 testMediatorLiveBean.success("这里给liveData赋值") 这里会回调
        }
    }
}
```
**使用Dialog**

需要继承[MvvmDialog](https://gitee.com/kotle/MvvmLib/blob/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/mvvm/MvvmDialog.kt),因为MvvmDialog是继承AppCompatDialogFragment，所有本质也是一个Fragment，MvvmFragment支持的功能这里都支持。
```kotlin
class BaseDialog : MvvmDialog() {
    /**
     * 最后会被setContent()调用
     */
    override fun getLayoutResOrView(): Any? = FrameLayout(appCompatActivity!!)

    /**
     * 最后会被setContent()调用
     */
//    override fun getLayoutResOrView(): Any? =R.layout.activity


    /**
     * 在onCreate()中被回调
     * 做一些与viewModel相关的事件
     * 可以注册一些liveData监听
     */
    override fun initViewModel() {
    }

    /**
     * 在onCreate()中被回调
     * 此时已经执行完setContent()方法
     * 做一些与Ui相关的事情
     */
    override fun initUi(savedInstanceState: Bundle?) {
    }

    /**
     * 在onCreate()中被回调
     * 可以处理一些数据
     */
    override fun initData() {
    }

    /**
     * 注册需要点击的view
     * 在onSingleClick(view: View)会被回调
     */
    override fun getClickView(): List<View?>? = null

    /**
     * 在getClickView(): List<View?>?注册的view
     * 点击会在这这个方法回调
     */
    override fun onSingleClick(view: View) {
    }

    /**
     * 布局你的view显示参数，可以是位置，大小，内外边距
     */
    override fun onRootViewLayoutParams(lp: FrameLayout.LayoutParams) {
        super.onRootViewLayoutParams(lp)
    }

    /**
     * MessageBus发送的消息会在这里回调
     */
    override fun onMessageBus(event: Any?, code: Int) {
        super.onMessageBus(event, code)
    }

    /**
     * 获取两次view的是点击时间间隔
     * 少于这个时间多次点击只相应一次
     * 默认300毫秒
     */
    override fun getDoubleClickSpace(): Long {
        return super.getDoubleClickSpace()
    }

    /**
     * 设置对话框背景透明度
     * 0-1,默认0.6
     */
    override fun getDialogDimAmount(): Float {
        return super.getDialogDimAmount()
    }

    /**
     * 触摸外面是否可以取消
     * 默认不可以
     */
    override fun isCanceledOnTouchOutside(): Boolean {
        return super.isCanceledOnTouchOutside()
    }

    /**
     * 设置对话框进入退出动画
     */
    @StyleRes
    override fun getDialogAnim(): Int? {
        return super.getDialogAnim()
    }

    /**
     * 是否需要默认,进入退出动画
     * 默认需要
     */
    override fun isNeedDefaultAnim(): Boolean {
        return super.isNeedDefaultAnim()
    }
    
    /**
     * 这里列举所有可以调用的方法
     */
    fun otherFun() {
        //获取当前的activity
        val currentActivity = appCompatActivity

        //获取当前activity创建的viewModel实例，与activity共享
        val activityViewModel = getActivityViewModel<BaseViewModel>()

        //创建自己的viewModel,可以多次调用，也可以传入不同泛型
        val viewModel = getViewModel<BaseViewModel>()

        //对liveData进行监听，每次赋值这里会回调
        viewModel.testLiveBean.register {
            when (it.status) {
                LiveBeanStatus.START -> {
                    //调用 testLiveBean.start() 这里会回调
                }
                LiveBeanStatus.SUCCESS -> {
                    //调用 testLiveBean.success("这里给liveData赋值") 这里会回调
                }
                LiveBeanStatus.FAIL -> {
                    //调用 testLiveBean.fail("错误信息")这里会回调
                }
            }
        }

        //对liveData进行监听，只有调用 success("这里给liveData赋值") 才会回调
        viewModel.testMediatorLiveBean.registerOnSuccess {
            //调用 testMediatorLiveBean.success("这里给liveData赋值") 这里会回调
        }
    }
}
```

**使用ViewModel**

你需要继承[MvvmViewModel](https://gitee.com/kotle/MvvmLib/blob/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/mvvm/MvvmViewModel.kt),封装了几个与网络相关的调用方法，请查看MvvmViewModel源码
```kotlin
class BaseViewModel : MvvmViewModel() {
    /**
     * 创建一个带有状态的liveData，用于与activity/fragment/dialog通讯
     * 具体什么状态需要自己调用方法实现
     */
    val testLiveBean = createLiveBean<String>()

    fun testLiveBeanFun() {
        //支持任何地方调用，已经处理在子线程调用的情况，对应的liveBean状态是LiveBeanStatus.START
        testLiveBean.start()
        //支持任何地方调用，已经处理在子线程调用的情况，对应的liveBean状态是LiveBeanStatus.FAIL
        testLiveBean.fail("错误信息")
        //支持任何地方调用，已经处理在子线程调用的情况，对应的liveBean状态是LiveBeanStatus.SUCCESS
        testLiveBean.success("这里给liveData赋值")
    }

    /**
     * 创建一个可以监听LiveBean的liveData，用于与activity/fragment/dialog通讯
     *  具体什么状态需要自己调用方法实现
     */
    val testMediatorLiveBean = createMediatorLiveBean<String>()

    fun testMediatorLiveBeanFun() {
        //支持任何地方调用，已经处理在子线程调用的情况，对应的liveBean状态是LiveBeanStatus.START
        testMediatorLiveBean.start()
        //支持任何地方调用，已经处理在子线程调用的情况，对应的liveBean状态是LiveBeanStatus.FAIL
        testMediatorLiveBean.fail("错误信息")
        //支持任何地方调用，已经处理在子线程调用的情况，对应的liveBean状态是LiveBeanStatus.SUCCESS
        testMediatorLiveBean.success("这里给liveData赋值")
        //对testLiveBean进行监听，testLiveBean数据发生改变这里会回调
        testMediatorLiveBean.addLiveBeanListener(testLiveBean, Observer {

        })
    }


    /**
     * 这里列举所有可以调用的方法
     */
    fun otherFun() {
        //创建一个model类
        val model = getModel<BaseModel>()
    }
}
```

**使用Model**

需要继承[MvvmModel](https://gitee.com/kotle/MvvmLib/blob/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/mvvm/MvvmModel.kt)，处理数据来源。封装了几个与网络相关的调用方法，请查看MvvmModel源码

至此
mvv的使用已经介绍完毕

---------------------------------------------------------------------------------------------

其它封装工具类介绍
=====
**[MessageBus.Obj](https://gitee.com/kotle/MvvmLib/blob/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/mvvm/mvvm_helper/MessageBus.kt)**

仿照EventBus,进行各个对象之间数据传递

 - **fun register(receiver: MessageBusInterface)** //注册监听
 - **fun unRegister(receiver: MessageBusInterface)** //取消监听
 - **fun post(code: Int, event: Any?, isSticky: Boolean = false)** //发送事件,isSticky 新注册的第一个接口是否接受此事件
 - **fun postLastValue(code: Int, event: Any?)** //每次新注册的对象才会收到回调
 - **fun postLastValue(code: Int, event: Serializable?, isClone: Boolean)** //这个方法可以设置克隆event，如果克隆，注册监听的对象接受到的数据是独立的。否则，所有接受者接受到同一个对象
 - **fun postLastValue(code: Int, event: List<Serializable>?, isClone: Boolean)** //这个方法可以设置克隆event，如果克隆，注册监听的对象接受到的数据是独立的。否则，所有接受者接受到同一个对象
 
 - **fun post(code: Int, event: Any?, isSticky: Boolean = false, vararg mark: Class<*>)** //mark，只有传入的类才可以收到这次数据
 - **fun post(code: Int, event: Serializable?, isClone: Boolean, isSticky: Boolean = false, vararg mark: Class<*>)** //分发数据的时候是否克隆，过滤
 - **fun post(code: Int, event: List<Serializable>?, isClone: Boolean, isSticky: Boolean = false, vararg mark: Class<*>)**  //分发数据的时候是否克隆，过滤
 

**[CoroutineUtil.kt](https://gitee.com/kotle/MvvmLib/blob/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/utils/CoroutineUtil.kt)**

协程工具，推荐使用协调替代线程使用

 - **fun launchThread(start: CoroutineStart = CoroutineStart.DEFAULT,
                 block: suspend CoroutineScope.() -> Unit): Job** //启动一个协程作用域，并且在子线程运行
- **fun launchUi(start: CoroutineStart = CoroutineStart.DEFAULT,
             block: suspend CoroutineScope.() -> Unit): Job**  //启动一个协程作用域，并且在主线程运行


**稍后更新[其他工具类](https://gitee.com/kotle/MvvmLib/tree/mvvm2/basemvvm/src/main/java/com/yizisu/basemvvm/utils)**	 
			 
			 
			 
				 


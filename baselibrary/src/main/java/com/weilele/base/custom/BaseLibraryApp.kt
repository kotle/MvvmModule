package com.weilele.base.custom

import android.app.Application
import android.content.Context
import com.weilele.base.library.BuildConfig
import com.weilele.mvvm.LibraryConf
import com.weilele.mvvm.MvvmConf
import com.weilele.mvvm.utils.local.updateLocal


open class BaseLibraryApp : Application() {
    override fun attachBaseContext(base: Context?) {
        //设置debug
        MvvmConf.isDebug = BuildConfig.DEBUG
        //是否运行错误拦截
        MvvmConf.enableUncaughtExceptionHandler = MvvmConf.isDebug
        //设置只有debug，才打印日志
        LibraryConf.enableLoggerLibrary = MvvmConf.isDebug
        super.attachBaseContext(
            base?.updateLocal(getLanguageTag())
        )
    }

    open fun getLanguageTag(): String? = null
}
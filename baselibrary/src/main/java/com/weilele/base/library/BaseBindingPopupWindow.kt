package com.weilele.base.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding

abstract class BaseBindingPopupWindow<Binding : ViewBinding> : BasePopupWindow(),
    IBindingHelper<Binding> by BindingHelperImpl() {
    override fun onMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Any? {
        return createBinding(this, inflater, container, savedInstanceState)
    }
}
package com.weilele.base.library

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.weilele.mvvm.base.MvvmDialog
import com.weilele.mvvm.base.livedata.LiveDataWrap

abstract class BaseDialog : MvvmDialog(), IBaseHelper,
    IBaseHelperView by BaseHelperImpl() {

    final override fun beforeMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): ViewGroup? {
        return onCreateSwitchView(
            inflater,
            container,
            isNeedToolbar(),
            isReplaceActionBar(),
            isNeedBackIcon(),
            isNeedSwitchView()
        )
    }

    override fun afterMvvmCreateView(
        inflater: LayoutInflater,
        beforeView: ViewGroup?,
        createView: Any?,
        savedInstanceState: Bundle?
    ): Any? {
        return onCreateFinalContentView(beforeView, createView, inflater)
    }

    override fun getObserverLiveData(): List<LiveDataWrap>? {
        return super.getObserverLiveData()
    }

    override fun initUi(savedInstanceState: Bundle?) {
    }

    override fun initData() {
    }


    override fun getClickView(): List<View?>? = null


    override fun onSingleClick(view: View) {

    }

    override fun onRootViewLayoutParams(lp: FrameLayout.LayoutParams) {
        super.onRootViewLayoutParams(lp)
        if (lp.gravity == FrameLayout.LayoutParams.UNSPECIFIED_GRAVITY) {
            lp.gravity = Gravity.CENTER
        }
    }
}
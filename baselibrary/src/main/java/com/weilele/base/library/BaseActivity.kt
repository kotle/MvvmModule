package com.weilele.base.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.weilele.base.library.BaseHelperImpl
import com.weilele.base.library.IBaseHelper
import com.weilele.base.library.IBaseHelperView
import com.weilele.mvvm.base.MvvmActivity
import com.weilele.mvvm.base.livedata.LiveDataWrap

abstract class BaseActivity : MvvmActivity(), IBaseHelper,
    IBaseHelperView by BaseHelperImpl() {

    final override fun beforeMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): ViewGroup? {
        return onCreateSwitchView(
            inflater,
            container,
            isNeedToolbar(),
            isReplaceActionBar(),
            isNeedBackIcon(),
            isNeedSwitchView()
        )
    }

    override fun afterMvvmCreateView(
        inflater: LayoutInflater,
        beforeView: ViewGroup?,
        createView: Any?,
        savedInstanceState: Bundle?
    ): Any? {
        return onCreateFinalContentView(beforeView, createView, inflater)
    }

    override fun isNeedSwitchView(): Boolean {
        return super.isNeedSwitchView()
    }

    override fun isNeedBackIcon(): Boolean {
        return super.isNeedBackIcon()
    }

    override fun isNeedToolbar(): Boolean {
        return super.isNeedToolbar()
    }

    override fun isReplaceActionBar(): Boolean {
        return super.isReplaceActionBar()
    }

    /**
     * 注册需要监听的liveDate对象
     */
    override fun getObserverLiveData(): List<LiveDataWrap>? {
        return super.getObserverLiveData()
    }

    /**
     * 在onCreate()中被回调
     * 此时已经执行完setContent()方法
     * 做一些与Ui相关的事情
     */
    override fun initUi(savedInstanceState: Bundle?) {
    }

    /**
     * 在onCreate()中被回调
     * 可以处理一些数据
     */
    override fun initData() {
    }

    /**
     * 注册需要点击的view
     * 在onSingleClick(view: View)会被回调
     */
    override fun getClickView(): List<View?>? = null

    /**
     * 在getClickView(): List<View?>?注册的view
     * 点击会在这这个方法回调
     */
    override fun onSingleClick(view: View) {
    }

    /**
     * 是否支持屏幕适配，默认支持
     */
    override fun isNeedScreenAdaptation(): Boolean {
        return super.isNeedScreenAdaptation()
    }

    /**
     * 获取两次view的是点击时间间隔
     * 少于这个时间多次点击只相应一次
     * 默认300毫秒
     */
    override fun getDoubleClickSpace(): Long {
        return super.getDoubleClickSpace()
    }
}

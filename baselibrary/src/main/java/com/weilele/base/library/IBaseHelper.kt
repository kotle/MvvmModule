package com.weilele.base.library


interface IBaseHelper {
    /**
     * 是否需要可以切换不同状态的view
     */
    fun isNeedSwitchView(): Boolean = false

    /**
     * 是否需要toolbar
     */
    fun isNeedToolbar(): Boolean = false

    /**
     * 是否用toolbar替换actionbar
     */
    fun isReplaceActionBar(): Boolean = true

    /**
     * 标题栏是否需要返回图标
     * isNeedToolbar()才会被调用
     */
    fun isNeedBackIcon() = true
}
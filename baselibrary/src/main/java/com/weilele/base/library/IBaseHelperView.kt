package com.weilele.base.library

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.LiveData
import com.weilele.mvvm.base.livedata.LiveDataWrap
import com.weilele.mvvm.base.livedata.StatusData
import com.weilele.mvvm.base.livedata.wrapWithSwitchViewObserver
import com.weilele.mvvm.view.SimpleSwitchView

/**
 * 讲switchView与livedata状态绑定的一个例子，可以根据实际情况扩展
 */


interface IBaseHelperView {
    data class BaseHelperBean(
        var rootView: LinearLayout?,
        var switcherView: SwitchView?,
        var toolbar: Toolbar?
    )

    /**
     * 获取根view
     * 如果需要toolbar，则返回LinerLayout
     * 如果需要toolbar，switchView，则返回LinerLayout
     * 如果需要switchView，则返回switchView
     * 如果什么都不需要，则返回传入的view
     */
    fun onCreateSwitchView(
        layoutInflater: LayoutInflater,
        parent: ViewGroup?,
        isNeedToolbar: Boolean,
        replaceActionBar: Boolean,
        isNeedBackIcon: Boolean,
        isNeedSwitchView: Boolean
    ): ViewGroup?

    /**
     * 返回创建的view
     */
    fun onCreateFinalContentView(
        beforeView: ViewGroup?,
        createView: Any?,
        layoutInflater: LayoutInflater,
    ): Any?

    /**
     * 获取view
     */
    fun getBaseHelperBean(): BaseHelperBean

    /**
     * 获取toolbar
     */
    fun getToolbar(): Toolbar?

    /**
     * 获取switchView
     */
    fun getSwitchView(): SwitchView?

    /**
     * 获取根view
     * 当有toolbar的时候才有
     */
    fun getRootView(): LinearLayout?

    /**
     * 显示loading
     */
    fun showLoadingView(info: SimpleSwitchView.LoadingInfo? = null) {
        getBaseHelperBean().switcherView?.showLoadingView(info)
    }

    /**
     * 显示other
     */
    fun showOtherView(
        info: SimpleSwitchView.OtherInfo? = null
    ) {
        getBaseHelperBean().switcherView?.showOtherView(info)
    }

    /**
     * 显示other
     */
    fun showContentView(info: SimpleSwitchView.ContentInfo? = null) {
        getBaseHelperBean().switcherView?.showContentView(info)
    }

    /**
     * 自动处理loading，和other情况
     */
    fun <T> LiveData<StatusData<T>>.wrapSwitchView(
        onGetOtherInfo: Function1<StatusData<T>, SimpleSwitchView.OtherInfo?>? = null/*获取额外的显示信息*/,
        onGetLoadingInfo: Function1<StatusData<T>, SimpleSwitchView.LoadingInfo?>? = null/*获取额外的显示信息*/,
        onNullCall: Function1<StatusData<T>?, Unit>? = null/*当值为其他的时候回调*/,
        interceptSuccess: Function1<@ParameterName("success") StatusData<T>, Unit>
    ): LiveDataWrap {
        return this.wrapWithSwitchViewObserver(
            { getBaseHelperBean().switcherView },
            onGetOtherInfo,
            onGetLoadingInfo,
            onNullCall,
            interceptSuccess
        )
    }
}
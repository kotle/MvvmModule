package com.weilele.base.library.net

import com.weilele.mvvm.setMvvmLibOnCreateOkHttpListener
import com.weilele.mvvm.utils.net.createRetrofitApi
import okhttp3.OkHttpClient

object CommonNet {
    init {
        setMvvmLibOnCreateOkHttpListener(::onInterceptRequest)
    }


    const val BASE_URL = ""

    val api by lazy { createRetrofitApi<Api>(BASE_URL) }

    /**
     * 拦截请求，可以添加统一请求头设置，以及其他操作
     */
    private fun onInterceptRequest(builder: OkHttpClient.Builder) {
        builder.addInterceptor {
            val oldRequest = it.request()
            val newBuild = oldRequest.newBuilder()
            //----------可以添加统一处理 开始---------
            //eg：添加一个请求头
            newBuild.addHeader("android_id", "android_id")
            //----------可以添加统一处理 结束---------
            it.proceed(newBuild.build())
        }
    }
}
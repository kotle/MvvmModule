package com.weilele.base.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.viewbinding.ViewBinding
import com.weilele.mvvm.base.PopupWindowDialog
import com.weilele.mvvm.base.helper.createViewBinding
import java.lang.NullPointerException


/**
 * 如果需要isNeedSwitchView返回true，需要在构造函数处对PopupWindow宽和高重新赋值
 * 这个与PopupWindowDialog区别只是加入了 SwitchView
 */
abstract class BasePopupWindow : PopupWindowDialog(), IBaseHelper,
    IBaseHelperView by BaseHelperImpl() {

    final override fun beforeMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): ViewGroup? {
        return onCreateSwitchView(
            inflater,
            container,
            isNeedToolbar(),
            isReplaceActionBar(),
            isNeedBackIcon(),
            isNeedSwitchView()
        )
    }

    override fun afterMvvmCreateView(
        inflater: LayoutInflater,
        beforeView: ViewGroup?,
        createView: Any?,
        savedInstanceState: Bundle?
    ): Any? {
        return onCreateFinalContentView(beforeView, createView, inflater)
    }

    override fun initUi(savedInstanceState: Bundle?) {

    }

    override fun initData() {
    }


    override fun getClickView(): List<View?>? = null


    override fun onSingleClick(view: View) {

    }

    override fun onRootViewLayoutParams(isAsDropDown: Boolean, lp: FrameLayout.LayoutParams) {
        super.onRootViewLayoutParams(isAsDropDown, lp)
    }
}
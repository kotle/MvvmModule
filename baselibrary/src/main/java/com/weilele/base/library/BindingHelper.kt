package com.weilele.base.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.weilele.mvvm.base.helper.createViewBinding
import java.lang.IllegalArgumentException

interface IBindingHelper<Binding : ViewBinding> {
    val mBinding: Binding

    fun createBinding(
        self: IBindingHelper<Binding>,
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Binding?
}

class BindingHelperImpl<Binding : ViewBinding> : IBindingHelper<Binding> {
    private var _binding: Binding? = null
    override val mBinding: Binding
        get() = _binding ?: throw IllegalArgumentException("您还为初始化Binging")

    override fun createBinding(
        self: IBindingHelper<Binding>,
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Binding? {
        _binding = self.createViewBinding(inflater, null, container, false, 0)
        return _binding
    }
}
package com.weilele.base.library

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.DrawableRes
import com.weilele.base.library.databinding.DefaultEmptyViewBinding
import com.weilele.base.library.databinding.DefaultLoadingViewBinding
import com.weilele.mvvm.utils.activity.invisible
import com.weilele.mvvm.utils.activity.onClick
import com.weilele.mvvm.utils.activity.visible
import com.weilele.mvvm.view.SimpleSwitchView

class SwitchView : SimpleSwitchView {
    class OtherBean(
        val message: String? = null,
        @DrawableRes val drawable: Int? = null,
        val retryText: String? = null,
        val onSingleClick: Function1<View, Unit>? = null
    ) : OtherInfo()

    data class LoadingBean(
        val message: String? = null,
        val isHideContentView: Boolean = false,
        val isEnableContentView: Boolean = false
    ) : LoadingInfo()

    private lateinit var otherViewBinding: DefaultEmptyViewBinding
    private lateinit var loadingViewBinding: DefaultLoadingViewBinding

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context) : super(context)


    override fun getDefaultLoadingView(parent: FrameLayout): View {
        loadingViewBinding =
            DefaultLoadingViewBinding.inflate(LayoutInflater.from(context), parent, false)
        return loadingViewBinding.root
    }

    override fun getDefaultOtherView(parent: FrameLayout): View {
        otherViewBinding =
            DefaultEmptyViewBinding.inflate(LayoutInflater.from(context), parent, true)
        return otherViewBinding.root
    }

    override fun showContentView(info: ContentInfo?) {
        super.showContentView(info)
    }

    override fun showLoadingView(info: LoadingInfo?) {
        super.showLoadingView(info)
        if (info is LoadingBean) {
            loadingViewBinding.loadingMessageTv.text = info.message
        }
    }

    override fun showOtherView(info: OtherInfo?) {
        super.showOtherView(info)
        if (info is OtherBean) {
            otherViewBinding.emptyHintTv.text = info.message
            info.drawable?.let {
                otherViewBinding.emptyHintIv.setImageResource(it)
            }
            otherViewBinding.tvRetry.text = info.retryText
            otherViewBinding.tvRetry.onClick(null, info.onSingleClick)
            otherViewBinding.tvRetry.onClick(null, info.onSingleClick)
            val onSingleClick = info.onSingleClick
            if (onSingleClick == null) {
                otherViewBinding.tvRetry.invisible()
            } else {
                otherViewBinding.tvRetry.visible()
            }
        }
    }
}
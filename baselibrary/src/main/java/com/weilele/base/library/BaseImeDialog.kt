package com.weilele.base.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.weilele.mvvm.base.MvvmImeDialog
import com.weilele.mvvm.base.livedata.LiveDataWrap
import com.weilele.mvvm.utils.activity.dip

abstract class BaseImeDialog : MvvmImeDialog(), IBaseHelper,
    IBaseHelperView by BaseHelperImpl() {

    final override fun beforeMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): ViewGroup? {
        return onCreateSwitchView(
            layoutInflater,
            container,
            isNeedToolbar(),
            isReplaceActionBar(),
            isNeedBackIcon(),
            isNeedSwitchView()
        )
    }

    override fun afterMvvmCreateView(
        inflater: LayoutInflater,
        beforeView: ViewGroup?,
        createView: Any?,
        savedInstanceState: Bundle?
    ): Any? {
        return onCreateFinalContentView(beforeView, createView, inflater)
    }

    override fun getObserverLiveData(): List<LiveDataWrap>? {
        return super.getObserverLiveData()
    }

    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
    }

    override fun initData() {
    }


    override fun getClickView(): List<View?>? = null


    override fun onSingleClick(view: View) {

    }

    override fun onRootViewLayoutParams(lp: FrameLayout.LayoutParams) {
        super.onRootViewLayoutParams(lp)
        lp.marginStart = dip(24)
        lp.marginEnd = dip(24)
    }
}
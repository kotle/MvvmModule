package com.weilele.base.library

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewbinding.ViewBinding
import com.weilele.mvvm.base.helper.SingleClickListener
import com.weilele.mvvm.utils.activity.getResColor
import com.weilele.mvvm.utils.activity.getResDrawable
import com.weilele.mvvm.utils.safeGet
import com.weilele.mvvm.view.SimpleSwitchView
import com.weilele.mvvm.widget.BaseToolBar

class BaseHelperImpl : IBaseHelperView {

    private val baseHelperBean = IBaseHelperView.BaseHelperBean(null, null, null)

    override fun onCreateSwitchView(
        layoutInflater: LayoutInflater,
        parent: ViewGroup?,
        isNeedToolbar: Boolean,
        replaceActionBar: Boolean,
        isNeedBackIcon: Boolean,
        isNeedSwitchView: Boolean
    ): ViewGroup? {
        val context = layoutInflater.context
        val toolbar = getToolbar(context, isNeedToolbar)
        val switcherView = getSwitchView(context, isNeedSwitchView)
        baseHelperBean.toolbar = toolbar
        baseHelperBean.switcherView = switcherView
        return if (toolbar == null) {
            switcherView
        } else {
            val linearLayout = LinearLayout(layoutInflater.context).apply {
                id = View.generateViewId()
                baseHelperBean.rootView = this
                orientation = LinearLayout.VERTICAL
                addView(
                    toolbar, LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                )
                initToolbarView(
                    context as AppCompatActivity,
                    toolbar,
                    replaceActionBar,
                    isNeedBackIcon
                )
            }
            if (switcherView == null) {
                linearLayout
            } else {
                linearLayout.addView(
                    switcherView, LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                )
                switcherView
            }
        }
    }

    override fun onCreateFinalContentView(
        beforeView: ViewGroup?,
        createView: Any?,
        layoutInflater: LayoutInflater,
    ): Any? {
        if (beforeView == null) {
            return createView
        }
        val view = when (createView) {
            is Int -> {
                layoutInflater.inflate(createView, beforeView, false)
            }
            is View -> {
                createView
            }
            is ViewBinding -> {
                createView.root
            }
            else -> {
                null
            }
        }
        if (view != null) {
            if (beforeView is SimpleSwitchView) {
                beforeView.replaceContentView(view)
                beforeView.showContentView(null)
            } else {
                beforeView.addView(view)
            }
        }
        return findParent(beforeView)
    }

    private fun findParent(view: View): View {
        val parent = view.parent.safeGet<View>() ?: return view
        return findParent(parent)
    }

    /**
     * 创建toolbar
     */
    private fun getToolbar(context: Context, isNeedToolbar: Boolean): Toolbar? {
        return if (isNeedToolbar) {
            BaseToolBar(context)
        } else {
            return null
        }
    }


    /**
     * 将toolbar添加到容器
     */
    private fun initToolbarView(
        context: AppCompatActivity,
        toolbar: Toolbar,
        replaceActionBar: Boolean,
        isNeedBackIcon: Boolean,
    ) {
        if (replaceActionBar && context.supportActionBar == null) {
            //没有actionbar
            context.setSupportActionBar(toolbar)
        }
        toolbar.setBackgroundResource(R.color.toolBarBackground)
        if (isNeedBackIcon) {
            val drawable = context.getResDrawable(R.drawable.toolbarLogo)
            drawable?.setTint(context.getResColor(R.color.toolbarLogoTint))
            toolbar.navigationIcon = drawable
            toolbar.title = context.title
            toolbar.popupTheme = R.style.toolbarPopupTheme
            toolbar.setTitleTextAppearance(context, R.style.toolbarTitleTextAppearance)
            toolbar.setNavigationOnClickListener(SingleClickListener {
                context.onBackPressed()
            })
        }
    }


    /**
     * 创建支持根据状态切换的view
     */
    private fun getSwitchView(
        context: Context,
        isNeedSwitchView: Boolean
    ): SwitchView? {
        return if (isNeedSwitchView) {
            SwitchView(context).apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            }
        } else {
            null
        }
    }


    override fun getBaseHelperBean(): IBaseHelperView.BaseHelperBean {
        return baseHelperBean
    }

    override fun getToolbar(): Toolbar? = baseHelperBean.toolbar

    override fun getSwitchView(): SwitchView? = baseHelperBean.switcherView

    override fun getRootView(): LinearLayout? = baseHelperBean.rootView
}
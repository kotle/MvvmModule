# Mvvm库
本库的初衷，更快的开发，更高的性能，更方便的扩展，来应对产品的各种需求，主要以MVVM作为开发模式使用ViewBinding面向Fragment和Holder进行编程，方便复用，扩展和后续的需求变更。
* 全新的RecyclerView框架，更少的代码，更好的复用，更高效的刷新，对于多viewType使用也更方便。对ListAdapter , RecyclerView.Adater ， PagingAdapter(基于Paging3)进行了统一封装
* 全新的网络框架，更少的代码处理网络请求，而且不会内存泄露
* 支持Fragment页面和Activity页面快速互转
* 对Dialog的封装，支持高度自定义的同时避免各种不兼容的场景
* 有状态（Loading/Error/Success）的liveData封装，应对更多场景
* 支持对键盘的兼容控制，避免因为输入导致各种问题
* 默认支持全局防止快速点击
* 默认支持屏幕适配（j基于今日头条方案优化，适配文字），可以一键移除此功能，无侵入式。
* 基于常用View，支持在XML的以属性的方式配置背景，方式灵活，减少drawable文件的书写，更快的写出Material Ui效果
* 支持一行代码处理动态权限请求
* 支持一行代码集成指纹登陆功能
* 支持多语言切换
* 支持一行代码发送通知栏
* 下载框架，支持断点续传和大文件下载
* 大量常用的工具函数和属性托管函数
* 等等......

导入依赖
=====
```groovy
allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```

**最新版本号** [![](https://jitpack.io/v/com.gitee.kotle/MvvmModule.svg)](https://jitpack.io/#com.gitee.kotle/MvvmModule) 

~~~groovy
implementation "com.gitee.kotle:MvvmModule:最新版本号"
~~~



第三方依赖库
=====

为了防止依赖冲突，本库打包时未依赖任何第三方，根据需要导入，如果有用到代码，未导入库，会崩溃。
[本库使用到的所有依赖](https://gitee.com/kotle/MvvmModule/blob/master/dependencieslibrary/build.gradle)

关于配置
=====
Mvvm库依赖的第三方库相关配置[LibraryConf](https://gitee.com/kotle/MvvmModule/blob/master/mvvmlibrary/src/main/java/com/weilele/mvvm/LibraryConf.kt)，如果需要修改配置，必须在[Application]的[attachBaseContext]里面执行，注意：不能在[onCreate]中，因为使用[startup]初始化库的先后顺序不同

[**LibraryConf.kt**](https://gitee.com/kotle/MvvmModule/blob/master/mvvmlibrary/src/main/java/com/weilele/mvvm/LibraryConf.kt)

| 变量名/函数名        | 说明                         |
| -------------------- | ---------------------------- |
| enableMultiLanguages | 是否支持多语言               |
| enableLoggerLibrary  | 是否可以是用第三方日志打印库 |

Mvvm库相关功能配置[MvvmConf](https://gitee.com/kotle/MvvmModule/blob/master/mvvmlibrary/src/main/java/com/weilele/mvvm/MvvmConf.kt)，如果需要修改配置，必须在[Application]的[attachBaseContext]里面执行，注意：不能在[onCreate]中，因为使用[startup]初始化库的先后顺序不同

[**MvvmConf**](https://gitee.com/kotle/MvvmModule/blob/master/mvvmlibrary/src/main/java/com/weilele/mvvm/MvvmConf.kt)

| 变量名/函数名                      | 说明                                                         |
| ---------------------------------- | ------------------------------------------------------------ |
| doubleClickSpace                   | 配置全局默认的两次响应点击的时间间隔                         |
| enableScreenAdaptation             | 屏幕适配，是否开启                                           |
| enableOtherLibraryScreenAdaptation | 屏幕适配是否适配第三方库（没有继承MvvmActivity）             |
| screenAdaptationUiDp               | 屏幕适配，设计尺寸，单位dp  [enableScreenAdaptation]为true才有效 |
| screenAdaptationByWidth            | [screenAdaptationUiDp]设置的尺寸是否是设计稿的短边           |
| isDebug                            | 是否是debug模式                                              |
| enableOkHttpTrustAllCertificates   | 是否信任所有证书,默认不信任，为了传输安全 （必须在创建okhttp之前使用） |
| enableOkHttpCookieJar              | 是否使用cookieJar                                            |
| customOkHttpCookieJar              | 自定义cookie管理                                             |
| okHttpCacheSize                    | 设置okhttp缓存，如果大于0，开启缓存                          |
| channel                            | 设置渠道名                                                   |
| enableScaleFontBySystem            | 设置字体大小是否受系统字体大小的调节影响                     |
| scaledDensityScaleFactor           | 可以设置这个参数，调整字体大小                               |
| enableUncaughtExceptionHandler     | 是否可以自定义异常捕获                                       |



# 使用

## 1.使用RecyclerView

（1）简单使用：本库对adapter进行了封装，使用起来更方便。写这个架构的想法就是，面向Holder编程，因为Holder也是最容易复用的对象。

```kotlin
//1.需要一个继承MvvmHolder<DtatType/*数据类型*/,ViewBinding/*viewBinding*/>的Holder类
class PicHolder : MvvmHolder<String, HolderItemPicBinding> {
    constructor(binding: HolderItemPicBinding) : super(binding)
    constructor(itemView: View) : super(itemView)

    override fun bindData(data:String) {
       //处理自己逻辑
    }
}
//2.创建adapter对象,并且绑定PicHolder
val rcvAdapter=mvvmRcvAdapter(PicHolder::class.java)
//3.设置给recyclerView
recyclerView.adapter=rcvAdapter
//4.刷新数据
rcvAdapter.refreshList(datas)

```

（2）多viewType使用

```kotlin
//每个viewType对应一个MvvmHolder类
class Holder1 : MvvmHolder<Long, ViewBinding> {
    ...
}
class Holder2 : MvvmHolder<Int, ViewBinding> {
    ...
}
//对于多viewType，是根据MvvmHolder的第一个泛型决定的，
//当adapter的列表数据类型和MvvmHolder的泛型对应上，则就是创建该MvvmHolder对象
val adapter=mvvmRcvAdapter<Number>()
adapter.register(Holder1::class.java)
adapter.register(Holder2::class.java)
recyclerView.adapter=adapter
adapter.refreshList(mutableListOf(100,200L))


```

（3）其他类型的adapter

```kotlin
//继承RecyclerView.Adapter，使用diffUtil进行数据刷新，差异的计算在主线程
val adapter=mvvmRcvAdapter()
//继承ListAdapter，在子线程计算差异，计算完毕回调到主线程
val adapter=mvvmListAdapter()
//继承PagingDataAdapter，内部使用Paging3,进行分页处理
val adapter=mvvmPagingAdapter()
```



## 2.关于Mvvm

在**MvvmActivity** ，**MvvmFragment**，**MvvmDialog**封装了许多常用的函数，比如对**startActivityForResult**的优化，**对权限请求**的处理，**防止多次点击**等。还对**ViewModel**和**LiveData**做了许多扩展。

```kotlin
//一个简单的使用例子，从网络加载图片显示在界面上
//至少需要创建3个类（PicHolder/PicFragment/PicViewModel）
class PicHolder : MvvmHolder<String, HolderItemPicBinding> {
    constructor(binding: HolderItemPicBinding) : super(binding)
    constructor(itemView: View) : super(itemView)

    override fun bindData(data:String) {
       //显示图片逻辑
    }
}
//viewModel
class PicViewModel : BaseViewModel() {
    //图片的liveData对象
    val picPageData = createStatusLiveData<String>()

    /**
     * 加载数据
     */
    fun getPic() {
       //从数据库或者网络加载，然后通过picPageData给暴露出去
       picPageData.success(mutableListOf("图片url1","图片url2"))
    }
}
class PicFragment : MvvmFragment() {
    //创建viewModel
    private val viewModel by mvvmViewModel<PicViewModel>()
    //创建适配器
    private val picAdapter= mvvmListAdapter(PicHolder::class.java)
    
    override fun onMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Any? {
        //创建view
        return RecyclerView(requireContext()).apply { 
            adapter=picAdapter
        }
    }

   
    override fun initUi(savedInstanceState: Bundle?) {
        //请求加载图片
        viewModel.getPic()
    }
    
    //统一设置对liveData的监听
    override fun getObserverLiveData(): List<LiveDataWrap>? {
        return listOf(
            viewModel.picPageData wrapSuccessObserver {
                picAdapter.refreshList(it)
            }
        )
    }
}

```



## 3.大量的工具函数

框架也写了大量的工具函数，详细信息如下
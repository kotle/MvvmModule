package com.weilele.mvvmlib

import org.junit.Test

import java.io.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val p = Person()
        p.name = "111"
        val copyObj = copyObj(p)
        println(p)
    }

    class Person : Serializable {
        var name: String? = null
    }

    /**
     * 复制对象
     * 后续测试如果卡顿可以将这段代码放在子线程运行
     */
    private fun copyObj(event: Any?): Any? {
        event ?: return event
        val name = event.javaClass.simpleName
        val out = ObjectOutputStream(FileOutputStream(name))
        out.writeObject(event)
        out.close()
        val ins = ObjectInputStream(FileInputStream(name))
        val newEvent = ins.readObject()
        ins.close()
        return newEvent
    }
}

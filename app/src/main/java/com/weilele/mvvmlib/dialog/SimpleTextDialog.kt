package com.weilele.mvvmlib.dialog

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.weilele.base.library.BaseBindingBottomSheetDialog
import com.weilele.base.library.BaseBindingDialog
import com.weilele.base.library.BaseDialog
import com.weilele.mvvm.utils.activity.onClick
import com.weilele.mvvm.utils.activity.toast
import com.weilele.mvvm.utils.fragment.addOnDismissListenerExt
import com.weilele.mvvmlib.databinding.DialogSimpleTextDialogBinding


fun createSimpleTextDialog(block: (SimpleTextDialog) -> Unit): SimpleTextDialog {
    return SimpleTextDialog().also {
        it.lifecycleScope.launchWhenStarted {
            block.invoke(it)
        }
    }
}

fun AppCompatActivity?.simpleTextDialog(block: (SimpleTextDialog) -> Unit): SimpleTextDialog {
    val dialog = createSimpleTextDialog(block)
    dialog.show(this)
    return dialog
}

fun Fragment?.simpleTextDialog(block: (SimpleTextDialog) -> Unit): SimpleTextDialog {
    val dialog = createSimpleTextDialog(block)
    dialog.show(this)
    return dialog
}

class SimpleTextDialog : BaseBindingBottomSheetDialog<DialogSimpleTextDialogBinding>() {
    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        mBinding.testClick.onClick {
            mBinding.testClick.text.toast()
        }
        addOnDismissListenerExt {
            "对话框隐藏".toast()
        }
    }
}
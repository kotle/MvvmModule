package com.weilele.mvvmlib.dialog

import android.widget.EditText
import com.weilele.base.library.BaseBindingImeDialog
import com.weilele.mvvm.utils.logI
import com.weilele.mvvmlib.databinding.FragmentImeHelperBinding


class ImeDialogTest : BaseBindingImeDialog<FragmentImeHelperBinding>() {

    override fun getFocusViewView(): EditText? {
        return mBinding.et2
    }

    override fun onImeStatusChange(
        offSet: Int,
        imeHeight: Int,
        statusBarHeight: Int,
        navigationBarHeight: Int
    ): Boolean {
        logI { "offSet:$offSet imeHeight:$imeHeight statusBarHeight:$statusBarHeight navigationBarHeight:$navigationBarHeight" }
        return super.onImeStatusChange(offSet, imeHeight, statusBarHeight, navigationBarHeight)
    }
}
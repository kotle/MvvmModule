package com.weilele.mvvmlib

import android.content.Context
import android.content.res.Configuration
import com.weilele.base.custom.BaseLibraryApp
import com.weilele.mvvm.MvvmConf


class App : BaseLibraryApp() {
    override fun onCreate() {
        super.onCreate()

//        setMvvmLibOnCreateOkHttpListener {
//            //设置缓存
//            it.addNetworkInterceptor { chain ->
//                val request: Request = chain.request()
//                var originResponse: Response = chain.proceed(request)
//                //设置响应的缓存时间为60秒，即设置Cache-Control头，并移除pragma消息头，因为pragma也是控制缓存的一个消息头属性
//                originResponse = originResponse.newBuilder()
//                    .removeHeader("pragma")
//                    .header("Cache-Control", "max-age=6000")
//                    .build()
//                originResponse
//            }
//        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MvvmConf.apply {
            okHttpCacheSize = 10 * 1024 * 1204
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        MvvmConf.enableOkHttpTrustAllCertificates = true

    }
}
package com.weilele.mvvmlib.sample

import android.os.Bundle
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.weilele.base.library.BaseBindingActivity
import com.weilele.mvvm.adapter.bindHolder
import com.weilele.mvvm.adapter.mvvmRcvAdapter
import com.weilele.mvvm.base.WebViewHelperActivity
import com.weilele.mvvm.utils.activity.getResString
import com.weilele.mvvm.utils.permission.callPermissions
import com.weilele.mvvmlib.R
import com.weilele.mvvmlib.databinding.ActivityMainBinding
import com.weilele.mvvmlib.databinding.HolderSimpleTextBinding
import com.weilele.mvvmlib.dialog.ImeDialogTest
import com.weilele.mvvmlib.dialog.simpleTextDialog
import com.weilele.mvvmlib.fragment.*
import com.weilele.mvvmlib.fragment.pageing3.Paging3Fragment
import com.weilele.mvvmlib.sample.holder.SimpleTextHolder

class MainActivity : BaseBindingActivity<ActivityMainBinding>() {
    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        initRcv()
    }

    private fun initRcv() {
//        mBinding.vLayout.adapter = SimpleTextHolder.toAdapter().apply {
//            refreshList(getDatas()) {
//                println("数据已经提交")
//            }
//        }
        mBinding.vLayout.adapter = mvvmRcvAdapter<Any>().apply {
            bindHolder<Any, Int, HolderSimpleTextBinding> {
                mBinding.bean = SimpleTextHolder.Bean("Intintt,$it")
            }
            bindHolder<Any, Int, HolderSimpleTextBinding> {
                mBinding.bean = SimpleTextHolder.Bean("Int,$it")
            }
            bindHolder<Any, Long, HolderSimpleTextBinding> {
                mBinding.bean = SimpleTextHolder.Bean("Long,$it")
            }
            bindHolder<Any, SimpleTextHolder.Bean, HolderSimpleTextBinding>(
                onCreateHolder = { layoutInflater, viewGroup ->
                    SimpleTextHolder(
                        HolderSimpleTextBinding.inflate(
                            layoutInflater,
                            viewGroup,
                            false
                        )
                    )
                }
            ) {
                it.text = it.text + "---->"
                mBinding.bean = it
            }
            register(SimpleTextHolder::class.java)
            addItems(
                mutableListOf<Any>(
                    1, 2L, 3L, 4, 5
                )
            )
            addItems(getDatas())
        }
    }

    private fun getDatas(): MutableList<SimpleTextHolder.Bean>? {
        val datas = mutableListOf(
            SimpleTextHolder.Bean("生物识别:${getResString(R.string.hello_blank_fragment)}") {
                BiometricFragment.start(this)
            },
            SimpleTextHolder.Bean("SAF文件") {
                SafFragment.start(this)
            },
            SimpleTextHolder.Bean("测试应用崩溃") {
                throw IllegalArgumentException("这个应用真垃圾，这就崩了")
            },
            SimpleTextHolder.Bean("消息提示对话框") {
                simpleTextDialog { dialog ->
                    dialog.mBinding.tvContent.text = it.text
                }
            },
            SimpleTextHolder.Bean("MainFragment") {
                MainFragment.start(this)
            },
            SimpleTextHolder.Bean("测试vp2和键盘冲突") {
                ViewPage2Fragment.start(this)
            },
            SimpleTextHolder.Bean("键盘对话框") {
                ImeDialogTest().show(this)
            },
            SimpleTextHolder.Bean("MaterialAlertDialogBuilder") {
                MaterialAlertDialogBuilder(this)
                    .setTitle("Title")
                    .setMessage("MaterialAlertDialogBuilder:${this.getResString(R.string.hello_blank_fragment)}")
                    .setIcon(R.mipmap.ic_launcher_round)
                    .setNeutralButton("中性按钮", null)
                    .setNegativeButton("负按钮", null)
                    .setPositiveButton("正按钮", null)
                    .show()
            },
            SimpleTextHolder.Bean("通知栏") {
                NotificationFragment.start(this)
            },
            SimpleTextHolder.Bean("Paging3") {
                Paging3Fragment.start(this)
            },
            SimpleTextHolder.Bean("测试系统下载器") {
                callPermissions(listOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    DownloadFragment.start(this)
                }
            },
            SimpleTextHolder.Bean("测试webView下载文件") {
                WebViewHelperActivity.Builder()
                    .setUrl("https://www.cnblogs.com/china1/p/13194914.html")
                    .setOnWebView {
                        it.get()?.settings?.apply {
                            javaScriptEnabled = true
                            allowFileAccess = true
                            javaScriptCanOpenWindowsAutomatically = true
                        }
                        true
                    }
                    .start(this)
            },
        )
        repeat(20) {
            datas.add(SimpleTextHolder.Bean("占位的Item$it"))
        }
        return datas
    }

    override fun isNeedSwitchView(): Boolean {
        return true
    }
}
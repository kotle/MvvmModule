package com.weilele.mvvmlib.sample.holder

import android.view.View
import com.weilele.mvvm.adapter.MvvmHolder
import com.weilele.mvvm.adapter.mvvmListAdapter
import com.weilele.mvvm.adapter.mvvmPagingAdapter
import com.weilele.mvvm.utils.activity.onClick
import com.weilele.mvvm.utils.activity.scaleAnimByTouchListener
import com.weilele.mvvmlib.databinding.HolderSimpleTextBinding


class PagingTextHolder : MvvmHolder<SimpleTextHolder.Bean, HolderSimpleTextBinding> {
    companion object {
        fun toAdapter() = mvvmListAdapter(PagingTextHolder::class.java)
        fun toPageAdapter() = mvvmPagingAdapter(PagingTextHolder::class.java, { old, new ->
            old == null
        }, { old, new ->
            old?.text == new?.text
        })
    }

    init {
//        itemView.scaleAnimByTouchListener()
    }

    constructor(binding: HolderSimpleTextBinding) : super(binding)
    constructor(itemView: View) : super(itemView)

    override fun bindData(data: SimpleTextHolder.Bean) {
        mBinding.text.text = data.text
    }
}
package com.weilele.mvvmlib.sample.holder

import android.annotation.SuppressLint
import android.view.View
import com.weilele.mvvm.adapter.MvvmHolder
import com.weilele.mvvm.adapter.mvvmListAdapter
import com.weilele.mvvm.adapter.mvvmPagingAdapter
import com.weilele.mvvm.utils.activity.attachBadgeDawable
import com.weilele.mvvm.utils.activity.dp
import com.weilele.mvvm.utils.activity.onClick
import com.weilele.mvvm.utils.activity.scaleAnimByTouchListener
import com.weilele.mvvmlib.databinding.HolderSimpleTextBinding


class SimpleTextHolder : MvvmHolder<SimpleTextHolder.Bean, HolderSimpleTextBinding> {
    companion object {
        fun toAdapter() = mvvmListAdapter(SimpleTextHolder::class.java)
        fun toPageAdapter() = mvvmPagingAdapter(SimpleTextHolder::class.java)
    }

    class Bean(
        var text: String,
        val onclick: Function1<Bean, Unit>? = null
    )

    init {
        itemView.onClick {
            dataWhenBind?.let {
                it.onclick?.invoke(it)
            }
        }
        itemView.scaleAnimByTouchListener()
    }

    constructor(binding: HolderSimpleTextBinding) : super(binding)
    constructor(itemView: View) : super(itemView)

    @SuppressLint("UnsafeOptInUsageError")
    override fun bindData(data: Bean) {
        mBinding.bean = data
//        mBinding.text.text = data.text
        itemView.post {
            itemView.attachBadgeDawable().let {
                it.verticalOffset = 16.dp
                it.maxCharacterCount = 2
                it.setContentDescriptionNumberless("+")
                it.horizontalOffset = 16.dp
                it.number = bindingPosition
            }
        }
    }
}
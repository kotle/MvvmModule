package com.weilele.mvvmlib.sample.holder

import android.view.View
import com.weilele.mvvm.adapter.MvvmHolder
import com.weilele.mvvm.utils.activity.dp
import com.weilele.mvvm.utils.glide.setImageRoundedCorners
import com.weilele.mvvmlib.databinding.HolderItemPicBinding
import com.weilele.mvvmlib.model.PicBean

class PicHolder : MvvmHolder<PicBean.DataDTO, HolderItemPicBinding> {
    constructor(binding: HolderItemPicBinding) : super(binding)
    constructor(itemView: View) : super(itemView)

    override fun bindData(data: PicBean.DataDTO) {
        mBinding.root.setImageRoundedCorners(data.url, 16.dp)!!.clearOnDetach()
    }
}
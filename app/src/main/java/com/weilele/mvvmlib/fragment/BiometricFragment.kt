package com.weilele.mvvmlib.fragment


import android.os.Bundle
import android.util.Base64
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.weilele.base.library.BaseBindingFragment
import com.weilele.base.library.FragmentContainerActivity
import com.weilele.delegate.mmkvStorage
import com.weilele.mvvm.adapter.refreshList
import com.weilele.mvvm.base.livedata.appCompatActivity
import com.weilele.mvvm.utils.activity.getResString
import com.weilele.mvvm.utils.activity.toast
import com.weilele.mvvm.utils.biometric.decrypt
import com.weilele.mvvm.utils.biometric.encrypt
import com.weilele.mvvm.utils.biometric.startBiometric
import com.weilele.mvvmlib.R
import com.weilele.mvvmlib.databinding.FragmentBiometricBinding
import com.weilele.mvvmlib.sample.holder.SimpleTextHolder


class BiometricFragment : BaseBindingFragment<FragmentBiometricBinding>() {
    companion object {
        private const val KEY_ALIAS = "DEFAULT_KEY_2"
        fun start(appCompatActivity: AppCompatActivity) {
            FragmentContainerActivity.invoke<BiometricFragment>(appCompatActivity)
        }
    }

    init {
        lifecycleScope.launchWhenCreated {
            println("launchWhenCreated")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private var password: String? by mmkvStorage("password", "")
    private var iv by mmkvStorage("iv", "")
    override fun initUi(savedInstanceState: Bundle?) {
        println("initUi")
        super.initUi(savedInstanceState)
        mBinding.rcv.adapter = SimpleTextHolder.toAdapter().apply {
            refreshList(
                mutableListOf(
                    SimpleTextHolder.Bean("生物识别加密${context.getResString(R.string.hello_blank_fragment)}") {
                        //每次加密过后，iv都会发生变化，解密必须是用最新的iv
                        appCompatActivity?.startBiometric(null, KEY_ALIAS) { result, error ->
                            result?.cryptoObject?.cipher?.let {
                                iv = Base64.encodeToString(it.iv, Base64.URL_SAFE)
                                password = result.encrypt("我是加密的信息")
                            }
                            error?.message?.toast()
                        }
                    },
                    SimpleTextHolder.Bean("生物识别解密") {
                        appCompatActivity?.startBiometric(
                            Base64.decode(iv, Base64.URL_SAFE),
                            KEY_ALIAS
                        ) { result, error ->
                            error?.message?.toast()
                            result?.decrypt(password)?.toast()
                        }
                    },
                ).apply {
                    repeat(100) {
                        add(SimpleTextHolder.Bean("$it"))
                    }
                }
            )
        }
    }

    override fun onBackPressed(): OnBackPressedCallback = backPressed {
        appCompatActivity?.finishAfterTransition()
    }
}
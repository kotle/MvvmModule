package com.weilele.mvvmlib.fragment


import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import com.arialyy.aria.core.task.DownloadGroupTask
import com.arialyy.aria.core.task.DownloadTask
import com.weilele.base.library.BaseBindingFragment
import com.weilele.base.library.FragmentContainerActivity
import com.weilele.delegate.unsafeLazy
import com.weilele.mvvm.utils.activity.onClick
import com.weilele.mvvm.utils.activity.toast
import com.weilele.mvvm.utils.aria.download.MvvmDownload
import com.weilele.mvvm.utils.aria.download_group.MvvmDownloadGroup
import com.weilele.mvvm.utils.logI
import com.weilele.mvvmlib.databinding.FragmentDownloadBinding
import java.lang.Exception


class DownloadFragment : BaseBindingFragment<FragmentDownloadBinding>() {
    companion object {
        private const val KEY_ALIAS = "DEFAULT_KEY_2"
        fun start(appCompatActivity: AppCompatActivity) {
            FragmentContainerActivity.invoke<DownloadFragment>(appCompatActivity)
        }
    }

    private val url =
        mutableListOf("http://cdn.sonyselect.com.cn/Hi-Res/addition/1.1.4.01624701431036.exe")

    private val download by unsafeLazy {
        MvvmDownloadGroup
        MvvmDownload
    }

    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        download.updateConfig {
            it.isConvertSpeed = true
        }
        var id: Long? = null
        mBinding.start.onClick {
            download.removeListener(this, id)
            id = download.startOrResume(
                url.first(),
                requireContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)!!.absolutePath + "wwwwww.apk"
            ) {
//                it.setGroupAlias("王者荣耀")
//                it.setSubFileName(mutableListOf("王者荣耀222.apk"))
            }
            download.addListener(this, object : MvvmDownload.LifecycleListener(id!!) {

                override fun onTaskResume(task: DownloadTask) {
                    super.onTaskResume(task)
                    "继续下载".toast()

                }

                override fun onTaskCancel(task: DownloadTask) {
                    super.onTaskCancel(task)
                    "取消或者删除下载".toast()
                }

                override fun onWait(task: DownloadTask) {
                    super.onWait(task)
                }

                override fun onPre(task: DownloadTask) {
                    super.onPre(task)
                }

                override fun onTaskPre(task: DownloadTask) {
                    super.onTaskPre(task)
                }

                override fun onTaskStart(task: DownloadTask) {
                    super.onTaskStart(task)
                    "开始下载".toast()
                }

                override fun onTaskStop(task: DownloadTask) {
                    super.onTaskStop(task)
                    "暂停下载".toast()
                }

                override fun onTaskFail(task: DownloadTask, e: Exception?) {
                    super.onTaskFail(task, e)
                    "下载出错：${e?.message}"
                }

                override fun onTaskComplete(task: DownloadTask) {
                    super.onTaskComplete(task)
                    "下载完成".toast()
                    onTaskRunning(task)
                }

                override fun onTaskRunning(task: DownloadTask) {
                    super.onTaskRunning(task)
                    logI { "onTaskRunning--->${task.percent}" }
                    mBinding.progress.progressText =
                        "进度：${task.entity.percent}%  速度：${task.convertSpeed}  大小：${task.convertCurrentProgress}"
                    mBinding.progress.progress = task.entity.percent / 100f
                }

                override fun onNoSupportBreakPoint(task: DownloadTask) {
                    super.onNoSupportBreakPoint(task)
                    "本次下载不支持断点".toast()
                }
            })
        }
        mBinding.delete.onClick {
            download.deleteAllTask(true)
        }
        mBinding.resume.onClick {
            id?.let {
                download.resume(it)
            }
        }
        mBinding.stop.onClick {
            id?.let {
                download.pause(it)
            }
        }
    }
}
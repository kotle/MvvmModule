package com.weilele.mvvmlib.fragment.pageing3


import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.paging.PagingData
import androidx.recyclerview.widget.RecyclerView
import com.weilele.base.library.BaseBindingFragment
import com.weilele.base.library.FragmentContainerActivity
import com.weilele.delegate.mvvmViewModel
import com.weilele.mvvm.adapter.bindPagingAdapter
import com.weilele.mvvmlib.databinding.FragmentPaging3Binding
import com.weilele.mvvmlib.sample.holder.PagingTextHolder

class Paging3Fragment : BaseBindingFragment<FragmentPaging3Binding>() {
    companion object {
        fun start(appCompatActivity: AppCompatActivity) {
            FragmentContainerActivity.invoke<Paging3Fragment>(appCompatActivity)
        }
    }

    private val adapter = PagingTextHolder.toPageAdapter()
    private val viewModel by mvvmViewModel<Paging3ViewModel>()

    override fun initData() {
        super.initData()
        getSwitchView()?.apply {
            isCanSwipeRefresh = true
            setOnRefreshListener {
                adapter.refresh()
            }
        }
        val head = object : LoadStateAdapter<RecyclerView.ViewHolder>() {
            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, loadState: LoadState) {
            }

            override fun onCreateViewHolder(
                parent: ViewGroup,
                loadState: LoadState
            ): RecyclerView.ViewHolder {
                return object : RecyclerView.ViewHolder(
                    if (loadState.endOfPaginationReached) {
                        Button(parent.context)
                    } else {
                        ProgressBar(parent.context)
                    }
                ) {

                }
            }

            override fun displayLoadStateAsItem(loadState: LoadState): Boolean {
                return true
            }
        }
        adapter.setOnHolderClickListener { holder, clickView ->
            val newList = adapter.snapshot().items.toMutableList()
            newList.removeAt(holder.bindingPosition)
            adapter.submitData(this.lifecycle, PagingData.from(newList))
        }
        val foot = object : LoadStateAdapter<RecyclerView.ViewHolder>() {
            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, loadState: LoadState) {
            }

            override fun onCreateViewHolder(
                parent: ViewGroup,
                loadState: LoadState
            ): RecyclerView.ViewHolder {
                return object : RecyclerView.ViewHolder(
                    if (loadState.endOfPaginationReached) {
                        Button(parent.context)
                    } else {
                        ProgressBar(parent.context)
                    }
                ) {

                }
            }

            override fun getStateViewType(loadState: LoadState): Int {
                return if (loadState.endOfPaginationReached) {
                    0
                } else {
                    1
                }
            }

            override fun displayLoadStateAsItem(loadState: LoadState): Boolean {
                return true
            }
        }
        mBinding.root.adapter = adapter.withLoadStateHeaderAndFooter(head, foot)
        adapter.addLoadStateListener {
            if (it.refresh != LoadState.Loading) {
                getSwitchView()?.stopRefresh()
            }
        }

        adapter.addLoadStateListener {
            if (it.refresh != LoadState.Loading) {
                getSwitchView()?.stopRefresh()
            }
        }
        bindPagingAdapter(adapter, viewModel.pagingData2)
    }

    override fun isNeedSwitchView(): Boolean {
        return true
    }
}
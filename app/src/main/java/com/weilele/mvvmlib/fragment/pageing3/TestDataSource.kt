package com.weilele.mvvmlib.fragment.pageing3

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.weilele.mvvmlib.sample.holder.SimpleTextHolder

class TestDataSource(private val initKey: Int) : PagingSource<Int, SimpleTextHolder.Bean>() {

    override fun getRefreshKey(state: PagingState<Int, SimpleTextHolder.Bean>): Int? {
        return initKey
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, SimpleTextHolder.Bean> {
        val page = params.key ?: initKey
        val size = params.loadSize
        val start = page * size + 1
        return LoadResult.Page(mutableListOf<SimpleTextHolder.Bean>().apply {
            repeat(size) {
                add(SimpleTextHolder.Bean("${start + it}"))
            }
        }, if (page == 0) null else page - 1, if (page == 3) null else page + 1)
    }

    override val jumpingSupported: Boolean
        get() = true
}
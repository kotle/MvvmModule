package com.weilele.mvvmlib.fragment


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.weilele.base.library.BaseBindingFragment
import com.weilele.base.library.FragmentContainerActivity
import com.weilele.mvvm.utils.view_page.simplePagerAdapter
import com.weilele.mvvmlib.databinding.FragmentViewPage2Binding

/**
 * vp2默认缓存三个已经加载过的页面
 *
 * offscreenPageLimit默认0，初始化值加载当前页面
 * eg:offscreenPageLimit默认0设置为1，加载前后各一个页面，只有当前的走onResume()
 */
class ViewPage2Fragment : BaseBindingFragment<FragmentViewPage2Binding>() {
    companion object {
        fun start(activity: AppCompatActivity) {
            FragmentContainerActivity.invoke<ViewPage2Fragment>(activity)
        }
    }

    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        //offscreenPageLimit默认0，初始化值加载当前页面
        //eg设置为1，加载前后各一个页面，只有当前的走onResume()
        mBinding.root.offscreenPageLimit=1
        mBinding.root.simplePagerAdapter(this, 10) { position ->
            ImeHelperFragment()
        }
    }
}
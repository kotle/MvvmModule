package com.weilele.mvvmlib.fragment.pageing3

import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import com.weilele.base.library.BaseViewModel
import com.weilele.delegate.unsafeLazy
import com.weilele.mvvm.adapter.createPagingData
import com.weilele.mvvm.adapter.createPagingLoadData
import com.weilele.mvvmlib.sample.holder.SimpleTextHolder

class Paging3ViewModel : BaseViewModel() {
    val pagingData by unsafeLazy {
        createPagingLoadData<String, SimpleTextHolder.Bean>(
            PagingConfig(30),
            "100"
        ) {
            val key = it.key
            PagingSource.LoadResult.Page(mutableListOf(), null, null)
        }
    }

    val pagingData2 by unsafeLazy {
        createPagingData(PagingConfig(20, initialLoadSize = 20), 0) {
            TestDataSource(0)
        }
    }
}
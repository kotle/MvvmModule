package com.weilele.mvvmlib.fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.weilele.base.library.BaseBindingFragment
import com.weilele.base.library.FragmentContainerActivity
import com.weilele.mvvm.adapter.refreshList
import com.weilele.mvvm.utils.`object`.MvvmNotification
import com.weilele.mvvmlib.R
import com.weilele.mvvmlib.databinding.FragmentNotificationBinding
import com.weilele.mvvmlib.sample.holder.SimpleTextHolder

class NotificationFragment : BaseBindingFragment<FragmentNotificationBinding>() {
    companion object {
        fun start(appCompatActivity: AppCompatActivity) {
            FragmentContainerActivity.invoke<NotificationFragment>(appCompatActivity)
        }
    }

    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        initAdapter()
    }

    private fun initAdapter() {
        var count = 0
        mBinding.root.adapter = SimpleTextHolder.toAdapter().apply {
            refreshList(
                mutableListOf(
                    SimpleTextHolder.Bean("普通文本通知") {
                        count++
                        MvvmNotification.notifySimpleText(
                            1,
                            "1",
                            "我是普通文本通知：$count",
                            R.mipmap.ic_launcher
                        )
                    },
                    SimpleTextHolder.Bean("设置通知渠道组") {
                        val groupId = "666"
                        MvvmNotification.createNotificationChannelGroup(groupId, "测试渠道组")
                        MvvmNotification.createNotificationChannel("1", "测试渠道组-渠道1") {
                            it.setGroup(groupId)
                        }
                        MvvmNotification.createNotificationChannel("2", "测试渠道组-渠道2") {
                            it.setGroup(groupId)
                        }
                    },
                )
            )
        }
    }
}
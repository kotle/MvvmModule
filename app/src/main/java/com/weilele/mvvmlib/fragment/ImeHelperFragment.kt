package com.weilele.mvvmlib.fragment

import android.os.Bundle
import com.weilele.base.library.BaseBindingFragment
import com.weilele.mvvm.utils.activity.onClick
import com.weilele.mvvm.utils.android_r.*
import com.weilele.mvvmlib.databinding.FragmentImeHelperBinding

/**
 * viewpage2为竖直（水平方向没问题）方向，如果下一个页面没有加载，弹出键盘会加载下一个界面，导致焦点问题，
 * 可以设置offscreenPageLimit解决，原理就是预加载下一个界面，免得弹出键盘的时候又去加载
 */
class ImeHelperFragment : BaseBindingFragment<FragmentImeHelperBinding>() {
    private val imeHelper by lazy { ImeHelper(this) }
    private val imeHelper2 by lazy { ImeHelper2(this) }
    override fun onResume() {
        super.onResume()
    }

    override fun initUi(savedInstanceState: Bundle?) {
        super.initUi(savedInstanceState)
        mBinding.show.onClick {
            mBinding.et2.showIme()
        }
        mBinding.hide.onClick {
            mBinding.et2.hideIme()
        }
//        imeHelper.setOnImeListener { imeOffset, imeHeight ->
//            mBinding.space.setGone(imeOffset <= 0)
//        }
//        imeHelper2.setOnImeListener { offset,imeHeight,sh,nh->
//            logI { "键盘事件回调------>" }
//        }
    }
}
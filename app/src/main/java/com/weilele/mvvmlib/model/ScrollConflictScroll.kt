package com.weilele.mvvmlib.model

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import com.weilele.delegate.unsafeLazy
import com.weilele.mvvm.view.ScrollConflictHelper
import com.weilele.mvvm.widget.BaseNestedScrollView

class ScrollConflictScroll : BaseNestedScrollView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private val scrollConflictHelper by unsafeLazy { ScrollConflictHelper(this) }
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        scrollConflictHelper.dispatchTouchEvent(ev)
        return super.dispatchTouchEvent(ev)
    }
}
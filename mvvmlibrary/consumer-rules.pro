# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

##XBanner 图片轮播混淆配置
-keep class com.weilele.mvvm.utils.recycler_view.**{*;}
-keep class com.weilele.mvvm.adapter.**{*;}
#因为会用到反射创建对象，最好不要混淆
-keep class * extends com.weilele.mvvm.base.MvvmViewModel {
 <init>(...);
}
#ViewBinding不混淆
-keep  class * implements androidx.viewbinding.ViewBinding {
 *;
}
-keep class * extends com.weilele.mvvm.base.MvvmModel {
 <init>(...);
}
-dontwarn com.weilele.**
#-keep class * extends com.weilele.mvvm.utils.recycler_view.BaseRecyclerViewAdapter {
# <init>(...);
#}
#保持构造函数不被混淆，否则无法反射生成holder
-keep class * extends com.weilele.mvvm.adapter.MvvmHolder {
 <init>(...);
}
-keep class * implements com.weilele.mvvm.adapter.IMvvmAdapter {
 <init>(...);
}
-keep  class * implements com.weilele.MvvmNonConfused {
 *;
}

#gilde
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep class * extends com.bumptech.glide.module.AppGlideModule {
 <init>(...);
}
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keep class com.bumptech.glide.load.data.ParcelFileDescriptorRewinder$InternalRewinder {
  *** rewind();
}
#banner
-keep class androidx.viewpager2.widget.ViewPager2{
androidx.viewpager2.widget.PageTransformerAdapter mPageTransformerAdapter;
androidx.viewpager2.widget.ScrollEventAdapter mScrollEventAdapter;
}

-keep class androidx.viewpager2.widget.PageTransformerAdapter{
androidx.recyclerview.widget.LinearLayoutManager mLayoutManager;
}

-keep class androidx.recyclerview.widget.RecyclerView$LayoutManager{
androidx.recyclerview.widget.RecyclerView mRecyclerView;
}

-keep class androidx.viewpager2.widget.ScrollEventAdapter{
androidx.recyclerview.widget.LinearLayoutManager mLayoutManager;
}
# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

-ignorewarnings
-dontwarn com.baidu.**
-dontwarn com.tencent.**
#此接口的所有实现类不被混淆
-keep public class * implements vc.thinker.shared.flash.bean.UnMix
#保留某个包下面的类以及子包，一般是bean目录
-keep public class com.yizisu.swagger.bo.** {*;}
-keep public class vc.thinker.shared.flash.bean.** {*;}
#------------------------------------------------------jpush start
-dontoptimize
-dontpreverify

-dontwarn cn.jpush.**
-keep class cn.jpush.** { *; }
-keep class * extends cn.jpush.android.helpers.JPushMessageReceiver { *; }

-dontwarn cn.jiguang.**
-keep class cn.jiguang.** { *; }
#==================gson && protobuf==========================
-dontwarn com.google.**
-keep class com.google.gson.** {*;}
-keep class com.google.protobuf.** {*;}

-keep class com.thinker.jpush.** { *; }
-dontwarn com.thinker.jpush.**
#------------------------------------------------------jpush end
#------------------------------------------------------网络start
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepattributes Signature
-keepattributes *Annotation*
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
# Application classes that will be serialized/deserialized over Gson 下面替换成自己的实体类
#-keep class com.example.bean.** { *; }
-keep class com.thinker.genhttplib.** { *; }

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-dontwarn com.squareup.okhttp3.**
-keep class com.squareup.okhttp3.** { *;}
-dontwarn okio.**
#------------------------------------------------------网络end
#------------------------------------------------------车牌识别start
#混淆start
#*******************下面避免混淆配置需要在主工程中配置*******************


# -------------不需要更改-------------------------------------------------
-optimizationpasses 5                    # 指定代码的压缩级别
-dontusemixedcaseclassnames              # 是否使用大小写混合
-dontpreverify                           # 混淆时是否做预校验
-verbose                                 # 混淆时是否记录日志
-dontwarn                                # 去掉警告
-dontskipnonpubliclibraryclassmembers    # 是否混淆第三方jar
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*  # 混淆时所采用的算法


# ------ 保持哪些类名称不被混淆，内部变量方法会被混淆----------------------------------
# ------ 如果使用FragmentContainerView,Fragment是不能被混淆的，否则无法通过反射创建对象
#-keep public class * extends android.app.Activity
#-keep public class * extends android.app.Application
#-keep public class * extends android.app.Service
#-keep public class * extends android.content.BroadcastReceiver
#-keep public class * extends android.content.ContentProvider
#-keep public class * extends android.app.backup.BackupAgentHelper
#-keep public class * extends android.preference.Preference
-keep public class * extends androidx.fragment.app.Fragment
-keep public class * extends android.app.Fragment
#-keep public class * extends android.view.View
#-keep public class * extends androidx.appcompat.app.AppCompatActivity
#-keep public class com.android.vending.licensing.ILicensingService


# ------ 保持一些方法不被混淆    ----------------不需要更改----------------------------------------------
#------保持native方法不被混淆 ------
-keepclasseswithmembernames class * {
    native <methods>;
}
#------保持自定义控件类不被混淆------
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}
#------保持自定义控件类不被混淆------
-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
#------保持自定义控件类不被混淆------
-keepclassmembers class * extends android.app.Activity {
    public void *(android.view.View);
}
#------保持枚举 enum 类不被混淆 ------
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
#------保持 Parcelable 不被混淆------
-keep class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}

# ------ 去掉发出的警告   ----------------随情况更改-------------------------------------



-dontwarn com.wintone.Adaptor.**          #去掉该路径下的类所发出的警告
-keep class com.wintone.Adaptor.** { *;}  #如果该路径发出了警告，就不混淆该路径
-dontwarn com.wintone.cipher.**
-keep class com.wintone.cipher.** { *;}
-dontwarn com.kernal.lisence.**
-keep class com.kernal.lisence.** { *;}
-dontwarn kernal.sun.misc.**
-keep class kernal.sun.misc.** { *;}
-dontwarn com.kernal.plateid.**
-keep class com.kernal.plateid.** { *;}
-dontwarn utills.**
-keep class utills.** { *;}
-dontwarn com.kernal.lisence.DeviceFP.**
-keep class com.kernal.lisence.DeviceFP.** { *;}

# ------ 序列号授权需要   -----------
#-dontwarn org.**
#-keep class org.** { *;}


#混淆end
#------------------------------------------------------车牌识别end

#-------------------------------------------------高德地图start
-keep class com.thinker.gdmaplib.** { *; }
-dontwarn com.thinker.gdmaplib.**

#3D 地图
-keep class com.amap.api.mapcore.**{*;}
-keep class com.amap.api.maps.**{*;}
-keep class com.autonavi.amap.mapcore.*{*;}
#定位
-keep class com.amap.api.location.**{*;}
-keep class com.loc.**{*;}
-keep class com.amap.api.fence.**{*;}
-keep class com.autonavi.aps.amapapi.model.**{*;}
# 搜索
-keep class com.amap.api.services.**{*;}
# 2D地图
-keep class com.amap.api.maps2d.**{*;}
-keep class com.amap.api.mapcore2d.**{*;}
# 导航
-keep class com.amap.api.navi.**{*;}
-keep class com.autonavi.**{*;}

#语音
-keep class com.iflytek.cloud.**{*;}
-keep class com.iflytek.msc.**{*;}
-keep interface com.iflytek.**{*;}
#-------------------------------------------------高德地图end

# 蒲公英混淆
#-libraryjars libs/pgyer_sdk_x.x.jar
-dontwarn com.pgyersdk.**
-keep class com.pgyersdk.** { *; }
-keep class com.pgyersdk.**$* { *; }
#科大讯飞
-keep class com.iflytek.**{*;}
-keepattributes Signature

#mob
-keep class com.mob.**{*;}
-keep class cn.smssdk.**{*;}
-dontwarn com.mob.**

#stripe
-keep class com.stripe.android.** { *; }

#takePhoto
-keep class com.jph.takephoto.** { *; }
-dontwarn com.jph.takephoto.**

-keep class com.darsh.multipleimageselect.** { *; }
-dontwarn com.darsh.multipleimageselect.**

-keep class com.soundcloud.android.crop.** { *; }
-dontwarn com.soundcloud.android.crop.**

#Vlayout
-keepattributes InnerClasses
-keep class com.alibaba.android.vlayout.ExposeLinearLayoutManagerEx { *; }
-keep class android.support.v7.widget.RecyclerView$LayoutParams { *; }
-keep class android.support.v7.widget.RecyclerView$ViewHolder { *; }
-keep class android.support.v7.widget.ChildHelper { *; }
-keep class android.support.v7.widget.ChildHelper$Bucket { *; }
-keep class android.support.v7.widget.RecyclerView$LayoutManager { *; }

-keep class androidx.recyclerview.widget.RecyclerView$LayoutParams { *; }
-keep class androidx.recyclerview.widget.RecyclerView$ViewHolder { *; }
-keep class androidx.recyclerview.widget.ChildHelper { *; }
-keep class androidx.recyclerview.widget.ChildHelper$Bucket { *; }
-keep class androidx.recyclerview.widget.RecyclerView$LayoutManager { *; }

#umeng
-keep class com.uc.** {*;}
-keep class com.umeng.** {*;}

-keep class com.uc.** {*;}

-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keep class com.zui.** {*;}
-keep class com.miui.** {*;}
-keep class com.heytap.** {*;}
-keep class a.** {*;}
-keep class com.vivo.** {*;}

#litepal 数据库
-keep class org.litepal.** {
    *;
}
-keep class * extends org.litepal.crud.DataSupport {
    *;
}
-keep class * extends org.litepal.crud.LitePalSupport {
    *;
}

#第三方下载库
-dontwarn com.arialyy.aria.**
-keep class com.arialyy.aria.**{*;}
-keep class **$$DownloadListenerProxy{ *; }
-keep class **$$UploadListenerProxy{ *; }
-keep class **$$DownloadGroupListenerProxy{ *; }
-keep class **$$DGSubListenerProxy{ *; }
-keepclasseswithmembernames class * {
    @Download.* <methods>;
    @Upload.* <methods>;
    @DownloadGroup.* <methods>;
}
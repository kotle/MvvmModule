package com.weilele.delegate

import com.weilele.mvvm.utils.file.mmkvGetValue
import com.weilele.mvvm.utils.file.mmkvPutValue
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


fun <T> mmkvStorage(key: String, defaultValue: T) = MmkvDelegate(key, defaultValue)

/**
 * 属性托管
 */
class MmkvDelegate<T>(
    private val key: String,
    private val defaultValue: T
) : ReadWriteProperty<Any?, T> {
    //使用临时变量，防止每次都访问mmkv
    private var temp: T? = null
    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        val lastValue = temp
        return if (lastValue == null) {
            val mmkvValue = mmkvGetValue(key, defaultValue)
            temp = mmkvValue
            mmkvValue
        } else {
            lastValue
        }
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        if (mmkvPutValue(key, value)) {
            temp = value
        }
    }
}
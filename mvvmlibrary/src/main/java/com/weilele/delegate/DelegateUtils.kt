package com.weilele.delegate


/**
 * 不安全的初始化，性能更高
 */
fun <T> unsafeLazy(initializer: () -> T) = lazy(LazyThreadSafetyMode.NONE, initializer)
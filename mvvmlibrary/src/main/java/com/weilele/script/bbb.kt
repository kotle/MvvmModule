package com.weilele.script

import java.io.File
import java.lang.StringBuilder

private val path = "E:\\20200521\\project\\weijiale\\LlMusic\\app\\src\\main\\java\\com\\zisuyi\\ll\\music\\bean\\migu"

fun main() {
    startCheckFile()
}

/**
 * 将基本数据类型全部转为字符串类型
 */
private fun startCheckFile() {
    val filePath = "E:\\20200521\\project\\weijiale\\LlMusic\\app\\src\\main\\java\\com\\zisuyi\\ll\\music\\bean\\netease\\NeteaseTrendPlaylistBean.java"
    val file = File(filePath)
    checkFile(file)
}

private fun checkFile(file: File) {
    if (file.isDirectory) {
        file.listFiles()?.forEach {
            if (file.isDirectory) {
                checkFile(it)
            } else {
                changeBean(file)
            }
        }
    } else {
        changeBean(file)
    }
}

private fun changeBean(file: File) {
    if (!file.exists()) {
        return
    }
    val text = file.readLines()
    val sb = StringBuilder()
    text.forEach {
        it.apply {
            if (!replaceText(sb, " int ", " String ")
                    && !replaceText(sb, "(int ", "(String ")
                    && !replaceText(sb, " Integer ", " String ")
                    && !replaceText(sb, "(Integer ", "(String ")
                    && !replaceText(sb, " long ", " String ")
                    && !replaceText(sb, "(long ", "(String ")
                    && !replaceText(sb, " double ", " String ")
                    && !replaceText(sb, "(double ", "(String ")
            ) {
                sb.append(it + "\n")
            }
        }
    }
    file.writeText(sb.toString())
}

private fun String.replaceText(sb: StringBuilder, old: String, new: String): Boolean {
    return if (this.contains(old, ignoreCase = true)) {
        sb.append(replace(old, new, ignoreCase = true) + "\n")
        true
    } else {
        false
    }
}


package com.weilele.script;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestBean {

    @SerializedName("data")
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : Java 开发
         * edition : 3
         * author : ["smith","张三","李四"]
         */

        @SerializedName("title")
        private String title;
        @SerializedName("edition")
        private String edition;
        @SerializedName("edition")
        private String edition1;
        @SerializedName("edition")
        private String edition2;
        @SerializedName("edition")
        private String edition3;
        @SerializedName("edition")
        private String edition4;
        @SerializedName("edition")
        private String edition5;
        @SerializedName("author")
        private List<String> author;

        public String getEdition1() {
            return edition1;
        }

        public void setEdition1(String edition1) {
            this.edition1 = edition1;
        }

        public String getEdition2() {
            return edition2;
        }

        public void setEdition2(String edition2) {
            this.edition2 = edition2;
        }

        public String getEdition3() {
            return edition3;
        }

        public void setEdition3(String edition3) {
            this.edition3 = edition3;
        }

        public String getEdition4() {
            return edition4;
        }

        public void setEdition4(String edition4) {
            this.edition4 = edition4;
        }

        public String getEdition5() {
            return edition5;
        }

        public void setEdition5(String edition5) {
            this.edition5 = edition5;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getEdition() {
            return edition;
        }

        public void setEdition(String edition) {
            this.edition = edition;
        }

        public List<String> getAuthor() {
            return author;
        }

        public void setAuthor(List<String> author) {
            this.author = author;
        }
    }
}

package com.weilele.library.websocket

enum class WebSocketState {
    Connecting,
    NoConnect,
    Connected
}
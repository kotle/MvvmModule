package com.weilele.library.websocket

import com.weilele.library.websocket.okhttp.OKHttpWebSocketImpl

object WebSocketFactory {
    fun createWebSocket(url: String): IWebSocket {
        return OKHttpWebSocketImpl(url)
    }

//    fun createSocketIo(url: String): IWebSocket {
//        return SocketIoImpl(url)
//    }
}
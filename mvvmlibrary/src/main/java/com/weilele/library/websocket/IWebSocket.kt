package com.weilele.library.websocket

interface IWebSocket {
    /**
     * 开始连接
     */
    fun connect()

    /**
     * 断开连接
     */
    fun disConnect()

    /**
     * 设置接收到消息消息的监听
     */
    fun setOnReceiveMessageListener(onMessage: Function1<Any?, Unit>)

    /**
     * 设置连接监听
     */
    fun setOnConnectListener(onSocketStatusChange: Function1<WebSocketState, Unit>)

    /**
     * 发送文本消息
     */
    fun sendMessage(msg: Any?): Boolean

    /**
     * 发送文本消息
     * 如果网络已经断开，等连接的时候自动发送此条消息
     */
    fun sendMessageWaitConnect(msg: Any?)

    /**
     * 是否连接
     */
    fun isConnect(): Boolean

    /**
     * 获取真实对象
     */
    fun getRealWebSocket(): Any?

    /**
     * 获取连接监听
     */
    fun addConnectListener() {}

    /**
     * 每隔duration的时间检查是否处于连接
     * 不是则自动连接
     */
    fun enableAutoConnectTask(
        enable: Boolean,
        duration: Long? = null,
        taskRunListener: Function1<IWebSocket, Unit>? = null
    )
}
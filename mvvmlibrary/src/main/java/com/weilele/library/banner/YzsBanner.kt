package com.weilele.library.banner

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.annotation.Nullable
import androidx.lifecycle.LifecycleOwner
import androidx.viewpager2.widget.ViewPager2
import com.weilele.mvvm.utils.safeGet
import com.weilele.library.banner.impl.ybanner.YzsYbannerImpl
import com.weilele.mvvm.utils.glide.setImageGlide
import java.lang.ref.WeakReference


interface YzsBanner<T : YzsBanner.BannerData> {
    companion object {
        operator fun <T : BannerData> invoke(viewGroup: ViewGroup): YzsBanner<T> {
            return YzsYbannerImpl(viewGroup)
        }
    }

    interface BannerData {
        fun getBannerUrl(): Any

        fun getBannerTitle(): String? = null
    }

    interface Listener<T : BannerData> {
        /**
         * 当加载banner的时候回调
         */
        fun onLoadBanner(
            bannerItemView: View,
            bannerData: T,
            position: Int
        ) {
            //默认实现，可以重写
            bannerItemView.safeGet<ImageView>()?.setImageGlide(bannerData.getBannerUrl())
        }

        /**
         * 当点击banner的时候回调
         */
        fun onBannerClick(
            bannerItemView: View,
            bannerData: T,
            position: Int
        ) {
            //默认实现，可以重写
        }

        fun onBannerSelected(
            bannerData: T,
            position: Int
        ) {
            //默认实现，可以重写
        }
    }

    /**
     * 获取viewPager2
     */
    val viewPager2: ViewPager2

    /**
     * 默认实现监听，否则数据不加载
     */
    class DefaultListener<T : BannerData> : Listener<T>

    /**
     * 设置监听
     */
    fun setListener(listener: Listener<T>)

    /**
     * 设置view与页面的距离
     * 横向的就是左右间隔
     * 竖向就是上线间隔
     */
    fun setItemMargin(margin: Int)

    /**
     * 设置页面与页面的距离
     */
    fun setPagerMargin(margin: Int)

    /**
     * 设置数据
     */
    fun setDatas(
        datas: MutableList<T>,
        @LayoutRes layoutId: Int? = null,
        isCanInfiniteScroll: Boolean = true
    )

    /**
     * 获取设置的数据
     */
    fun getDatas(): MutableList<T>?

    /**
     * 是否可以自动滚动
     */
    fun setCanAutoScroll(enable: Boolean)

    /**
     * 滚动时长
     */
    fun setAutoScrollDuration(duration: Int)

    /**
     * 滚动间隔
     */
    fun setAutoScrollInterval(time: Long)

    /**
     * 开始自动轮播
     * 前提是[setCanAutoScroll]设置为true
     */
    fun startAutoScroll()

    /**
     * 停止自动轮播
     */
    fun stopAutoScroll()

    /**
     * setPageTransformer
     * 设置动画
     */
    fun setPageTransformer(@Nullable transformer: ViewPager2.PageTransformer?)

    /**
     * 只允许响应当前页面点击
     * 因为viewpager 一般是一个页面一屏幕
     * 有时候需要点击露出在两边的item，这里设置为false
     */
    fun canOnlyClickCurrentItem(onlyClick: Boolean)

    /**
     * 设置关联生命周期
     * 不可见的时候，自动停止自动滚动
     */
    fun setLifecycle(lifecycleOwner: LifecycleOwner?)
}
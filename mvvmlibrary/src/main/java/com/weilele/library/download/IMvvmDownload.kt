package com.weilele.library.download

import android.app.DownloadManager
import android.net.Uri
import android.os.Environment
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.weilele.library.download.impl.downloadmanager.DownloadManagerImpl
import com.weilele.mvvm.base.helper.ILifecycleObserver

/**
 * 暂时不知怎么取消，功能测试未完成
 */
interface IMvvmDownload : ILifecycleObserver {
    companion object {
        //当下载等待开始时
        const val DOWNLOAD_STATUS_PENDING = DownloadManager.STATUS_PENDING

        //下载中
        const val DOWNLOAD_STATUS_RUNNING = DownloadManager.STATUS_RUNNING

        //下载失败（有任务，但是没有下载）
        const val DOWNLOAD_STATUS_FAILED = DownloadManager.STATUS_FAILED

        //下载暂停
        const val DOWNLOAD_STATUS_PAUSED = DownloadManager.STATUS_PAUSED

        //下载完成
        const val DOWNLOAD_STATUS_SUCCESSFUL = DownloadManager.STATUS_SUCCESSFUL


        operator fun invoke(appCompatActivity: AppCompatActivity): IMvvmDownload {
            val impl = DownloadManagerImpl(appCompatActivity.applicationContext)
            appCompatActivity.lifecycle.addObserver(impl)
            return impl
        }

        operator fun invoke(fragment: Fragment): IMvvmDownload {
            val impl = DownloadManagerImpl(fragment.requireContext().applicationContext)
            fragment.lifecycle.addObserver(impl)
            return impl
        }
    }

    /**
     * 销毁的时候需要记得移除[listener]
     */
    fun addListener(listener: Listener)

    /**
     * 移除监听
     */
    fun removeListener(listener: Listener)

    /**
     * 开始下载任务
     */
    fun startDownload(request: Request): Long

    /**
     * 查询下载
     */
    fun queryDownload(vararg ids: Long/*用于过滤，查询指定下载*/): MutableList<Request>

    /**
     * 查询下载
     */
    fun queryDownload(@MvvmDownloadStatus status: Int/*用于过滤，查询指定下载*/): MutableList<Request>


    /**
     * 暂停
     */
    fun pauseDownload(vararg ids: Long): Boolean


    /**
     * 恢复下载
     */
    fun resumeDownload(vararg ids: Long): Boolean


    /**
     * 删除下载
     */
    fun deleteDownload(vararg ids: Long): Boolean

    open class Request(
        val uri: Uri,
        val title: CharSequence,
        val mineType: String? = null,
        val savePublic: Boolean = false,
        val dirType: String = Environment.DIRECTORY_DOWNLOADS,
        val subPath: String = title.toString(),
        val description: String? = null,
    ) {

        /**
         * 唯一标识符
         */
        internal var _id = -1L
        val id: Long
            get() = _id

        /**
         * 文件总大小
         */
        internal var _totalSize = 0L
        val totalSize: Long
            get() = _totalSize

        /**
         * 已经下载大小
         */
        internal var _downloadedSize = 0L
        val downloadedSize: Long
            get() = _downloadedSize

        /**
         * 当前状态
         */
        @MvvmDownloadStatus
        internal var _status = DOWNLOAD_STATUS_PENDING
        val status: Int
            @MvvmDownloadStatus
            get() = _status
    }

    interface Listener {
        /**
         * 获取到最新详情的监听
         */
        fun onUpdate(request: Request) {}

        /**
         * 通知栏点击
         */
        fun onNotificationClicked(request: MutableList<Request>) {}

        /**
         * 查询到最新所有的下载列表刷新
         */
        fun onUpdateList(request: MutableList<Request>) {}
    }

}
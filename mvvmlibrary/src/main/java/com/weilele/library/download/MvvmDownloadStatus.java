package com.weilele.library.download;

import androidx.annotation.IntDef;

import com.weilele.mvvm.base.livedata.StatusData;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({IMvvmDownload.DOWNLOAD_STATUS_FAILED,
        IMvvmDownload.DOWNLOAD_STATUS_RUNNING,
        IMvvmDownload.DOWNLOAD_STATUS_PAUSED,
        IMvvmDownload.DOWNLOAD_STATUS_SUCCESSFUL,
        IMvvmDownload.DOWNLOAD_STATUS_PENDING,
})
@Retention(RetentionPolicy.SOURCE)
public @interface MvvmDownloadStatus {
}

package com.weilele.mvvm.base

import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import com.weilele.mvvm.base.helper.*
import com.weilele.mvvm.base.livedata.LiveDataWrap
import com.weilele.mvvm.base.livedata.observeForever
import com.weilele.mvvm.base.livedata.observer
import com.weilele.mvvm.base.livedata.removeObserver
import com.weilele.mvvm.utils.safeGet
import java.lang.IllegalArgumentException

/**
 * 和dialog一样使用的popupWindow
 * 优先推荐使用dialog，
 * dialog实现有问题的时候可以使用这个尝试
 */
abstract class PopupWindowDialog :
    MvvmPopupWindow(
        ViewGroup.LayoutParams.MATCH_PARENT,
        ViewGroup.LayoutParams.WRAP_CONTENT
    ), BaseMvvmInterface, ILifecycleObserver {

    /**
     * 保存所有的注册监听者
     */
    private val observerLiveDatas by lazy { mutableListOf<LiveDataWrap>() }

    val appCompatActivity: AppCompatActivity
        get() = context.safeGet() ?: throw IllegalArgumentException("使用这个PopupWindow必须需要一个activity")

    /**
     * 创建viewModel
     */
    inline fun <reified T : ViewModel> createViewModel(): T {
        return appCompatActivity.createViewModel()
    }

    /**
     * 创建view
     */
    private fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contentView =
            defaultMvvmCreateView(inflater, container, savedInstanceState) ?: return null
        val rootView = FrameLayout(inflater.context)
        rootView.addView(contentView, rootViewLp)
        return rootView
    }

    private fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        if (view == null) {
            dismiss()
            return
        }
        //在popup中[getObserverLiveData]也是永久注册
        getObserverLiveData()?.also {
            it.observeForever()
            observerLiveDatas.addAll(it)
        }
        getObserverLiveDataForever()?.also {
            it.observeForever()
            observerLiveDatas.addAll(it)
        }
        getClickView()?.forEach {
            it?.setOnClickListener(this)
        }
        initUi(savedInstanceState)
        initData()
    }

    private val rootViewLp by lazy {
        FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
    }

    /**
     * 调用之后显示对话框
     */
    open fun show(
        activity: FragmentActivity?,
        gravity: Int = Gravity.CENTER,
        offX: Int = 0,
        offY: Int = 0
    ) {
        activity ?: return
        checkContext(activity)
        //activity.window.decorView 上线文不是activity
        showAtLocation(activity.window.decorView, gravity, offX, offY)
    }

    override fun onPreShow(isAsDropDown: Boolean, gravity: Int) {
        rootViewLp.gravity = gravity
        onRootViewLayoutParams(isAsDropDown, rootViewLp)
        contentView = onCreateView(LayoutInflater.from(appCompatActivity), null, null)
        onViewCreated(contentView, null)
        super.onPreShow(isAsDropDown, rootViewLp.gravity)
    }

    /**
     * 设置布局参数
     */
    protected open fun onRootViewLayoutParams(isAsDropDown: Boolean, lp: FrameLayout.LayoutParams) {
        lp.gravity = Gravity.CENTER
    }

    override fun dismiss() {
        observerLiveDatas.removeObserver()
        super.dismiss()
    }
}
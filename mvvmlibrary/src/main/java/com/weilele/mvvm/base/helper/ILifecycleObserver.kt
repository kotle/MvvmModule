package com.weilele.mvvm.base.helper

import androidx.annotation.Keep
import androidx.lifecycle.*

/**
 * 需要keep注解，不然混淆之后子类的方法不回调（之前版本没问题，不知哪个版本出现这个问题）
 * 如果不用keep注解，子类需要加上父类注解
 * 执行顺序如下
 *
 * --->onCreate()
 * --->onCreate(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner, event: Lifecycle.Event)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange()
 *
 * --->onStart()
 * --->onStart(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner, event: Lifecycle.Event)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange()
 *
 * --->onResume()
 * --->onResume(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner, event: Lifecycle.Event)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange()
 *
 * --->onPause(lifecycleOwner: LifecycleOwner)
 * --->onPause()
 * --->onLifeChange(lifecycleOwner: LifecycleOwner, event: Lifecycle.Event)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange()
 *
 * --->onStop(lifecycleOwner: LifecycleOwner)
 * --->onStop()
 * --->onLifeChange(lifecycleOwner: LifecycleOwner, event: Lifecycle.Event)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange()
 *
 * --->onDestroy(lifecycleOwner: LifecycleOwner)
 * --->onDestroy()
 * --->onLifeChange(lifecycleOwner: LifecycleOwner, event: Lifecycle.Event)
 * --->onLifeChange(lifecycleOwner: LifecycleOwner)
 * --->onLifeChange()
 */
@Keep
interface ILifecycleObserver : DefaultLifecycleObserver {
    override fun onCreate(owner: LifecycleOwner) {
        onCreate()
    }

    override fun onStart(owner: LifecycleOwner) {
        onStart()
    }

    override fun onResume(owner: LifecycleOwner) {
        onResume()
    }

    override fun onPause(owner: LifecycleOwner) {
        onPause()
    }

    override fun onStop(owner: LifecycleOwner) {
        onStop()
    }

    override fun onDestroy(owner: LifecycleOwner) {
        onDestroy()
    }

    fun onCreate() {

    }

    fun onStart() {
    }

    fun onResume() {
    }

    fun onPause() {
    }

    fun onStop() {
    }

    fun onDestroy() {
    }
}
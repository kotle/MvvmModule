package com.weilele.mvvm.base.helper

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.weilele.mvvm.base.livedata.LiveDataWrap

/**
 * 描述：此接口使mvvm模式下的dialog,fragment,activity,popupWindow的子类命名方法统一，方便互转
 */
interface BaseMvvmInterface : OnSingleClickListener {

    /**
     * 创建之后的回调
     */
    fun beforeMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): ViewGroup? {
        return null
    }

    fun onMvvmCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): Any?

    fun afterMvvmCreateView(
        inflater: LayoutInflater,
        beforeView: ViewGroup?,
        createView: Any?,
        savedInstanceState: Bundle?
    ): Any? {
        return beforeView ?: createView
    }

    /**
     * 获取需要注册点击的view
     */
    fun getClickView(): List<View?>?

    /**
     * 获取需要注册监听的
     */
    fun getObserverLiveData(): List<LiveDataWrap>? = null

    /**
     * 获取永远需要注册监听的
     */
    fun getObserverLiveDataForever(): List<LiveDataWrap>? = null


    /**
     * 初始化view
     * 不必要，为了代码规范设置
     */
    fun initUi(savedInstanceState: Bundle?)

    /**
     * 初始化数据
     * 不必要，为了代码规范设置
     */
    fun initData()
}
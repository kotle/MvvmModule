package com.weilele.mvvm.base

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Html
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.database.getStringOrNull
import com.weilele.mvvm.R
import com.weilele.mvvm.adapter.ignoreError
import com.weilele.mvvm.app
import com.weilele.mvvm.utils.file.SaveFileType
import com.weilele.mvvm.utils.file.saveByteArrayToPublicDirectory
import com.weilele.mvvm.utils.longTimeToStr
import com.weilele.mvvm.utils.openFileByOtherApp
import com.weilele.mvvm.utils.permission.checkPermission
import java.io.PrintWriter
import java.io.StringWriter
import java.lang.reflect.Field
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

/**
 * 捕获全局异常
 */
class UncaughtExceptionActivity : AppCompatActivity() {
    companion object {
        private const val KEY_INFO = "errorInfo"
        fun start(context: Context, t: Thread, e: Throwable) {
            context.startActivity(
                Intent(app, UncaughtExceptionActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    ignoreError {
                        putExtra(KEY_INFO, getErrorInfo(e))
                    }
                }
            )
        }

        private fun getErrorInfo(e: Throwable): String {
            //用于存储设备信息
            val mInfo: MutableMap<String, String> = HashMap()
            val pm: PackageManager = app.packageManager
            val info: PackageInfo =
                pm.getPackageInfo(app.packageName, PackageManager.GET_ACTIVITIES)
            // 获取版本信息
            val versionName =
                if (TextUtils.isEmpty(info.versionName)) "未设置版本名称" else info.versionName
            val versionCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                info.longVersionCode.toString() + ""
            } else {
                info.versionCode.toString() + ""
            }
            mInfo["versionName"] = versionName
            mInfo["versionCode"] = versionCode
            // 获取设备信息
            val fields: Array<Field> = Build::class.java.fields
            if (fields.isNotEmpty()) {
                for (field in fields) {
                    field.isAccessible = true
                    mInfo[field.name] = field.get(null).toString()
                }
            }
            return getErrorStackTrace(mInfo, e)
        }

        private fun getErrorStackTrace(mInfo: MutableMap<String, String>, e: Throwable): String {
            val stringBuffer = StringBuffer()
            stringBuffer.append(
                "${
                    longTimeToStr(
                        System.currentTimeMillis(),
                        "yyyy年MM月dd HH:mm:ss"
                    )
                }<br><br>"
            )
            stringBuffer.append("------------错误堆栈信息---------<br>")
            val stringWriter = StringWriter()
            val writer = PrintWriter(stringWriter)
            e.printStackTrace(writer)
            var cause = e.cause
            while (cause != null) {
                cause.printStackTrace(writer)
                cause = e.cause
            }
            writer.close()
            val string: String = stringWriter.toString()
            stringBuffer.append(string)
            stringBuffer.append("<br><br>------------设备信息---------<br>")
            for ((keyName, value) in mInfo) {
                stringBuffer.append("<b>$keyName：</b>$value<br>")
            }
            return stringBuffer.toString()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = Color.parseColor("#333333")
        window.navigationBarColor = Color.parseColor("#333333")
        setContentView(R.layout.activity_uncaught_exception)
        val errorInfo = Html.fromHtml(
            intent.getStringExtra(KEY_INFO)
        )
        findViewById<TextView>(R.id.tvInfo).text = errorInfo
        findViewById<View>(R.id.btReStart).setOnClickListener {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            intent.setPackage(packageName)
            val info = packageManager.queryIntentActivities(intent, 0)
            info.forEach {
                if (it.activityInfo.packageName == packageName) {
                    intent.setClassName(this, it.activityInfo.name)
                    startActivity(intent)
                    finish()
                    return@setOnClickListener
                }
            }
        }
        saveLog(findViewById(R.id.btSave), errorInfo.toString())
    }

    private fun saveLog(it: TextView, errorInfo: String) {
        it.setOnClickListener { _ ->
            if (!checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1
                )
                return@setOnClickListener
            }
            it.isEnabled = false
            //格式化时间，作为Log文件名
            val dateFormat: DateFormat =
                SimpleDateFormat("yyyy年MM月dd日HH时mm分ss秒", Locale.getDefault())

            val uri = saveByteArrayToPublicDirectory(
                errorInfo.toByteArray(),
                SaveFileType.Download,
                "text/plain",
                "${dateFormat.format(Date())}.txt",
                "${SaveFileType.Download.path}/崩溃日志"
            )
            it.isEnabled = true
            if (uri != null) {
                Toast.makeText(this, "日志保存成功", Toast.LENGTH_SHORT).show()
                it.text = "第三方应用打开"
                it.setOnClickListener {
                    ignoreError {
                        val coursor = contentResolver.query(uri, null, null, null, null)
                        if (coursor?.moveToFirst() == true) {
                            var path =
                                coursor.getStringOrNull(coursor.getColumnIndex(MediaStore.Files.FileColumns.DATA))
                            if (path.isNullOrBlank()) {
                                coursor.getStringOrNull(coursor.getColumnIndex(MediaStore.Downloads.RELATIVE_PATH))
                            }
                            if (!path.isNullOrBlank()) {
                                openFileByOtherApp(path)
                            }
                        }
                        coursor?.close()
                    }
                }
            } else {
                Toast.makeText(this, "日志保存失败", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
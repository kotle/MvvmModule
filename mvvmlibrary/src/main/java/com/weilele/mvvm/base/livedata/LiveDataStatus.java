package com.weilele.mvvm.base.livedata;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({StatusData.Running, StatusData.Success, StatusData.Error})
@Retention(RetentionPolicy.SOURCE)
public @interface LiveDataStatus {
}

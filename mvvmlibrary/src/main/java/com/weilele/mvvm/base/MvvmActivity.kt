package com.weilele.mvvm.base


import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.weilele.delegate.unsafeLazy
import com.weilele.mvvm.MvvmConf
import com.weilele.mvvm.adapter.ignoreError
import com.weilele.mvvm.base.helper.*
import com.weilele.mvvm.base.helper.annotation.dealViewModelAnnotation
import com.weilele.mvvm.base.livedata.LiveDataWrap
import com.weilele.mvvm.base.livedata.observeForever
import com.weilele.mvvm.base.livedata.observer
import com.weilele.mvvm.base.livedata.removeObserver
import com.weilele.mvvm.utils.`object`.ScreenAdaptationObj
import com.weilele.mvvm.utils.local.updateLocal
import com.weilele.mvvm.utils.result_contract.NavigateForResultHelper
import com.weilele.mvvm.utils.result_contract.PermissionForResultHelper

/**
 * 描述：mvvm--基类activity
 */
abstract class MvvmActivity : AppCompatActivity(), BaseMvvmInterface {
    val navigateForResultHelper by unsafeLazy { NavigateForResultHelper(this) }
    val permissionForResultHelper by unsafeLazy { PermissionForResultHelper(this) }

    /**
     * 保存所有的注册监听者
     */
    private val observerLiveDatas = mutableListOf<LiveDataWrap>()

    /**
     * onCreate
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //跳转帮助，必须在onCreate注册
        navigateForResultHelper
        permissionForResultHelper
        dealViewModelAnnotation()
        getObserverLiveData()?.also {
            it.observer(this)
            observerLiveDatas.addAll(it)
        }
        getObserverLiveDataForever()?.also {
            it.observeForever()
            observerLiveDatas.addAll(it)
        }
        val parentView = findViewById<ViewGroup>(android.R.id.content)
        defaultMvvmCreateView(layoutInflater, parentView, savedInstanceState)?.let {
            setContentView(it)
        }
        initUi(savedInstanceState)
        getClickView()?.forEach {
            it?.setOnClickListener(this)
        }
        initData()
    }

    override fun setRequestedOrientation(requestedOrientation: Int) {
        //处理安卓8.0报错
        //Only fullscreen activities can request orientation
        ignoreError { super.setRequestedOrientation(requestedOrientation) }
    }

    override fun getResources(): Resources {
        val resources = super.getResources()
        onScreenAdaptation(resources)
        return resources
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(
            newBase?.updateLocal(getLanguageTag())
        )
    }

    open fun getLanguageTag(): String? = null

    /**
     * 屏幕适配
     */
    open fun onScreenAdaptation(resources: Resources) {
        ScreenAdaptationObj.setCustomDensity(
            resources,
            MvvmConf.enableScreenAdaptation && isNeedScreenAdaptation()
        )
    }

    override fun onDestroy() {
        observerLiveDatas.removeObserver()
        super.onDestroy()
    }

    /**
     * 是否需要屏幕适配
     */
    open fun isNeedScreenAdaptation() = true
}
package com.weilele.mvvm.base.helper

import android.view.View

/**
 * 描述：防止快速点击事件监听
 */
class SingleClickListener(
    private val clickSpace: Long? = null,
    private val onSingleClick: Function1<View, Unit>
) : OnSingleClickListener {
    override fun onSingleClick(view: View) {
        onSingleClick.invoke(view)
    }

    override fun getDoubleClickSpace(): Long {
        if (clickSpace != null) {
            return clickSpace
        }
        return super.getDoubleClickSpace()
    }
}
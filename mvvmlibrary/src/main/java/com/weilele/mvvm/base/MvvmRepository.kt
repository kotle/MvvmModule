package com.weilele.mvvm.base

/**
 * Repository 是谷歌的标准写法
 * Model 是我自己的写法
 * 两者等价
 */
open class MvvmRepository : MvvmModel() {
}
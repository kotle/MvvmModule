package com.weilele.mvvm

import android.app.Application
import com.weilele.MvvmInit
import okhttp3.CookieJar
import java.util.*

/**
 * 描述：通用的一些配置信息
 * 如果需要修改配置，应该在[Application]的[Application.attachBaseContext]里面执行
 * 注意：不能在[Application.onCreate]中，因为[MvvmInit]初始化使用了【startup】,导致执行顺序的问题
 */
object MvvmConf {
    /**
     * 设置两次点击的时间间隔
     */
    var doubleClickSpace = 300L

    /**
     * 屏幕适配，是否开启
     */
    var enableScreenAdaptation = true

    /**
     * 屏幕适配是否适配第三方库
     */
    var enableOtherLibraryScreenAdaptation = true

    /**
     * 屏幕适配，设计尺寸，单位dp
     * [enableScreenAdaptation]为true才有效
     */
    var screenAdaptationUiDp = 360

    /**
     * [screenAdaptationUiDp]设置的尺寸是否是设计稿的短边
     */
    var screenAdaptationByWidth = true

    /**
     * 是否是debug模式
     */
    var isDebug = false

    /**
     * 是否信任所有证书,默认不信任，为了传输安全
     * okhttp（必须在创建之前使用）
     */
    var enableOkHttpTrustAllCertificates = false

    /**
     * 是否使用cookieJar
     * okhttp（必须在创建之前使用）
     */
    var enableOkHttpCookieJar = false

    /**
     * 自定义cookie管理
     */
    var customOkHttpCookieJar: CookieJar? = null

    /**
     * 设置okhttp缓存，如果大于0，开启缓存
     */
    var okHttpCacheSize = 0

    /**
     * 设置当前打包的渠道
     */
    var channel: String = Locale.getDefault().toLanguageTag()

    /**
     * 设置字体大小是否受系统字体大小的调节影响
     */
    var enableScaleFontBySystem = false

    /**
     * 缩放密度
     * 因为适配之后的字体总是偏大，所以设置0.96使他小一点
     * 可以设置这个参数，调整字体大小
     */
    var scaledDensityScaleFactor = 0.96f

    /**
     * 是否可以自定义异常捕获
     */
    var enableUncaughtExceptionHandler = false
}

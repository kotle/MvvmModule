package com.weilele.mvvm.utils.aria.download_group

import com.arialyy.aria.core.Aria
import com.arialyy.aria.core.download.DownloadGroupTaskListener
import com.arialyy.aria.core.task.DownloadGroupTask
import java.lang.Exception

/**
 * 任务状态类
 */
object DownloadGroupTaskStatus : DownloadGroupTaskListener {
    init {
        register()
    }

    internal val listenerMap = hashMapOf<Long?, MvvmDownloadGroup.Listener>()

    override fun onWait(task: DownloadGroupTask) {
        getListener(task)?.onWait(task)
    }

    override fun onPre(task: DownloadGroupTask) {
        getListener(task)?.onPre(task)
    }

    override fun onTaskPre(task: DownloadGroupTask) {
        getListener(task)?.onTaskPre(task)
    }

    override fun onTaskResume(task: DownloadGroupTask) {
        getListener(task)?.onTaskResume(task)
    }

    override fun onTaskStart(task: DownloadGroupTask) {
        getListener(task)?.onTaskStart(task)
    }

    override fun onTaskStop(task: DownloadGroupTask) {
        getListener(task)?.onTaskStop(task)
    }

    override fun onTaskCancel(task: DownloadGroupTask) {
        getListener(task)?.onTaskCancel(task)
    }

    override fun onTaskFail(task: DownloadGroupTask?, e: Exception?) {
        if (task != null) {
            getListener(task)?.onTaskFail(task, e)
        }
    }

    override fun onTaskComplete(task: DownloadGroupTask) {
        getListener(task)?.onTaskComplete(task)
    }

    override fun onTaskRunning(task: DownloadGroupTask) {
        getListener(task)?.onTaskRunning(task)
    }

    override fun onNoSupportBreakPoint(task: DownloadGroupTask) {
        getListener(task)?.onNoSupportBreakPoint(task)
    }

    private fun getListener(task: DownloadGroupTask): MvvmDownloadGroup.Listener? {
        return listenerMap[task.entity.id]
    }

    /**
     * 注册
     */
    fun register() {
        Aria.download(this).register()
    }

    /**
     * 取消注册
     */
    fun unRegister() {
        Aria.download(this).unRegister()
    }
}
package com.weilele.mvvm.utils.aria.download_group

import androidx.lifecycle.LifecycleOwner
import com.arialyy.aria.core.common.AbsNormalTarget
import com.arialyy.aria.core.config.DownloadConfig
import com.arialyy.aria.core.download.DownloadGroupEntity
import com.arialyy.aria.core.download.DownloadGroupTaskListener
import com.arialyy.aria.core.download.target.GroupBuilderTarget
import com.arialyy.aria.core.download.target.GroupNormalTarget
import com.arialyy.aria.core.inf.IEntity
import com.arialyy.aria.core.task.DownloadGroupTask
import com.weilele.mvvm.base.helper.ILifecycleObserver
import com.weilele.mvvm.utils.aria.AriaHelper
import com.weilele.mvvm.utils.string.toStr
import java.lang.Exception

object MvvmDownloadGroup {

    init {
        //保证所以操作调用前Aria 已经初始化完毕
        AriaHelper
    }

    /**
     * downloadTask
     */
    private val downloadTask by lazy { AriaHelper.downloadGroup }

    /**
     * 下载参数配置
     */
    fun updateConfig(block: (DownloadConfig) -> Unit) {
        block.invoke(AriaHelper.aria.downloadConfig)
    }

    /**
     * 更新task
     */
    fun <Target : AbsNormalTarget<*>> updateTask(
        taskId: Long,
        block: (GroupNormalTarget) -> AbsNormalTarget<Target>
    ) {
        downloadTask
            .loadGroup(taskId)
            .let {
                block.invoke(it)
            }
            .save()
    }

    /**
     * 获取所有普通下载任务
     * 获取未完成的普通任务列表{@link #getAllNotCompleteTask()}
     * 获取已经完成的普通任务列表{@link #getAllCompleteTask()}
     */
    val allTask: MutableList<DownloadGroupEntity>?
        get() = downloadTask.groupTaskList

    /**
     * filePath:完整路径名（/mnt/sdcard/test.zip）
     * 总是从头开始下载
     */
    fun start(
        url: MutableList<String>,
        dirPath: String,
        dirSize: Long? = null,
        block: ((build: GroupBuilderTarget) -> Unit)? = null
    ): Long {
        val taskBuild = downloadTask
            //读取下载地址
            .loadGroup(url)
            //设置文件夹
            .setDirPath(dirPath)
        if (dirSize == null || dirSize < 0) {
            taskBuild.unknownSize()
        } else {
            taskBuild.setFileSize(dirSize)
        }
        block?.invoke(taskBuild)
        //创建并启动下载
        return taskBuild.create()
    }

    /**
     * 继续下载
     */
    fun resume(taskId: Long, dirSize: Long? = null) {
        downloadTask
            .loadGroup(taskId)
            .let {
                if (dirSize == null || dirSize < 0) {
                    it.unknownSize()
                } else {
                    it.setFileSize(dirSize)
                }
            }
            .resume()
    }

    /**
     * 恢复所有下载
     * 有bug，待修复
     */
    @Deprecated("有bug，待修复")
    fun resumeAllTask() {
        downloadTask
            .resumeAllTask()
    }

    /**
     * 暂停下载
     */
    fun pause(taskId: Long) {
        downloadTask
            .loadGroup(taskId)
            .stop()
    }


    /**
     * 暂停所有下载，有bug，待修复
     */
    @Deprecated("有bug，待修复")
    fun pauseAllTask() {
        downloadTask
            .stopAllTask()
    }

    /**
     * 删除任务
     *
     * @param removeFile {@code true} 不仅删除任务数据库记录，还会删除已经完成的文件,总是删除文件
     * {@code false}如果任务已经完成，只删除任务数据库记录
     */
    fun delete(taskId: Long, removeFile: Boolean = false) {
        downloadTask.loadGroup(taskId)
            .cancel(removeFile)
    }

    /**
     * 删除所有下载
     */
    fun deleteAllTask(removeFile: Boolean) {
        downloadTask
            .removeAllTask(removeFile)
    }

    /**
     * 判断任务是否存在
     */
    fun taskExists(urls: MutableList<String>): Boolean {
        return downloadTask
            .taskExists(urls)
    }

    /**
     * 根据id查询下载任务
     */
    fun getDownloadTask(id: Long?): DownloadGroupEntity? {
        id ?: return null
        return downloadTask.getGroupEntity(id)
    }

    /**
     * 根据下载地址查询下载任务
     */
    fun getDownloadTask(urls: MutableList<String>?): DownloadGroupEntity? {
        urls ?: return null
        return downloadTask.getGroupEntity(urls)
    }

    /**
     * 存在就继续下载
     */
    fun startOrResume(
        url: MutableList<String>,
        filePath: String,
        dirSize: Long? = null,
        block: ((build: GroupBuilderTarget) -> Unit)? = null
    ): Long {
        val allDownload = allTask
        var taskId: Long? = null
        allDownload?.let { tasks ->
            tasks.forEach {
                if (it.urls.toStr() == url.toStr()) {
                    taskId = it.id
                    if (it.state != IEntity.STATE_COMPLETE) {
                        resume(it.id, dirSize)
                    }
                    return@let
                }
            }
        }
        if (taskId == null) {
            taskId =
                start(url, filePath, dirSize, block)
        }
        return taskId!!
    }

    /**
     * 添加监听器
     * id：数据库id
     */
    fun addListener(id: Long?, listener: Listener) {
        DownloadGroupTaskStatus.listenerMap[id] = listener
    }

    /**
     * 添加监听器
     * lifecycleOwner 生命周期对象，将监听器绑定生命周期
     */
    fun addListener(lifecycleOwner: LifecycleOwner?, listener: LifecycleListener) {
        lifecycleOwner?.lifecycle?.addObserver(listener)
        DownloadGroupTaskStatus.listenerMap[listener.id] = listener
    }


    /**
     * 移除监听器
     */
    fun removeListener(id: Long?) {
        DownloadGroupTaskStatus.listenerMap.remove(id)
    }

    /**
     * 移除监听器
     */
    fun removeListener(lifecycleOwner: LifecycleOwner?, id: Long?) {
        val listener = getListener(id) ?: return
        if (listener is ILifecycleObserver) {
            lifecycleOwner?.lifecycle?.removeObserver(listener)
        }
        DownloadGroupTaskStatus.listenerMap.remove(id)
    }

    /**
     * 移除监听器
     */
    fun removeListener(lifecycleOwner: LifecycleOwner?, listener: LifecycleListener) {
        lifecycleOwner?.lifecycle?.removeObserver(listener)
        DownloadGroupTaskStatus.listenerMap.remove(listener.id)
    }

    /**
     * 查询监听器
     */
    fun getListener(id: Long?): Listener? {
        return DownloadGroupTaskStatus.listenerMap[id]
    }

    /**
     * 带有生命周期的监听，如果设置了监听，在onStop的时候自动移除，onStart的时候自动监听，防止内存泄漏
     * key：下载时为下载链接地址，上传时为文件地址
     * 如果key发生了变化，请移除此监听，添加新的监听
     */
    open class LifecycleListener(val id: Long) : Listener, ILifecycleObserver {
        private var hasListener = false
        override fun onStart() {
            super.onStart()
            start()
        }

        override fun onStop() {
            super.onStop()
            stop()
        }

        private fun stop() {
            //判断当前时候正在监听
            hasListener = getListener(id) != null
            removeListener(id)
        }

        private fun start() {
            //如果退出的时候添加了监听，则自动添加监听
            if (hasListener) {
                hasListener = false
                addListener(id, this)
            }
        }
    }

    /**
     * 监听器
     */
    interface Listener : DownloadGroupTaskListener {

        override fun onWait(task: DownloadGroupTask) {

        }

        override fun onPre(task: DownloadGroupTask) {
        }

        override fun onTaskPre(task: DownloadGroupTask) {
        }

        override fun onTaskResume(task: DownloadGroupTask) {
        }

        override fun onTaskStart(task: DownloadGroupTask) {
        }

        override fun onTaskStop(task: DownloadGroupTask) {
        }

        override fun onTaskCancel(task: DownloadGroupTask) {
        }

        override fun onTaskFail(task: DownloadGroupTask, e: Exception?) {
        }

        override fun onTaskComplete(task: DownloadGroupTask) {
        }

        override fun onTaskRunning(task: DownloadGroupTask) {
        }

        override fun onNoSupportBreakPoint(task: DownloadGroupTask) {
        }
    }
}
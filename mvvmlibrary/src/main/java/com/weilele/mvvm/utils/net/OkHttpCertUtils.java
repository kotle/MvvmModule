package com.weilele.mvvm.utils.net;

import android.annotation.SuppressLint;

import com.weilele.mvvm.MvvmConf;

import java.io.File;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.CookieJar;
import okhttp3.OkHttpClient;

import static com.weilele.mvvm.MvvmLibKt.app;

public class OkHttpCertUtils {
    /**
     * 添加证书相关设置
     */
    public static OkHttpClient.Builder createCertOkHttpBuilder() {
        return createCertOkHttpBuilder(MvvmConf.INSTANCE.getEnableOkHttpTrustAllCertificates(),
                MvvmConf.INSTANCE.getEnableOkHttpCookieJar(), MvvmConf.INSTANCE.getOkHttpCacheSize());
    }

    public static OkHttpClient.Builder createCertOkHttpBuilder(boolean enableCert, boolean enableCookieJar, long cacheSize) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (enableCert) {
            //下面两行信任所有证书
            builder.sslSocketFactory(createSSLSocketFactory(), new TrustAllManager());
            builder.hostnameVerifier(new TrustAllHostnameVerifier());
        }
        if (cacheSize > 0) {
            //如果缓存的长度大于0，设置缓存
            builder.cache(new Cache(new File(app.getExternalCacheDir(), "okhttpcache"), cacheSize));
        }
        if (enableCookieJar) {
            CookieJar customCookie = MvvmConf.INSTANCE.getCustomOkHttpCookieJar();
            if (customCookie == null) {
                builder.cookieJar(new SimpleCookieJar());
            } else {
                builder.cookieJar(customCookie);
            }
        }
        return builder;
    }

    /**
     * 默认信任所有的证书
     * 最好加上证书认证，主流App都有自己的证书
     */
    @SuppressLint("TrulyRandom")
    private static SSLSocketFactory createSSLSocketFactory() {
        SSLSocketFactory sSLSocketFactory = null;
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, new TrustManager[]{new TrustAllManager()},
                    new SecureRandom());
            sSLSocketFactory = sc.getSocketFactory();
        } catch (Exception e) {
            if (MvvmConf.INSTANCE.isDebug()) {
                e.printStackTrace();
            }
        }
        return sSLSocketFactory;
    }

    @SuppressLint("TrustAllX509TrustManager")
    private static class TrustAllManager implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) {
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    @SuppressLint("BadHostnameVerifier")
    private static class TrustAllHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}

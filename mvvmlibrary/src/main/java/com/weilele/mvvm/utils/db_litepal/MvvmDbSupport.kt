package com.weilele.mvvm.utils.db_litepal

import org.litepal.crud.LitePalSupport

open class MvvmDbSupport : LitePalSupport() {
    /**
     * 数据库id，数据操作的唯一标识
     * 可通过此id进行增删改查
     */
    var dbId: Long
        get() = baseObjId
        set(value) {
            assignBaseObjId(value)
        }
}
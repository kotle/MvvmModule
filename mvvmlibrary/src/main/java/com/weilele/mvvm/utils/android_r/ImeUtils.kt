package com.weilele.mvvm.utils.android_r

import android.app.Activity
import android.content.res.Resources
import android.graphics.Insets
import android.os.Build
import android.os.CancellationSignal
import android.view.*
import android.view.animation.LinearInterpolator
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import com.weilele.mvvm.utils.activity.getActivity
import com.weilele.mvvm.utils.activity.getScreenInfo
import com.weilele.mvvm.utils.activity.onClick
import com.weilele.mvvm.utils.logE

@RequiresApi(Build.VERSION_CODES.R)
fun Activity.testSize() {
    val view = window.decorView.findViewById<FrameLayout>(android.R.id.content)
    window.setDecorFitsSystemWindows(false)
    view.setOnApplyWindowInsetsListener { v, insets ->
        //状态栏
        val statusBars = insets.getInsets(WindowInsets.Type.statusBars())
        //导航栏
        val navigationBars = insets.getInsets(WindowInsets.Type.navigationBars())
        //等绘制完成再去获取就是 true，这个稍微比较坑一点
        val navigationBarsVisible = insets.isVisible(WindowInsets.Type.navigationBars())
        //键盘
        val ime = insets.getInsets(WindowInsets.Type.ime())
        val info = getScreenInfo()
        insets
    }

    view.setWindowInsetsAnimationCallback(object :
        WindowInsetsAnimation.Callback(DISPATCH_MODE_CONTINUE_ON_SUBTREE) {
        override fun onProgress(
            insets: WindowInsets,
            runningAnimations: MutableList<WindowInsetsAnimation>
        ): WindowInsets {
            logE {
                "ime:" + insets.getInsets(WindowInsets.Type.ime()).top +
                        " --- " + insets.getInsets(WindowInsets.Type.ime()).bottom
            }
            return insets
        }

        override fun onStart(
            animation: WindowInsetsAnimation,
            bounds: WindowInsetsAnimation.Bounds
        ): WindowInsetsAnimation.Bounds {
            return super.onStart(animation, bounds)
        }

        override fun onEnd(animation: WindowInsetsAnimation) {
            super.onEnd(animation)
        }

        override fun onPrepare(animation: WindowInsetsAnimation) {
            super.onPrepare(animation)
        }
    })
    view.windowInsetsController?.controlWindowInsetsAnimation(
        WindowInsets.Type.ime(),
        -1,
        LinearInterpolator(),
        // A cancellation signal, which allows us to cancel the request to control
        CancellationSignal(),
        // The WindowInsetsAnimationControlListener
        object : WindowInsetsAnimationControlListener {
            override fun onReady(controller: WindowInsetsAnimationController, types: Int) {
                view.onClick {
                    controller.setInsetsAndAlpha(Insets.of(0, 0, 0, 200), 0.5f, 0.5f)
                }
            }

            override fun onFinished(controller: WindowInsetsAnimationController) {

            }

            override fun onCancelled(controller: WindowInsetsAnimationController?) {
            }
        }
    )
}

fun Window.getContentView(): FrameLayout = decorView.findViewById(android.R.id.content)

/**
 * 全屏activity监听
 */
fun Window?.fullScreen(
    isFullScreen: Boolean,
    enableCutoutEdges: Boolean = true,/*允许绘制到耳朵区域*/
    statusBar: Boolean = true,/*隐藏状态栏*/
    navigationBar: Boolean = true,/*隐藏导航栏*/
    fitsSystemWindows: Boolean = !isFullScreen
) {
    val window = this ?: return
    val insetsController = WindowCompat.getInsetsController(this, decorView) ?: return
    val lp = window.attributes
    if (isFullScreen) {
        // 延伸显示区域到耳朵区
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            lp.layoutInDisplayCutoutMode = if (enableCutoutEdges) {
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
            } else {
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_DEFAULT
            }
            window.attributes = lp
        }
        insetsController.apply {
            systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
            if (statusBar) {
                hide(WindowInsetsCompat.Type.statusBars())
            }
            if (navigationBar) {
                hide(WindowInsetsCompat.Type.navigationBars())
            }
        }
    } else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            lp.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_DEFAULT
            window.attributes = lp
        }
        insetsController.apply {
            if (statusBar) {
                show(WindowInsetsCompat.Type.statusBars())
            }
            if (navigationBar) {
                show(WindowInsetsCompat.Type.navigationBars())
            }
        }
    }
    WindowCompat.setDecorFitsSystemWindows(this@fullScreen, fitsSystemWindows)
}

fun View?.fullScreen(
    isFullScreen: Boolean,
    enableCutoutEdges: Boolean = true,/*允许绘制到耳朵区域*/
    statusBar: Boolean = true,/*隐藏状态栏*/
    navigationBar: Boolean = true,/*隐藏导航栏*/
    fitsSystemWindows: Boolean = !isFullScreen
) {
    this.getActivity().fullScreen(
        isFullScreen,
        enableCutoutEdges,
        statusBar,
        navigationBar,
        fitsSystemWindows
    )
}

fun Activity?.fullScreen(
    isFullScreen: Boolean,
    enableCutoutEdges: Boolean = true,/*允许绘制到耳朵区域*/
    statusBar: Boolean = true,/*隐藏状态栏*/
    navigationBar: Boolean = true,/*隐藏导航栏*/
    fitsSystemWindows: Boolean = !isFullScreen
) {
    this?.window?.fullScreen(
        isFullScreen,
        enableCutoutEdges,
        statusBar,
        navigationBar,
        fitsSystemWindows
    )
}

/**
 * 是否竖屏
 * 认为 宽度<长度  即为竖屏，否则为横屏
 */
fun Resources.isPortraitScreen(): Boolean {
    val d = this.displayMetrics
    return d.widthPixels <= d.heightPixels
}

fun View.isPortraitScreen(): Boolean {
    return resources.isPortraitScreen()
}

fun Activity.isPortraitScreen(): Boolean {
    return resources.isPortraitScreen()
}

fun Fragment.isPortraitScreen(): Boolean {
    return this.resources.isPortraitScreen()
}
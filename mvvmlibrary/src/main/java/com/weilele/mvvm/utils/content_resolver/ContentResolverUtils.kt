package com.weilele.mvvm.utils.content_resolver

import android.database.Cursor
import android.net.Uri
import android.os.CancellationSignal
import android.provider.MediaStore
import com.weilele.mvvm.app
import com.weilele.mvvm.utils.file.isExternalStorageLegacy

/**
 * 查询公共数据库
 */
inline fun contentResolverQuery(
    uri: Uri,
    projection: Array<String>? = null,
    selection: String? = null,
    selectionArgs: Array<String>? = null,
    sortOrder: String? = null,
    cancellationSignal: CancellationSignal? = null,
    beforeBlock: ((Cursor) -> Unit) = {},
    eachBlock: ((Cursor) -> Unit)
) {
    app.contentResolver.query(
        uri,
        projection,
        selection,
        selectionArgs,
        sortOrder,
        cancellationSignal
    )?.let {
        beforeBlock.invoke(it)
        if (it.moveToNext()) {
            eachBlock.invoke(it)
        }
        it.close()
    }
}

/**
 * 查询视频类型
 */
inline fun contentResolverQueryVideo(
    projection: Array<String>? = null,
    selection: String? = null,
    selectionArgs: Array<String>? = null,
    sortOrder: String? = null,
    cancellationSignal: CancellationSignal? = null,
    beforeBlock: ((Cursor) -> Unit) = {},
    eachBlock: ((Cursor) -> Unit)
) {
    contentResolverQuery(
        MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
        projection, selection, selectionArgs, sortOrder, cancellationSignal, beforeBlock, eachBlock
    )
}

/**
 * 查询图片类型
 */
inline fun contentResolverQueryImages(
    projection: Array<String>? = null,
    selection: String? = null,
    selectionArgs: Array<String>? = null,
    sortOrder: String? = null,
    cancellationSignal: CancellationSignal? = null,
    beforeBlock: ((Cursor) -> Unit) = {},
    eachBlock: ((Cursor) -> Unit)
) {
    contentResolverQuery(
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
        projection, selection, selectionArgs, sortOrder, cancellationSignal, beforeBlock, eachBlock
    )
}

/**
 * 查询音频类型
 */
inline fun contentResolverQueryAudio(
    projection: Array<String>? = null,
    selection: String? = null,
    selectionArgs: Array<String>? = null,
    sortOrder: String? = null,
    cancellationSignal: CancellationSignal? = null,
    beforeBlock: ((Cursor) -> Unit) = {},
    eachBlock: ((Cursor) -> Unit)
) {
    contentResolverQuery(
        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
        projection, selection, selectionArgs, sortOrder, cancellationSignal, beforeBlock, eachBlock
    )
}

/**
 * 查询下载类型
 */
inline fun contentResolverQueryDownloads(
    projection: Array<String>? = null,
    selection: String? = null,
    selectionArgs: Array<String>? = null,
    sortOrder: String? = null,
    cancellationSignal: CancellationSignal? = null,
    beforeBlock: ((Cursor) -> Unit) = {},
    eachBlock: ((Cursor) -> Unit)
) {
    contentResolverQuery(
        if (!isExternalStorageLegacy) {
            MediaStore.Downloads.EXTERNAL_CONTENT_URI
        } else {
            MediaStore.Files.getContentUri("external")
        },
        projection, selection, selectionArgs, sortOrder, cancellationSignal, beforeBlock, eachBlock
    )
}

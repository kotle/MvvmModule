package com.weilele.mvvm.utils.net.gson_adapter;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.weilele.mvvm.MvvmLibKt;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;


/**
 * A {@linkplain Converter.Factory converter} which uses Gson for JSON.
 *
 * <p>Because Gson is so flexible in the types it supports, this converter assumes that it can
 * handle all types. If you are mixing JSON serialization with something else (such as protocol
 * buffers), you must {@linkplain Retrofit.Builder#addConverterFactory(Converter.Factory) add this
 * instance} last to allow the other converters a chance to see their types.
 */
public class MyGsonConverterFactory<T> extends Converter.Factory {
    /**
     * Create an instance using a default {@link Gson} instance for conversion. Encoding to JSON and
     * decoding from JSON (when no charset is specified by a header) will use UTF-8.
     */
    public static <T> MyGsonConverterFactory<T> create(Function3<T, Throwable,Object, Object> resultCall) {
        return create(new Gson(), resultCall);
    }


    /**
     * Create an instance using {@code gson} for conversion. Encoding to JSON and decoding from JSON
     * (when no charset is specified by a header) will use UTF-8.
     */
    @SuppressWarnings("ConstantConditions") // Guarding public API nullability.
    public static <T> MyGsonConverterFactory<T> create(Gson gson, Function3<T, Throwable,Object, Object> resultCall) {
        if (gson == null) throw new NullPointerException("gson == null");
        return new MyGsonConverterFactory<>(gson, resultCall);
    }

    private final Gson gson;
    private final Function3<T, Throwable,Object, Object> resultCall;

    private MyGsonConverterFactory(Gson gson, Function3<T, Throwable,Object, Object> resultCall) {
        this.gson = gson;
        this.resultCall = resultCall;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(
            Type type, Annotation[] annotations, Retrofit retrofit) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new GsonResponseBodyConverter(gson, adapter, resultCall);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(
            Type type,
            Annotation[] parameterAnnotations,
            Annotation[] methodAnnotations,
            Retrofit retrofit) {
        TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
        return new GsonRequestBodyConverter<>(gson, adapter);
    }
}


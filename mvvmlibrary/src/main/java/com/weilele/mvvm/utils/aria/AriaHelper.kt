package com.weilele.mvvm.utils.aria

import com.arialyy.aria.core.Aria
import com.weilele.mvvm.app
import com.weilele.mvvm.utils.aria.download.MvvmDownload
import com.weilele.mvvm.utils.aria.download_group.MvvmDownloadGroup
import com.weilele.mvvm.utils.aria.upload.MvvmUpload

object AriaHelper {
    init {
        Aria.init(app)
    }

    /**
     * aria对象
     */
    val aria by lazy { Aria.get(app) }

    /**
     * 下载对象
     */
    val download by lazy { Aria.download(MvvmDownload) }

    /**
     * 下载群组
     */
    val downloadGroup by lazy { Aria.download(MvvmDownloadGroup) }

    /**
     * 上传任务
     */
    val upload by lazy { Aria.upload(MvvmUpload) }

}
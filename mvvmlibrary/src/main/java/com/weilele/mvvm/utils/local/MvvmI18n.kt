package com.weilele.mvvm.utils.local

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.LocaleList
import java.util.*


fun Context?.updateLocal(languageTag: String?): Context? {
    languageTag ?: return this
    this ?: return this
    return createConfigurationContext(updateLocal(resources.configuration, languageTag))
}

private fun updateLocal(configuration: Configuration, local: Locale): Configuration {
    configuration.setLocale(local)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        val locals = LocaleList(local)
        configuration.setLocales(locals)
    }
    return configuration
}

private fun updateLocal(configuration: Configuration, languageTag: String): Configuration {
    return updateLocal(configuration, Locale.forLanguageTag(languageTag))
}
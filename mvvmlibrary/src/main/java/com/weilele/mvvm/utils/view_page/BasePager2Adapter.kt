package com.weilele.mvvm.utils.view_page

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2

fun ViewPager2.simplePagerAdapter(
    fm: FragmentManager,
    lifecycle: Lifecycle,
    itemCount: Int,
    onCreateItem: Function1<@ParameterName("position") Int, Fragment>
): FragmentStateAdapter {
    val baseAdapter = object : FragmentStateAdapter(fm, lifecycle) {
        override fun getItemCount(): Int = itemCount

        override fun createFragment(position: Int): Fragment {
            return onCreateItem.invoke(position)
        }
    }
    if (adapter == null) {
        adapter = baseAdapter
    }
    return baseAdapter
}

fun <T> ViewPager2.simplePagerAdapter(
    fm: FragmentManager,
    lifecycle: Lifecycle,
    datas: MutableList<T>,
    onCreateItem: Function2<@ParameterName("position") Int, T, Fragment>
): FragmentStateAdapter {
    val baseAdapter = object : FragmentStateAdapter(fm, lifecycle) {
        override fun getItemCount(): Int = datas.count()

        override fun createFragment(position: Int): Fragment {
            return onCreateItem.invoke(position, datas[position])
        }
    }
    if (adapter == null) {
        adapter = baseAdapter
    }
    return baseAdapter
}

/**
 * 适配器
 */
fun ViewPager2.simplePagerAdapter(
    activity: AppCompatActivity,
    itemCount: Int,
    onCreateItem: Function1<@ParameterName("position") Int, Fragment>
): FragmentStateAdapter {
    return simplePagerAdapter(
        activity.supportFragmentManager,
        activity.lifecycle,
        itemCount,
        onCreateItem
    )
}

fun ViewPager2.simplePagerAdapter(
    fragment: Fragment,
    itemCount: Int,
    onCreateItem: Function1<@ParameterName("position") Int, Fragment>
): FragmentStateAdapter {
    return simplePagerAdapter(
        fragment.childFragmentManager,
        fragment.lifecycle,
        itemCount,
        onCreateItem
    )
}

fun <T> ViewPager2.simplePagerAdapter(
    activity: AppCompatActivity,
    datas: MutableList<T>,
    onCreateItem: Function2<@ParameterName("position") Int, T, Fragment>
): FragmentStateAdapter {
    return simplePagerAdapter(
        activity.supportFragmentManager,
        activity.lifecycle,
        datas,
        onCreateItem
    )
}

fun <T> ViewPager2.simplePagerAdapter(
    fragment: Fragment,
    datas: MutableList<T>,
    onCreateItem: Function2<@ParameterName("position") Int, T, Fragment>
): FragmentStateAdapter {
    return simplePagerAdapter(
        fragment.childFragmentManager,
        fragment.lifecycle,
        datas,
        onCreateItem
    )
}



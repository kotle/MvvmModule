package com.weilele.mvvm.utils.`object`

import android.app.Activity
import android.content.res.Resources
import android.util.DisplayMetrics
import com.weilele.mvvm.MvvmConf
import com.weilele.mvvm.app
import kotlin.math.max
import kotlin.math.min

/**
 * 描述：屏幕适配,app的尺寸不做处理，不做适配，只对activity适配
 * 可以根据需要来觉得是否适配
 */
class ScreenAdaptation(
    private val enableScaleFontBySystem: Boolean = MvvmConf.enableScaleFontBySystem,/*字体是否受系统设置大小影响*/
    private val screenAdaptationByWidth: Boolean = MvvmConf.screenAdaptationByWidth,/*是否以短边进行适配*/
    private val screenAdaptationUiDp: Int = MvvmConf.screenAdaptationUiDp,/*适配的边，总长度*/
    private val scaledDensityScaleFactor: Float = MvvmConf.scaledDensityScaleFactor,/*字体调节因子*/
) {
    /**
     * 每次取最新值，横竖屏切换会导致配置发生变化
     */
    private val appDisplayMetrics
        get() = app.resources.displayMetrics

    /**
     * 适配前
     * 设备的像素密度值 160dp==1  320dp==2 厂商可以更改此值
     */
    private val appDensity
        get() = appDisplayMetrics.density
    private val appScaledDensity
        get() = appDisplayMetrics.scaledDensity
    private val appXdpi
        get() = appDisplayMetrics.xdpi
    private val appYdpi
        get() = appDisplayMetrics.ydpi

    /**
     * 适配前的字体缩放因子
     */
    private val customFontScaleDensity
        get() = appScaledDensity / (if (enableScaleFontBySystem) 1f else app.resources.configuration.fontScale)

    /**
     * 适配后缩放因子
     * 设备的像素密度值 160dp==1  320dp==2
     * 根据[MvvmConf.screenAdaptationUiDpWidth]计算
     */
    val density
        get() = (if (screenAdaptationByWidth)
            min(
                appDisplayMetrics.widthPixels.toFloat(),
                appDisplayMetrics.heightPixels.toFloat()
            )
        else
            max(
                appDisplayMetrics.widthPixels.toFloat(),
                appDisplayMetrics.heightPixels.toFloat()
            )) / screenAdaptationUiDp

    /**
     * 屏幕适配后的字体缩放因子
     * 因为适配之后的字体总是偏大，所以设置[MvvmConf.scaledDensityScaleFactor](默认0.96)使他小一点
     */
    val scaledDensity
        get() = density * (customFontScaleDensity / appDensity) * scaledDensityScaleFactor

    val xdpi
        get() = (density / appDensity) * appXdpi

    val ydpi
        get() = (density / appDensity) * appYdpi

    /**
     * 屏幕适配后的Dpi
     */
    val densityDpi
        get() = (160 * density).toInt()

    /**
     * 更改resources的值为屏幕适配结果
     */
    fun setCustomDensity(activity: Activity?, enable: Boolean) {
        setCustomDensity(activity?.resources, enable)
    }

    /**
     * 更改resources的值为屏幕适配结果
     */
    fun setCustomDensity(resources: Resources?, enable: Boolean) {
        resources ?: return
        val activityDm = resources.displayMetrics
        if (enable) {
            activityDm.density = density
            activityDm.scaledDensity = scaledDensity
            activityDm.densityDpi = densityDpi
            activityDm.xdpi = xdpi
            activityDm.ydpi = ydpi
        } else {
            activityDm.setTo(appDisplayMetrics)
        }
    }
}
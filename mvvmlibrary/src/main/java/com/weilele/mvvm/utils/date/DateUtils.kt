package com.weilele.mvvm.utils.date

import java.util.*

/**
 * 时间戳转为日历对象
 */
fun Long?.toCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    this ?: return calendar
    calendar.timeInMillis = this
    return calendar
}
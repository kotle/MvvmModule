package com.weilele.mvvm.utils.activity

import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.annotation.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.weilele.mvvm.MvvmConf
import com.weilele.mvvm.app
import com.weilele.mvvm.base.livedata.appCompatActivity

private fun Any?.getContextFromAny(): Context {
    return when (this) {
        is Fragment -> {
            context ?: app
        }
        is View -> {
            context ?: app
        }
        is Context -> {
            this
        }
        else -> {
            app
        }
    }
}

/**
 * 获取颜色值
 */
fun Any?.getResColor(@ColorRes colorRes: Int): Int {
    return ContextCompat.getColor(getContextFromAny(), colorRes)
}

/**
 * 设置颜色
 */
fun TextView.setTextColorResource(@ColorRes colorRes: Int) {
    setTextColor(ContextCompat.getColor(context, colorRes))
}

/**
 * 获取字符串
 */
fun Any?.getResString(@StringRes stringRes: Int): CharSequence {
    return getContextFromAny().getString(stringRes)
}

/**
 * 获取字符串
 */
fun Any?.getResText(@StringRes stringRes: Int): CharSequence {
    return getContextFromAny().getText(stringRes)
}

/**
 * 获取图片
 */
fun Any?.getResDrawable(@DrawableRes drawableRes: Int): Drawable? {
    return AppCompatResources.getDrawable(getContextFromAny(), drawableRes)
}

/**
 * 获取图片
 */
fun Any?.getResColorStateList(@ColorRes colorRes: Int): ColorStateList? {
    return AppCompatResources.getColorStateList(getContextFromAny(), colorRes)
}

/**
 * 资源文件的bool值
 */
fun Any?.getResBool(@BoolRes boolRes: Int): Boolean {
    return getContextFromAny().resources.getBoolean(boolRes)
}

/**
 * 资源文件的像素值（dp->px）
 * 返回绝对像素 px
 *  getDimension()是基于当前DisplayMetrics进行转换，获取指定资源id对应的尺寸。
 *                文档里并没说这里返回的就是像素，要注意这个函数的返回值是float，像素肯定是int。
 *  getDimensionPixelSize()与getDimension()功能类似，
 *                不同的是将结果转换为int，并且小数部分四舍五入。
 *  getDimensionPixelOffset()与getDimension()功能类似，
 *                不同的是将结果转换为int，并且偏移转换（offset conversion，函数命名中的offset是这个意思）
 *                 是直接截断小数位，即取整（其实就是把float强制转化为int，注意不是四舍五入哦
 */
fun Any?.getResDimension(@DimenRes dimenRes: Int): Float {
    return getContextFromAny().resources.getDimension(dimenRes)
}

fun Any?.getResDimensionPixelSize(@DimenRes dimenRes: Int): Int {
    return getContextFromAny().resources.getDimensionPixelSize(dimenRes)
}

data class ScreenInfo(
    val statusBarHeight: Int,
    val navigationBarHeight: Int,
    val statusBarShow: Boolean,
    val navigationBarShow: Boolean,
    val screenWidth: Int,
    val screenHeight: Int,
)

/**
 * 获取屏幕相关信息
 */
fun Activity.getScreenInfo(): ScreenInfo {
    return window.getScreenInfo()
}

/**
 * 获取屏幕相关信息
 */
fun Fragment.getScreenInfo(): ScreenInfo {
    return appCompatActivity!!.getScreenInfo()
}

/**
 * 获取屏幕相关信息
 */
fun Window.getScreenInfo(): ScreenInfo {
    val rect = Rect()
    decorView.getWindowVisibleDisplayFrame(rect)
    val statusBarHeight = rect.top
    val size = getScreenSize(false)
    val navigationBarHeight = size.y - rect.bottom
    return ScreenInfo(
        statusBarHeight, navigationBarHeight,
        statusBarHeight > 0,
        navigationBarHeight > 0,
        size.x, size.y
    )
}

fun logStackTrace(msg: String?) {
    if (MvvmConf.isDebug) {
        Throwable("调用堆栈打印------------->$msg").printStackTrace()
    }
}

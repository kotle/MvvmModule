package com.weilele.mvvm.utils.glide

import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

/**
 * glide加载图片监听
 */
class SimpleGlideLoadListener<TranscodeType>(private val onResourceReady: Function2<@ParameterName("result") TranscodeType?, @ParameterName("error") GlideException?, Unit>) :
        RequestListener<TranscodeType> {
    override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<TranscodeType>?,
            isFirstResource: Boolean
    ): Boolean {
        onResourceReady.invoke(null, e)
        return false
    }

    override fun onResourceReady(
            resource: TranscodeType,
            model: Any?,
            target: Target<TranscodeType>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
    ): Boolean {
        onResourceReady.invoke(resource, null)
        return false
    }
}
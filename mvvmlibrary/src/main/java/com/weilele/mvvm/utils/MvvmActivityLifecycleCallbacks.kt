package com.weilele.mvvm.utils

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.weilele.mvvm.setScreenAdaptation

class MvvmActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {
    companion object {
        //启动的activity集合
        internal val _activityList = mutableListOf<Activity>()
    }

    /*********************Created******************/
    override fun onActivityPreCreated(activity: Activity, savedInstanceState: Bundle?) {
        setScreenAdaptation(activity)
        super.onActivityPreCreated(activity, savedInstanceState)
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        setScreenAdaptation(activity)
        _activityList.add(activity)
    }

    override fun onActivityPostCreated(activity: Activity, savedInstanceState: Bundle?) {
        setScreenAdaptation(activity)
        super.onActivityPostCreated(activity, savedInstanceState)
    }

    /*********************Started******************/
    override fun onActivityPreStarted(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPreStarted(activity)
    }

    override fun onActivityStarted(activity: Activity) {
        setScreenAdaptation(activity)
    }


    override fun onActivityPostStarted(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPostStarted(activity)
    }

    /*********************Resumed******************/
    override fun onActivityPreResumed(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPreResumed(activity)
    }

    override fun onActivityResumed(activity: Activity) {
        setScreenAdaptation(activity)
    }

    override fun onActivityPostResumed(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPostResumed(activity)
    }

    /*********************Paused******************/
    override fun onActivityPrePaused(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPrePaused(activity)
    }

    override fun onActivityPaused(activity: Activity) {
        setScreenAdaptation(activity)
    }


    override fun onActivityPostPaused(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPostPaused(activity)
    }

    /*********************Stopp******************/
    override fun onActivityPreStopped(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPreStopped(activity)
    }

    override fun onActivityStopped(activity: Activity) {
        setScreenAdaptation(activity)
    }

    override fun onActivityPostStopped(activity: Activity) {
        setScreenAdaptation(activity)
        super.onActivityPostStopped(activity)
    }

    /*********************Destroyed******************/
    override fun onActivityPreDestroyed(activity: Activity) {
        super.onActivityPreDestroyed(activity)
    }

    override fun onActivityDestroyed(activity: Activity) {
        _activityList.remove(activity)
    }

    override fun onActivityPostDestroyed(activity: Activity) {
        super.onActivityPostDestroyed(activity)
    }

    /*********************SaveInstanceState******************/
    override fun onActivityPreSaveInstanceState(activity: Activity, outState: Bundle) {
        setScreenAdaptation(activity)
        super.onActivityPreSaveInstanceState(activity, outState)
    }


    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        setScreenAdaptation(activity)
    }

    override fun onActivityPostSaveInstanceState(activity: Activity, outState: Bundle) {
        setScreenAdaptation(activity)
        super.onActivityPostSaveInstanceState(activity, outState)
    }
}
package com.weilele.mvvm.utils.string

import java.lang.StringBuilder

/**
 * list转为string
 */
fun <T> List<T>?.toStr(split: String = ",", transform: Function1<T, String> = { it.toString() }): String {
    this ?: return ""
    val sb = StringBuilder()
    val count = this.count()
    this.forEachIndexed { index, t ->
        sb.append(transform.invoke(t))
        if (index + 1 != count) {
            sb.append(split)
        }
    }
    return sb.toString()
}

/**
 * 数组转str
 */
fun <T> Array<T>?.toStr(split: String = ",", transform: Function1<T, String> = { it.toString() }): String {
    this ?: return ""
    val sb = StringBuilder()
    val count = this.count()
    this.forEachIndexed { index, t ->
        sb.append(transform.invoke(t))
        if (index + 1 != count) {
            sb.append(split)
        }
    }
    return sb.toString()
}
package com.weilele.mvvm.utils.aria.download

import com.arialyy.aria.core.Aria
import com.arialyy.aria.core.download.DownloadTaskListener
import com.arialyy.aria.core.task.DownloadTask
import java.lang.Exception

/**
 * 任务状态类
 */
object DownloadTaskStatus : DownloadTaskListener {
    init {
        register()
    }

    internal val listenerMap = hashMapOf<Long?, MvvmDownload.Listener>()

    override fun onWait(task: DownloadTask) {
        getListener(task)?.onWait(task)
    }

    override fun onPre(task: DownloadTask) {
        getListener(task)?.onPre(task)
    }

    override fun onTaskPre(task: DownloadTask) {
        getListener(task)?.onTaskPre(task)
    }

    override fun onTaskResume(task: DownloadTask) {
        getListener(task)?.onTaskResume(task)
    }

    override fun onTaskStart(task: DownloadTask) {
        getListener(task)?.onTaskStart(task)
    }

    override fun onTaskStop(task: DownloadTask) {
        getListener(task)?.onTaskStop(task)
    }

    override fun onTaskCancel(task: DownloadTask) {
        getListener(task)?.onTaskCancel(task)
    }

    override fun onTaskFail(task: DownloadTask?, e: Exception) {
        if (task != null) {
            getListener(task)?.onTaskFail(task, e)
        }
    }

    override fun onTaskComplete(task: DownloadTask) {
        getListener(task)?.onTaskComplete(task)
    }

    override fun onTaskRunning(task: DownloadTask) {
        getListener(task)?.onTaskRunning(task)
    }

    override fun onNoSupportBreakPoint(task: DownloadTask) {
        getListener(task)?.onNoSupportBreakPoint(task)
    }

    private fun getListener(task: DownloadTask): MvvmDownload.Listener? {
        return listenerMap[task.entity.id]
    }

    /**
     * 注册
     */
    fun register() {
        Aria.download(this).register()
    }

    /**
     * 取消注册
     */
    fun unRegister() {
        Aria.download(this).unRegister()
    }
}
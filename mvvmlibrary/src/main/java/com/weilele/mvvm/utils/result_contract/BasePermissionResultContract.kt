package com.weilele.mvvm.utils.result_contract

import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.weilele.mvvm.base.MvvmActivity
import com.weilele.mvvm.base.MvvmDialog
import com.weilele.mvvm.base.MvvmFragment
import com.weilele.mvvm.base.helper.ILifecycleObserver


class PermissionForResultHelper : ILifecycleObserver, ActivityResultCallback<Map<String, Boolean>> {
    companion object {
        operator fun invoke(appCompatActivity: AppCompatActivity): PermissionForResultHelper {
            return PermissionForResultHelper().apply {
                appCompatActivity.lifecycle.addObserver(this)
            }
        }

        operator fun invoke(fragment: Fragment): PermissionForResultHelper {
            return PermissionForResultHelper().apply {
                fragment.lifecycle.addObserver(this)
            }
        }
    }

    private var launcher: ActivityResultLauncher<Array<String>>? = null

    private var resultListener: ((Map<String, Boolean>) -> Unit)? = null

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        launcher = when (owner) {
            is AppCompatActivity -> {
                owner.registerForActivityResult(
                    ActivityResultContracts.RequestMultiplePermissions(),
                    this
                )
            }
            is Fragment -> {
                owner.registerForActivityResult(
                    ActivityResultContracts.RequestMultiplePermissions(),
                    this
                )
            }
            else -> {
                null
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        launcher?.unregister()
        launcher = null
    }


    fun requestPermissions(
        permissions: Array<String>,
        option: ActivityOptionsCompat? = null,
        onResult: (Map<String, Boolean>) -> Unit
    ) {
        resultListener = onResult
        launcher?.launch(permissions, option)
    }

    override fun onActivityResult(result: Map<String, Boolean>) {
        resultListener?.invoke(result)
    }
}

fun MvvmActivity.requestPermissions(
    permissions: Array<String>,
    option: ActivityOptionsCompat? = null,
    onResult: (Map<String, Boolean>) -> Unit
) {
    permissionForResultHelper.requestPermissions(permissions, option, onResult)
}

fun MvvmDialog.requestPermissions(
    permissions: Array<String>,
    option: ActivityOptionsCompat? = null,
    onResult: (Map<String, Boolean>) -> Unit
) {
    permissionForResultHelper.requestPermissions(permissions, option, onResult)
}

fun MvvmFragment.requestPermissions(
    permissions: Array<String>,
    option: ActivityOptionsCompat? = null,
    onResult: (Map<String, Boolean>) -> Unit
) {
    permissionForResultHelper.requestPermissions(permissions, option, onResult)
}





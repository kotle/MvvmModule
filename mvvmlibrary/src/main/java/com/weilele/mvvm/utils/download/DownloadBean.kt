package com.weilele.mvvm.utils.download

import java.io.File

data class DownloadBean(
        val fileName: String,
        val downloadUrl: String,
        val file: File,
        val savePath: String,
        val size: Long,
        val isNewDownloadFile: Boolean/*是新下载还是之前的*/
)
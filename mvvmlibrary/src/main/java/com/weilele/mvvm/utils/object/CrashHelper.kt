package com.weilele.mvvm.utils.`object`

import android.os.Process
import com.weilele.mvvm.MvvmConf
import com.weilele.mvvm.app
import com.weilele.mvvm.base.UncaughtExceptionActivity

/**
 * 全局异常捕获
 */
object CrashHelper : Thread.UncaughtExceptionHandler {

    override fun uncaughtException(t: Thread, e: Throwable) {
        if (!MvvmConf.enableUncaughtExceptionHandler) {
            Thread.getDefaultUncaughtExceptionHandler()?.uncaughtException(t, e)
            return
        }
        UncaughtExceptionActivity.start(app, t, e)
        Process.killProcess(Process.myPid())
        System.exit(-1)
    }
}
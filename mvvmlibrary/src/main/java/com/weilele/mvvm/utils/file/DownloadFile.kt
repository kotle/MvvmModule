package com.weilele.mvvm.utils.file


import kotlinx.coroutines.CoroutineScope
import java.io.File
import java.io.FileOutputStream
import java.lang.IllegalArgumentException

/**
 * 从网络下载图片
 * 不写入共有数据库
 */
fun downloadFile(scope: CoroutineScope, url: String, file: File): DownloadToOutputStream {
    if (file.isDirectory) {
        throw IllegalArgumentException("需要一个文件，但是${file.path}是一个文件夹")
    }
    if (!file.exists()) {
        val fileName = file.path.split(File.separator).last()
        val dir = File(file.path.removeSuffix(fileName))
        if (!dir.exists()) {
            dir.mkdirs()
        }
        file.createNewFile()
    }
    return DownloadToOutputStream(scope, url, FileOutputStream(file))
}

/**
 * 从网络下载图片
 * 不写入共有数据库
 */
fun downloadFile(scope: CoroutineScope, url: String, filePath: String): DownloadToOutputStream {
    return downloadFile(scope, url, File(filePath))
}
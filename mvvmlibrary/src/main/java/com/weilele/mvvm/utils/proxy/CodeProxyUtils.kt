package com.weilele.mvvm.utils.proxy

import java.lang.reflect.Method
import java.lang.reflect.Proxy

@Suppress("UNCHECKED_CAST")
fun <T> Any.codeProxy(
        before: ((proxy: Any?/*代理示例，真正执行逻辑代码的对象*/,any: Any?, method: Method?/*函数*/, args: Array<out Any>?/*参数*/) -> Any?/*返回值，不是null，就代表拦截*/)? = null,
        after: ((call: Any?/*原函数执行后的结果*/) -> Any?/*返回值，不是null，就返回此值*/)? = null,
): T {
    val cls = this.javaClass
    val handler = CodeProxyHelper(this, before, after)
    return Proxy.newProxyInstance(cls.classLoader, cls.interfaces, handler) as T
}
package com.weilele.mvvm.utils.check

import android.widget.TextView
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * 手机号号段校验，
 * 第1位：1；
 * 第2位：{3、4、5、6、7、8、9}任意数字；
 * 第3—11位：0—9任意数字
 * @param value
 * @return
 */
fun String?.isTelPhoneNumber(): Boolean {
    val value = this ?: return false
    if (value.length == 11) {
        val pattern: Pattern = Pattern.compile("^1[3|4|5|6|7|8|9][0-9]\\d{8}$")
        val matcher: Matcher = pattern.matcher(value)
        return matcher.matches()
    }
    return false
}

fun TextView?.isTelPhoneNumber(): Boolean = this?.text?.toString().isTelPhoneNumber()

/**
 * 验证输入的名字是否为“中文”或者是否包含“·”
 * 验证规则是：姓名由汉字或汉字加“•”或"·"组成，而且，“点”只能有一个，“点”的位置不能在首位也不能在末尾，只有在汉字之间才会验证通过。
 */
fun String?.isLegalName(): Boolean {
    val name = this ?: return false
    return if (name.contains("·") || name.contains("•")) {
        name.matches(Regex("^[\\u4e00-\\u9fa5]+[·•][\\u4e00-\\u9fa5]+$"))
    } else {
        name.matches(Regex("^[\\u4e00-\\u9fa5]+$"))
    }
}

fun TextView?.isLegalName(): Boolean = this?.text?.toString().isLegalName()

/**
 * 验证输入的身份证号是否合法
 * 由15位数字或18位数字（17位数字加“x”）组成，15位纯数字没什么好说的，18位的话，可以是18位纯数字，或者17位数字加“x”。
 */
fun String?.isLegalId(): Boolean {
    val id = this ?: return false
    return id.toUpperCase(Locale.getDefault()).matches(Regex("(^\\d{15}$)|(^\\d{17}([0-9]|X)$)"))
}

fun TextView?.isLegalId(): Boolean = this?.text?.toString().isLegalId()
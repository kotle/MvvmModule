package com.weilele.mvvm.utils.`object`

import android.app.Activity
import android.content.res.Resources


/**
 * 描述：屏幕适配,app的尺寸不做处理，不做适配，只对activity适配
 * 可以根据需要来觉得是否适配
 */
object ScreenAdaptationObj {
    private val screenAdaptation by lazy { ScreenAdaptation() }

    //屏幕适配适配后的密度因子
    val density: Float
        get() = screenAdaptation.density

    //屏幕适配后的字体缩放因子
    //因为适配之后的字体总是偏大，所以设置0.96使他小一点
    val scaledDensity: Float
        get() = screenAdaptation.scaledDensity

    //屏幕适配后的Dpi
    val densityDpi: Int
        get() = screenAdaptation.densityDpi

    fun setCustomDensity(activity: Activity?,enable:Boolean) {
        setCustomDensity(activity?.resources,enable)
    }

    fun setCustomDensity(resources: Resources?,enable:Boolean) {
        screenAdaptation.setCustomDensity(resources,enable)
    }
}
package com.weilele.mvvm.utils.`object`

import android.app.Notification
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationChannelGroupCompat
import androidx.core.app.NotificationManagerCompat
import com.weilele.mvvm.app

/**
 * 描述：发送通知栏的使用案列
 */
object MvvmNotification {
    val notificationManagerCompat by lazy {
        NotificationManagerCompat.from(app)
    }

    /**
     * 发送简单文本消息
     */
    fun notifySimpleText(
        id: Int,/*通知id，唯一*/
        channelId: String,/*通知渠道id*/
        content: String,
        @DrawableRes smallIcon: Int,
        notificationBuildAction: ((NotificationCompat.Builder) -> Unit)? = null,/*构建通知的额外参数*/
    ) {
        notify(id, channelId, smallIcon) {
            it.setContentText(content)
            notificationBuildAction?.invoke(it)
        }
    }

    /**
     * 发送一个通知,
     * 发送通知前必须先创建对应的渠道
     */
    inline fun notify(
        id: Int,/*通知id，唯一*/
        channelId: String,/*通知渠道id*/
        @DrawableRes smallIcon: Int,/*logo，不传报错*/
        notificationBuildAction: (NotificationCompat.Builder) -> Unit,/*构建通知的额外参数*/
    ) {
        if (getNotificationChannel(channelId) == null) {
            createNotificationChannel(channelId, channelId)
        }
        //构建通知
        val notification = createNotifyBuilder(channelId, smallIcon)
            .also {
                notificationBuildAction.invoke(it)
            }.build()
        //发送通知
        notify(id, notification)
    }

    /**
     * 发送通知
     */
    fun notify(
        id: Int,/*通知id，唯一*/
        notification: Notification
    ) {
        //发送通知
        notificationManagerCompat.notify(id, notification)
    }


    /**
     * 创建一个通知渠道
     */
    fun createNotifyBuilder(
        channelId: String,/*通知渠道*/
        @DrawableRes smallIcon: Int,/*logo，不传报错*/
    ): NotificationCompat.Builder {
        if (getNotificationChannel(channelId) == null) {
            createNotificationChannel(channelId, channelId)
        }
        //构建通知
        return NotificationCompat.Builder(app, channelId)
            //使用默认的声音、振动、闪光
            .setSmallIcon(smallIcon)
    }


    /**
     * 创建渠道
     */
    fun createNotificationChannel(
        channelId: String,
        channelName: String,
        buildAction: ((NotificationChannelCompat.Builder) -> Unit)? = null,/*渠道构建的额外参数*/
    ) {
        val channel = NotificationChannelCompat.Builder(
            channelId,
            NotificationManagerCompat.IMPORTANCE_DEFAULT
        ).setName(channelName)
            .also {
                buildAction?.invoke(it)
            }.build()
        notificationManagerCompat.createNotificationChannel(channel)
    }

    /**
     * 创建渠道组
     */
    fun createNotificationChannelGroup(
        groupId: String,
        groupName: String,
        buildAction: ((NotificationChannelGroupCompat.Builder) -> Unit)? = null,/*渠道构建的额外参数*/
    ) {
        val channel = NotificationChannelGroupCompat.Builder(
            groupId
        ).setName(groupName)
            .also {
                buildAction?.invoke(it)
            }.build()
        notificationManagerCompat.createNotificationChannelGroup(channel)
    }

    /**
     * 移除通知
     */
    fun cancel(id: Int) {
        notificationManagerCompat.cancel(id)
    }

    /**
     * 移除所有通知
     */
    fun cancelAll() {
        notificationManagerCompat.cancelAll()
    }

    /**
     * 删除渠道
     */
    fun deleteNotificationChannel(channelId: String) {
        notificationManagerCompat.deleteNotificationChannel(channelId)
    }

    /**
     * 删除渠道组
     */
    fun deleteNotificationChannelGroup(groupId: String) {
        notificationManagerCompat.deleteNotificationChannelGroup(groupId)
    }

    /**
     * 通知是否可用
     */
    fun areNotificationsEnabled() =
        notificationManagerCompat.areNotificationsEnabled()

    /**
     * 获取创建的渠道
     */
    fun getNotificationChannel(channelId: String) =
        notificationManagerCompat.getNotificationChannelCompat(channelId)

    /**
     * 获取创建的渠道组
     */
    fun getNotificationChannelGroup(groupId: String) =
        notificationManagerCompat.getNotificationChannelGroupCompat(groupId)

/*    fun sendNotifyActivity(
        context: Context*//*上下文*//*,
        intent: Intent*//*跳转的activity*//*,
        title: String?*//*标题*//*,
        msg: String?*//*内容*//*,
        NOTIFICATION_ID: Int*//*通知id*//*,
        name: CharSequence*//*用户可见频道名称，8.0以上有效*//*,
        smallIcon: Int *//*小图标，纯色*//*,
        bigIcon: Int*//*左边的logo*//*,
        @SuppressLint("InlinedApi") priority: Int = NotificationManager.IMPORTANCE_MAX*//*优先级，8.0以上有效*//*
    ) {
        val pendingIntent = PendingIntent.getActivity(
            context, 0,
            intent, PendingIntent.FLAG_ONE_SHOT
        )
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context, NOTIFICATION_ID.toString())
            .setSmallIcon(smallIcon)
            .setContentTitle(title)
            //使用默认的声音、振动、闪光
            .setDefaults(Notification.DEFAULT_ALL)
            .setContentText(msg)
            .setLargeIcon((ContextCompat.getDrawable(context, bigIcon) as BitmapDrawable).bitmap)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setShowWhen(true)
            .setTicker("$title-$msg")//在状态栏短暂显示的文字
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannelCompat.Builder(NOTIFICATION_ID.toString(), priority)
                .setName(name)
                .build()
            NotificationManagerCompat.from()
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(
            javaClass.simpleName,
            NOTIFICATION_ID,
            notificationBuilder.build()
        )
    }*/
}
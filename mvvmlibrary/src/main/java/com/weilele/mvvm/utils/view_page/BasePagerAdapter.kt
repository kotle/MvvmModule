package com.weilele.mvvm.utils.view_page

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.weilele.mvvm.utils.activity.getActivity
import java.lang.IllegalArgumentException

@Deprecated("使用ViewPager2替代")
fun ViewPager.simplePagerAdapter(
        itemCount: Int,
        fm: FragmentActivity? = this.getActivity(),
        onCreateItem: Function1<@ParameterName("position") Int, Fragment>
): FragmentStatePagerAdapter {
    if (fm == null) {
        throw IllegalArgumentException("FragmentManager is null")
    }
    val baseAdapter = object : FragmentStatePagerAdapter(
            fm.supportFragmentManager,
            BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        override fun getCount(): Int = itemCount

        override fun getItem(position: Int): Fragment {
            return onCreateItem.invoke(position)
        }

    }
    adapter = baseAdapter
    return baseAdapter
}

@Deprecated("使用ViewPager2替代")
fun <T> ViewPager.simplePagerAdapter(
        datas: MutableList<T>,
        fm: FragmentActivity? = this.getActivity(),
        onCreateItem: Function2<@ParameterName("position") Int, T, Fragment>
): FragmentStatePagerAdapter {
    if (fm == null) {
        throw IllegalArgumentException("FragmentManager is null")
    }
    val baseAdapter = object : FragmentStatePagerAdapter(
            fm.supportFragmentManager,
            BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        override fun getCount(): Int = datas.count()

        override fun getItem(position: Int): Fragment {
            return onCreateItem.invoke(position, datas[position])
        }

    }
    adapter = baseAdapter
    return baseAdapter
}

@Deprecated("使用ViewPager2替代")
fun <T> ViewPager.simplePagerAdapter(
        datas: MutableList<T>,
        fm: FragmentManager,
        onCreateItem: Function2<@ParameterName("position") Int, T, Fragment>
): FragmentStatePagerAdapter {
    val baseAdapter = object : FragmentStatePagerAdapter(
            fm,
            BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        override fun getCount(): Int = datas.count()

        override fun getItem(position: Int): Fragment {
            return onCreateItem.invoke(position, datas[position])
        }

    }
    adapter = baseAdapter
    return baseAdapter
}

@Deprecated("使用ViewPager2替代")
fun ViewPager.simplePagerAdapter(
        fm: FragmentManager,
        itemCount: Int,
        onCreateItem: Function1<@ParameterName("position") Int, Fragment>
): FragmentStatePagerAdapter {
    val baseAdapter = object : FragmentStatePagerAdapter(
            fm,
            BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        override fun getCount(): Int = itemCount

        override fun getItem(position: Int): Fragment {
            return onCreateItem.invoke(position)
        }

    }
    adapter = baseAdapter
    return baseAdapter
}
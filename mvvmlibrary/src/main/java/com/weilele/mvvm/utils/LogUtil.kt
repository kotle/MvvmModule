package com.weilele.mvvm.utils

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.orhanobut.logger.Logger
import com.weilele.mvvm.LibraryConf
import com.weilele.mvvm.MvvmConf

inline fun <T : Any> T.logV(call: (T) -> String?) {
    if (!MvvmConf.isDebug) {
        return
    }
    val tag = this::class.java.name
    val msg = call.invoke(this) ?: "log is null"
    if (LibraryConf.enableLoggerLibrary) {
        Logger.v(msg)
    } else {
        Log.v(tag, msg)
    }
}

inline fun <T : Any> T.logI(call: (T) -> String?) {
    if (!MvvmConf.isDebug) {
        return
    }
    val tag = this::class.java.name
    val msg = call.invoke(this) ?: "log is null"
    if (LibraryConf.enableLoggerLibrary) {
        Logger.i(msg)
    } else {
        Log.i(tag, msg)
    }
}

inline fun <T : Any> T.logD(call: (T) -> String?) {
    if (!MvvmConf.isDebug) {
        return
    }
    val tag = this::class.java.name
    val msg = call.invoke(this) ?: "log is null"
    if (LibraryConf.enableLoggerLibrary) {
        Logger.d(msg)
    } else {
        Log.d(tag, msg)
    }
}

inline fun <T : Any> T.logW(call: (T) -> String?) {
    if (!MvvmConf.isDebug) {
        return
    }
    val tag = this::class.java.name
    val msg = call.invoke(this) ?: "msg is null"
    if (LibraryConf.enableLoggerLibrary) {
        Logger.w(msg)
    } else {
        Log.w(tag, msg)
    }
}

inline fun <T : Any> T.logE(call: (T) -> String?) {
    if (!MvvmConf.isDebug) {
        return
    }
    val tag = this::class.java.name
    val msg = call.invoke(this) ?: "msg is null"
    if (LibraryConf.enableLoggerLibrary) {
        Logger.e(msg)
    } else {
        Log.e(tag, msg)
    }
}

inline fun <T : Any> T.printIntent(call: (T) -> Intent?) {
    printBundle {
        call.invoke(this)?.extras
    }
}

inline fun <T : Any> T.printBundle(call: (T) -> Bundle?) {
    if (!MvvmConf.isDebug) {
        return
    }
    val bundle = call.invoke(this) ?: return
    if (LibraryConf.enableLoggerLibrary) {
        logI { "开始打印Intent" }
        bundle.keySet().forEach {
            logI { _ -> "key:${it} value:${bundle.get(it)}" }
        }
        logI { "结束打印Intent" }
    }
}

inline fun printStackTrace(call: () -> Throwable?) {
    if (!MvvmConf.isDebug) {
        return
    }
    call.invoke()?.printStackTrace()
}
package com.weilele.mvvm.utils.number

import android.text.format.DateUtils
import com.google.android.material.animation.ArgbEvaluatorCompat

/**
 * 最大时间为一周 显示为（七天前）
 * 否则显示年月日
 */
fun getRelativeTimeSpanString(
    startTime: Long,
    endTime: Long = System.currentTimeMillis(),
    minResolution: Long = DateUtils.SECOND_IN_MILLIS/*默认最小精度单位为秒*/
): CharSequence {
    return DateUtils.getRelativeTimeSpanString(startTime, endTime, minResolution)
}

/**
 * 指定起始颜色，获取中间颜色
 */
fun getColorByFraction(
    fraction: Float,
    startColor: Int,
    endColor: Int,
): Int {
    return ArgbEvaluatorCompat.getInstance().evaluate(fraction, startColor, endColor)
}
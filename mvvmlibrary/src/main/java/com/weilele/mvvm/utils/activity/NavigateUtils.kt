package com.weilele.mvvm.utils.activity

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.fragment.app.Fragment

/**
 * 普通跳转
 */
inline fun <reified T : Activity> Activity?.navigateTo(
    intent: Intent? = null,
    option: ActivityOptionsCompat? = null,
    noinline setParams: ((Intent) -> Unit)? = null
) {
    this ?: return
    val i = intent ?: Intent(this, T::class.java)
    setParams?.invoke(i)
    ActivityCompat.startActivity(this, i, option?.toBundle())
}

/**
 * 普通跳转,从fragment
 */
inline fun <reified T : Activity> Fragment?.navigateTo(
    intent: Intent? = null,
    option: ActivityOptionsCompat? = null,
    noinline setParams: ((Intent) -> Unit)? = null
) {
    this ?: return
    val i = intent ?: Intent(context, T::class.java)
    setParams?.invoke(i)
    startActivity(i, option?.toBundle())
}

/**
 * 携带view的动画，从一个界面飞入另一个
 */
inline fun <reified T : Activity> Activity?.navigateWithViews(
    vararg viewName: Pair<View, String>,
    noinline setParams: ((Intent) -> Unit)? = null
) {
    this ?: return
    val option = ActivityOptionsCompat.makeSceneTransitionAnimation(this, *viewName)
    val intent = Intent(this, T::class.java)
    setParams?.invoke(intent)
    ActivityCompat.startActivity(this, intent, option.toBundle())
}

/**
 * activity跳转，普通跳转，从某个按钮展开的动画
 */
inline fun <reified T : Activity> Activity?.navigateUpScale(
    view: View,
    noinline setParams: ((Intent) -> Unit)? = null
) {
    this ?: return
    val option = ActivityOptionsCompat.makeScaleUpAnimation(
        view,
        view.width / 2, view.height / 2, view.width, view.height
    )
    val intent = Intent(this, T::class.java)
    setParams?.invoke(intent)
    ActivityCompat.startActivity(this, intent, option.toBundle())
}

/**
 * 揭露动画跳转
 */
inline fun <reified T : Activity> Activity?.navigateClipReveal(
    view: View,
    noinline setParams: ((Intent) -> Unit)? = null
) {
    this ?: return
    val option = ActivityOptionsCompat.makeClipRevealAnimation(
        view,
        view.width / 2, view.height / 2, view.width, view.height
    )
    val intent = Intent(this, T::class.java)
    setParams?.invoke(intent)
    ActivityCompat.startActivity(this, intent, option.toBundle())
}

package com.weilele.mvvm.utils.download

import com.weilele.mvvm.utils.thread.AsyncThread

/**
 * 下载监听
 */
abstract class DownListener : AsyncThread.AsyncThreadCall<Long, DownloadBean?> {
    final override fun onProgressUpdate(values: Array<out Long>) {
        super.onProgressUpdate(values)
        onProgressUpdate(values[0], values[1], values[2])
    }

    final override fun onProgressUpdateInIoThread(values: Array<out Long>) {
        super.onProgressUpdateInIoThread(values)
        onProgressUpdateInIoThread(values[0], values[1], values[2])
    }

    open fun onProgressUpdate(downloadSize: Long, totalSize: Long, progress: Long) {}

    open fun onProgressUpdateInIoThread(downloadSize: Long, totalSize: Long, progress: Long) {}
}
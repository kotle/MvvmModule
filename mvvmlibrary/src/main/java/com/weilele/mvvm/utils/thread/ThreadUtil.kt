package com.weilele.mvvm.utils.thread

import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * 描述：线程池工具类，推荐使用协程实现
 */
object ThreadUtil {
    /**
     * 单个线程，存活时间无限
     */
    private val singleThread by lazy {
        Executors.newSingleThreadExecutor()
    }

    /**
     * 固定线程，存活时间无限
     * 数量为手机cpu数量
     */
    private val muchThread by lazy {
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
    }

    /**
     * 单个线程执行任务，当此线程正在执行的时候，其他任务等待
     */
    @Deprecated("请使用协程")
    fun singleThreadRun(runnable: Runnable) {
        singleThread.execute(runnable)
    }

    /**
     * 开启多个线程执行任务
     */
    @Deprecated("请使用协程")
    fun muchThreadThreadRun(runnable: Runnable) {
        muchThread.execute(runnable)
    }

    /**
     * 周期性执行某任务
     */
    private val scheduledThreadPool by lazy {
        Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors())
    }

    /**
     * 周期性执行某任务
     */
    fun periodThreadRun(
        initialDelay: Long,
        period: Long,
        unit: TimeUnit, run: Runnable
    ) =
        scheduledThreadPool
            .scheduleAtFixedRate(run, initialDelay, period, unit)

}
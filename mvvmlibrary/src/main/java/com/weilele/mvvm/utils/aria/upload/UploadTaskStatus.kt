package com.weilele.mvvm.utils.aria.upload

import com.arialyy.aria.core.Aria
import com.arialyy.aria.core.task.UploadTask
import com.arialyy.aria.core.upload.UploadTaskListener
import java.lang.Exception

/**
 * 任务状态类
 */
object UploadTaskStatus : UploadTaskListener {
    init {
        register()
    }

    internal val listenerMap = hashMapOf<Long?, MvvmUpload.Listener>()

    override fun onWait(task: UploadTask) {
        getListener(task)?.onWait(task)
    }

    override fun onPre(task: UploadTask) {
        getListener(task)?.onPre(task)
    }

    override fun onTaskPre(task: UploadTask) {
        getListener(task)?.onTaskPre(task)
    }

    override fun onTaskResume(task: UploadTask) {
        getListener(task)?.onTaskResume(task)
    }

    override fun onTaskStart(task: UploadTask) {
        getListener(task)?.onTaskStart(task)
    }

    override fun onTaskStop(task: UploadTask) {
        getListener(task)?.onTaskStop(task)
    }

    override fun onTaskCancel(task: UploadTask) {
        getListener(task)?.onTaskCancel(task)
    }

    override fun onTaskFail(task: UploadTask?, e: Exception) {
        if (task != null) {
            getListener(task)?.onTaskFail(task, e)
        }
    }

    override fun onTaskComplete(task: UploadTask) {
        getListener(task)?.onTaskComplete(task)
    }

    override fun onTaskRunning(task: UploadTask) {
        getListener(task)?.onTaskRunning(task)
    }

    override fun onNoSupportBreakPoint(task: UploadTask) {
        getListener(task)?.onNoSupportBreakPoint(task)
    }

    private fun getListener(task: UploadTask): MvvmUpload.Listener? {
        return listenerMap[task.entity.id]
    }

    /**
     * 注册
     */
    fun register() {
        Aria.download(this).register()
    }

    /**
     * 取消注册
     */
    fun unRegister() {
        Aria.download(this).unRegister()
    }
}
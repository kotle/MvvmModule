package com.weilele.mvvm.utils.db_litepal

import com.weilele.mvvm.app
import org.litepal.LitePal
import org.litepal.LitePalDB
import org.litepal.crud.LitePalSupport
import org.litepal.extension.runInTransaction

/**
 * 1.直接调用对象的[LitePalSupport.delete]删除，不会级联删除，即不会删除关联表
 *   调用[LitePal.delete]删除，会级联删除，请慎用删除函数
 *
 * 2.基本字段（string/int/long等）支持继承
 *   对象（关联表）不支持继承操作，对象无法保存在数据库
 *
 * 3.查询关联表的时候，需要自己写关联表的查询代码，进行懒加载。
 *   或者调用[LitePal.find]时候，对[isEager]传入为true，表示查询关联表
 *
 * 4.使用事务调用[LitePal.runInTransaction]/函数
 *
 * 5.其他数据库增删改查，请使用[MvvmDb.dao]来操作,[MvvmDb.dao]指向[LitePal]
 */
object MvvmDb {

    data class DbBean(
        val dbName: String/*数据库名*/,
        val version: Int/*数据库版本*/,
        val cls: List<Class<*>>/*表*/
    )

    val dao
        get() = LitePal

    init {
        dao.initialize(app)
    }

    /**
     * 设置当前操作的数据库，不设置默认是assets的数据库
     */
    fun setCurrentDb(db: DbBean) {
        val litePalDB = LitePalDB(db.dbName, db.version)
        db.cls.forEach { className ->
            litePalDB.addClassName(className.name)
        }
        dao.use(litePalDB)
        dao.getDatabase()
    }
}
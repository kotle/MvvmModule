package com.weilele.mvvm

object LibraryConf {
    /**
     * 是否支持多语言
     */
    var enableMultiLanguages = false

    /**
     * 是否可以是用第三方日志打印库
     */
    var enableLoggerLibrary = false
}
package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.helper.widget.Flow
import androidx.core.view.children
import com.weilele.mvvm.utils.activity.dip

/**
 * 流式布局
 */
open class BaseFlowLayout : BaseConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var flowView: BaseFlow? = null

    override fun onFinishInflate() {
        super.onFinishInflate()
        checkXmlLayout()
    }

    override fun onViewAdded(view: View?) {
        if (view?.id == NO_ID) {
            view.id = generateViewId()
        }
        super.onViewAdded(view)
    }

    private fun checkXmlLayout() {
        val ids = mutableListOf<Int>()
        children.forEach { childView ->
            if (childView !is BaseFlow) {
                ids.add(childView.id)
            } else {
                flowView = childView
            }
        }
        setReferencedIds(ids)
    }

    private fun setReferencedIds(ids: MutableList<Int>) {
        val flowView = getFlow()
        if (flowView.parent == null) {
            addView(flowView)
        }
        flowView.referencedIds = ids.toIntArray()
    }

    /**
     * 做布局约束用,一般同级目录只应该存在一个
     */
    class BaseFlow : Flow {
        constructor(context: Context) : super(context)
        constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
        constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    }

    /************************************************************************/
    //获取布局约束
    fun getFlow(): BaseFlow {
        val flow = flowView ?: BaseFlow(context).apply {
            //android:layout_marginTop="8dp"
            //app:flow_horizontalAlign="start"
            //app:flow_horizontalBias="0"
            //app:flow_horizontalGap="8dp"
            //app:flow_horizontalStyle="packed"
            //app:flow_maxElementsWrap="20"
            //app:flow_verticalGap="8dp"
            //app:flow_wrapMode="chain"
            setHorizontalAlign(Flow.HORIZONTAL_ALIGN_START)
            setVerticalAlign(Flow.VERTICAL_ALIGN_TOP)
            setHorizontalGap(dip(8))
            setVerticalGap(dip(8))
            setHorizontalStyle(Flow.CHAIN_PACKED)
            setVerticalStyle(Flow.CHAIN_PACKED)
            setHorizontalBias(0f)
            setWrapMode(Flow.WRAP_CHAIN)
        }
        flowView = flow
        return flow
    }

    /**
     * 设置需要展示的view
     */
    fun setViews(views: List<View?>?) {
        removeAllViews()
        views ?: return
        val ids = mutableListOf<Int>()
        views.forEach { view ->
            view?.let {
                if (it.id == NO_ID) {
                    it.id = generateViewId()
                }
                addView(it)
                ids.add(it.id)
            }
        }
        setReferencedIds(ids)
    }

    /**
     * 设置水平间隔
     */
    fun setHorizontalGap(gap: Int) {
        getFlow().setHorizontalGap(gap)
    }

    /**
     * 设置竖直间隔
     */
    fun setVerticalGap(gap: Int) {
        getFlow().setVerticalGap(gap)
    }

    /**
     * 设置对其方式
     */
    fun setVerticalAlign(align: Int /*eg:Flow.VERTICAL_ALIGN_TOP*/) {
        getFlow().setVerticalAlign(align)
    }

    /**
     * 设置对其方式
     */
    fun setHorizontalAlign(align: Int) {
        getFlow().setHorizontalAlign(align)
    }

    /**
     * 设置布局方向
     */
    fun setOrientation(orientation: Int/*eg:Flow.HORIZONTAL*/) {
        getFlow().setOrientation(orientation)
    }

    /**
     * 设置每行的偏差 0f 靠左布局， 0.5f 居中布局  ,1f靠右布局
     */
    fun setHorizontalBias(bias: Float/*0-1f*/) {
        getFlow().setHorizontalBias(bias)
    }

    /**
     * 设置列的偏差 0f 靠上布局， 0.5f 居中布局  ,1f靠下布局
     */
    fun setVerticalBias(bias: Float/*0-1f*/) {
        getFlow().setVerticalBias(bias)
    }
}
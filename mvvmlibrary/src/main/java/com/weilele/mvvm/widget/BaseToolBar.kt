package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.appbar.MaterialToolbar
import com.weilele.mvvm.R
import com.weilele.mvvm.widget.helper.handleCustomViewBackground

/**
 * 支持设置标题和副标题是否居中
 * [MaterialToolbar.setTitleCentered]
 */
open class BaseToolBar : MaterialToolbar {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        handleAttr(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        handleAttr(attrs)
    }

    private fun handleAttr(attributes: AttributeSet?) {
        context.obtainStyledAttributes(attributes, R.styleable.BaseToolBar).apply {
            //处理背景色
            handleCustomViewBackground(
                this,
                _rippleColor = R.styleable.BaseToolBar_rippleColor,
                _unboundedRipple = R.styleable.BaseToolBar_unboundedRipple,
                _backgroundNormal = R.styleable.BaseToolBar_backgroundNormal,
                _backgroundPressed = R.styleable.BaseToolBar_backgroundPressed,
                _backgroundUnEnable = R.styleable.BaseToolBar_backgroundUnEnable,
                _cornerRadius = R.styleable.BaseToolBar_cornerRadius,
                _cornerSizeTopLeft = R.styleable.BaseToolBar_cornerSizeTopLeft,
                _cornerSizeTopRight = R.styleable.BaseToolBar_cornerSizeTopRight,
                _cornerSizeBottomLeft = R.styleable.BaseToolBar_cornerSizeBottomLeft,
                _cornerSizeBottomRight = R.styleable.BaseToolBar_cornerSizeBottomRight,
                _strokeColor = R.styleable.BaseToolBar_strokeColor,
                _strokeWidth = R.styleable.BaseToolBar_strokeWidth,
                _backgroundShape = R.styleable.BaseToolBar_backgroundShape,
            )
        }.recycle()
    }
}
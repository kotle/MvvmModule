package com.weilele.mvvm.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import com.weilele.mvvm.utils.activity.dip

/**
 * 圆环view
 */
open class BaseRingView : BaseTextView {
    private val ringMaxProgress = 360f
    //圆环颜色
    var ringColor = Color.WHITE
    //圆环背景颜色
    var ringBackgroundColor: Int? = null
    //圆环宽度
    var ringWidth = dip(3f)
    //开始的位置
    var ringStartAngle = -90f
    //进度值值0-360f
    var ringProgress: Float = 0f
        set(value) {
            field = value
            invalidate()
        }
    //进度比例，0-1f
    var ringProgressRatio: Float = 0f
        set(value) {
            field = value
            ringProgress = value * ringMaxProgress
        }
    //圆环的padding
    var ringPadding: RectF = RectF()
    private val paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
            strokeWidth = ringWidth
            color = ringColor

        }
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        ringBackgroundColor?.let {
            paint.color = it
            canvas?.drawArc(
                ringPadding.left + ringWidth,
                ringPadding.top + ringWidth,
                width - ringPadding.right - ringWidth,
                height - ringPadding.bottom - ringWidth,
                0f,
                360f,
                false,
                paint
            )
        }
        paint.color = ringColor
        canvas?.drawArc(
            ringPadding.left + ringWidth,
            ringPadding.top + ringWidth,
            width - ringPadding.right - ringWidth,
            height - ringPadding.bottom - ringWidth,
            ringStartAngle,
            ringProgress,
            false,
            paint
        )
    }
}
package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

/**
 * TabLayoutMediator
 */
open class BaseTabLayout : TabLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    inline fun attachViewPager2(
        viewPage2: ViewPager2,
        autoRefresh: Boolean = true,
        crossinline tabBlock: ((tab: Tab, position: Int) -> Unit) = { _, _ -> }
    ) {
        TabLayoutMediator(this, viewPage2, autoRefresh) { tab: Tab, position: Int ->
            tabBlock.invoke(tab, position)
        }.attach()
    }
}
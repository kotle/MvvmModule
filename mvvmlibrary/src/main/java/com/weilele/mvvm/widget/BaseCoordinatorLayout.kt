package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.weilele.mvvm.R
import com.weilele.mvvm.widget.helper.handleCustomViewBackground

open class BaseCoordinatorLayout : CoordinatorLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
        handleAttr(attrs)
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        handleAttr(attrs)
    }

    private fun handleAttr(attributes: AttributeSet?) {
        context.obtainStyledAttributes(attributes, R.styleable.BaseCoordinatorLayout).apply {
            //处理背景色
            handleCustomViewBackground(
                this,
                _rippleColor = R.styleable.BaseCoordinatorLayout_rippleColor,
                _unboundedRipple = R.styleable.BaseCoordinatorLayout_unboundedRipple,
                _backgroundNormal = R.styleable.BaseCoordinatorLayout_backgroundNormal,
                _backgroundPressed = R.styleable.BaseCoordinatorLayout_backgroundPressed,
                _backgroundUnEnable = R.styleable.BaseCoordinatorLayout_backgroundUnEnable,
                _cornerRadius = R.styleable.BaseCoordinatorLayout_cornerRadius,
                _cornerSizeTopLeft = R.styleable.BaseCoordinatorLayout_cornerSizeTopLeft,
                _cornerSizeTopRight = R.styleable.BaseCoordinatorLayout_cornerSizeTopRight,
                _cornerSizeBottomLeft = R.styleable.BaseCoordinatorLayout_cornerSizeBottomLeft,
                _cornerSizeBottomRight = R.styleable.BaseCoordinatorLayout_cornerSizeBottomRight,
                _strokeColor = R.styleable.BaseCoordinatorLayout_strokeColor,
                _strokeWidth = R.styleable.BaseCoordinatorLayout_strokeWidth,
                _backgroundShape = R.styleable.BaseCoordinatorLayout_backgroundShape,
            )
        }.recycle()
    }
}
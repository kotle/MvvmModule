package com.weilele.mvvm.widget

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.shape.*
import com.weilele.mvvm.R
import com.weilele.mvvm.utils.glide.OkHttpLoadImage
import com.weilele.mvvm.utils.glide.setImageAny
import com.weilele.mvvm.widget.helper.handleCustomViewBackground

/**
 * <style name="StyleShapeAppearanceImage" parent="">
 * <item name="cornerFamily">rounded</item>
 * <item name="cornerSize">16dp</item>
 * <item name="cornerSizeTopRight">10dp</item>
 * <item name="cornerSizeBottomRight">0dp</item>
 * </style>
 */
open class BaseShapeableImageView : ShapeableImageView {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        attrs?.let {
            context.obtainStyledAttributes(attrs, R.styleable.BaseShapeableImageView).apply {
                val isCircle = getBoolean(R.styleable.BaseShapeableImageView_isCircle, false)
                if (isCircle) {
                    setCircleImage()
                } else {
                    val roundRadius =
                        getDimension(R.styleable.BaseShapeableImageView_roundRadius, 0f)
                    setRoundRadius(roundRadius)
                }
                handleAttr(this)
                this.recycle()
            }
        }
    }

    private fun handleAttr(attributes: TypedArray) {
        //处理背景色
        handleCustomViewBackground(
            attributes,
            _rippleColor = R.styleable.BaseShapeableImageView_rippleColor,
            _unboundedRipple = R.styleable.BaseShapeableImageView_unboundedRipple,
            _backgroundNormal = R.styleable.BaseShapeableImageView_backgroundNormal,
            _backgroundPressed = R.styleable.BaseShapeableImageView_backgroundPressed,
            _backgroundUnEnable = R.styleable.BaseShapeableImageView_backgroundUnEnable,
            _cornerRadius = R.styleable.BaseShapeableImageView_cornerRadius,
            _cornerSizeTopLeft = R.styleable.BaseShapeableImageView_cornerSizeTopLeft,
            _cornerSizeTopRight = R.styleable.BaseShapeableImageView_cornerSizeTopRight,
            _cornerSizeBottomLeft = R.styleable.BaseShapeableImageView_cornerSizeBottomLeft,
            _cornerSizeBottomRight = R.styleable.BaseShapeableImageView_cornerSizeBottomRight,
            _strokeColor = R.styleable.BaseShapeableImageView_strokeColor,
            _strokeWidth = R.styleable.BaseShapeableImageView_strokeWidth,
            _backgroundShape = R.styleable.BaseShapeableImageView_backgroundShape,
        )
    }

/*    fun set() {
        shapeAppearanceModel = ShapeAppearanceModel.builder()
                .setAllCorners(CornerFamily.ROUNDED, 20f)
                .setTopLeftCorner(CornerFamily.CUT, RelativeCornerSize(0.3f))
                .setTopRightCorner(CornerFamily.CUT, RelativeCornerSize(0.3f))
                .setBottomRightCorner(CornerFamily.CUT, RelativeCornerSize(0.3f))
                .setBottomLeftCorner(CornerFamily.CUT, RelativeCornerSize(0.3f))
                .setAllCornerSizes(ShapeAppearanceModel.PILL)
                .setTopLeftCornerSize(20f)
                .setTopRightCornerSize(RelativeCornerSize(0.5f))
                .setBottomLeftCornerSize(10f)
                .setBottomRightCornerSize(AbsoluteCornerSize(30f))
                .build()
    }*/
    /**
     * 设置为圆形图片
     */
    fun setCircleImage() {
        shapeAppearanceModel = shapeAppearanceModel.toBuilder()
            .setAllCornerSizes(RelativeCornerSize(0.5f))
            .build()
    }

    /**
     * 设置图片圆角
     */
    fun setRoundRadius(radius: Float) {
        shapeAppearanceModel = shapeAppearanceModel.toBuilder()
            .setAllCornerSizes(AbsoluteCornerSize(radius))
            .build()
    }

    private var onProgressListener: OkHttpLoadImage.OnProgressListener? = null

    open fun setImageUrl(url: String?, listener: ((Float/*进度从0-1f*/) -> Unit)? = null) {
        if (url.isNullOrBlank() || listener == null) {
            setImageAny(url)
        } else {
            OkHttpLoadImage.unRegister(onProgressListener)
            onProgressListener = object : OkHttpLoadImage.OnProgressListener {
                override fun getKey(): String {
                    return url
                }

                override fun onProgress(progress: Float) {
                    listener.invoke(progress)
                    if (progress >= 1f) {
                        OkHttpLoadImage.unRegister(onProgressListener)
                        onProgressListener = null
                    }
                }
            }
            OkHttpLoadImage.register(onProgressListener)
            setImageAny(url)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        onProgressListener?.let {
            OkHttpLoadImage.register(it)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        OkHttpLoadImage.unRegister(onProgressListener)
    }
}
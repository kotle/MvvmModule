package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.card.MaterialCardView

open class BaseCardView : MaterialCardView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )
}
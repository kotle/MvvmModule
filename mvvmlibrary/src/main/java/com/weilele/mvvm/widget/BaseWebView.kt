package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.lifecycle.LifecycleOwner
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.weilele.mvvm.base.helper.ILifecycleObserver
import com.weilele.mvvm.utils.activity.dip
import com.weilele.mvvm.utils.activity.gone
import com.weilele.mvvm.utils.safeGet
import com.weilele.mvvm.utils.activity.visible

/**
 * 描述：
 */
open class BaseWebView : WebView, ILifecycleObserver {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var onScrollChangedListener: Function4<Int, Int, Int, Int, Unit>? = null
    var swipeRefreshLayout: SwipeRefreshLayout? = null
    var floatUpTopButton: View? = null
        set(value) {
            field = value
            value?.setOnClickListener {
                scrollTo(0, 0)
                floatUpTopButton?.gone()
            }
        }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        onScrollChangedListener?.invoke(l, t, oldl, oldt)
        swipeRefreshLayout?.isEnabled = this.scrollY == 0
        if (this.scrollY > dip(1000)) {
            floatUpTopButton?.visible()
        } else {
            floatUpTopButton?.gone()
        }
    }

    fun setOnScrollChanged(l: (l: Int, t: Int, oldl: Int, oldt: Int) -> Unit) {
        this.onScrollChangedListener = l
    }

    init {
        initSetting()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        context.safeGet<LifecycleOwner>()?.lifecycle?.addObserver(this)
    }

    private fun initSetting() {
        settings.apply {
//          javaScriptEnabled = true
//          allowFileAccess = true
//          javaScriptCanOpenWindowsAutomatically = true
//          domStorageEnabled = true
//          databaseEnabled = true
            javaScriptEnabled = false
            domStorageEnabled = false
            databaseEnabled = false
            mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            allowFileAccess = false
            savePassword = false
            javaScriptCanOpenWindowsAutomatically = false
            loadsImagesAutomatically = true
            setGeolocationEnabled(true)
            cacheMode = WebSettings.LOAD_NO_CACHE
        }
    }

    override fun onPause(lifecycleOwner: LifecycleOwner) {
        super<ILifecycleObserver>.onPause(lifecycleOwner)
        onPause()
    }

    override fun onPause() {
        super<ILifecycleObserver>.onPause()
    }

    override fun onResume() {
        super<ILifecycleObserver>.onResume()
    }

    override fun onDestroy(lifecycleOwner: LifecycleOwner) {
        super.onDestroy(lifecycleOwner)
        context.safeGet<LifecycleOwner>()?.lifecycle?.removeObserver(this)
        destroy()
    }
}
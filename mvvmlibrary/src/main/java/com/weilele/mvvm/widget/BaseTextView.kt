package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textview.MaterialTextView
import com.weilele.mvvm.R
import com.weilele.mvvm.widget.helper.handleCustomViewBackground
import com.weilele.mvvm.widget.helper.setFontFromAssets

/**
 * 描述：
 * setMovementMethod(ScrollingMovementMethod.getInstance());
 * TextView的textIsSelectable属性和setMovementMethod()
 * TextView的textIsSelectable属性可以支持长按文字可以复制，搜索等，而且支持对TextView的内容滑动。具体见图片
 *T extView的setMovementMethod()方法，也可以支持对TextView的内容滑动，但对Textview内容不支持长按文字可以复制，搜索等。
 * // 通过xml设置
 * android:outlineAmbientShadowColor="#FFAAAA" //控制没有背景时候的阴影颜色 需要设置outlineProvider不是background，安卓10以上有效
 * android:outlineSpotShadowColor="#BAFDCE" // 有背景时候，设置阴影颜色 安卓10以上有效
 * android:outlineProvider="background"
 */
open class BaseTextView : MaterialTextView {
    /**
     * 文字支持跑马灯
     */
    var isMarqueeText = false

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        handleTextViewAttribute(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        handleTextViewAttribute(attrs)
    }


    private fun handleTextViewAttribute(attributes: AttributeSet?) {
        context.obtainStyledAttributes(attributes, R.styleable.BaseTextView).apply {
            val fontPath = getString(R.styleable.BaseTextView_fontAssetsPath)
            if (!fontPath.isNullOrBlank()) {
                setFontFromAssets(fontPath)
            }
            val canMarqueeText = getBoolean(R.styleable.BaseTextView_canMarqueeText, isMarqueeText)
            isMarqueeText = canMarqueeText
            //处理背景色
            handleCustomViewBackground(
                this,
                _rippleColor = R.styleable.BaseTextView_rippleColor,
                _unboundedRipple = R.styleable.BaseTextView_unboundedRipple,
                _backgroundNormal = R.styleable.BaseTextView_backgroundNormal,
                _backgroundPressed = R.styleable.BaseTextView_backgroundPressed,
                _backgroundUnEnable = R.styleable.BaseTextView_backgroundUnEnable,
                _cornerRadius = R.styleable.BaseTextView_cornerRadius,
                _cornerSizeTopLeft = R.styleable.BaseTextView_cornerSizeTopLeft,
                _cornerSizeTopRight = R.styleable.BaseTextView_cornerSizeTopRight,
                _cornerSizeBottomLeft = R.styleable.BaseTextView_cornerSizeBottomLeft,
                _cornerSizeBottomRight = R.styleable.BaseTextView_cornerSizeBottomRight,
                _strokeColor = R.styleable.BaseTextView_strokeColor,
                _strokeWidth = R.styleable.BaseTextView_strokeWidth,
                _backgroundShape = R.styleable.BaseTextView_backgroundShape,
            )
        }.recycle()
    }

    override fun isFocused(): Boolean {
        return if (isMarqueeText) {
            true
        } else {
            super.isFocused()
        }
    }
}
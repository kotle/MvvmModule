package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText
import com.weilele.mvvm.R
import com.weilele.mvvm.widget.helper.handleCustomViewBackground
import com.weilele.mvvm.widget.helper.setFontFromAssets

/**
 * 描述：BaseEditText
 */
open class BaseEditText : TextInputEditText {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        handleTextViewAttribute(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        handleTextViewAttribute(attrs)
    }

    private fun handleTextViewAttribute(attributes: AttributeSet?) {
        context.obtainStyledAttributes(attributes, R.styleable.BaseEditText).apply {
            val fontPath = getString(R.styleable.BaseEditText_fontAssetsPath)
            if (!fontPath.isNullOrBlank()) {
                setFontFromAssets(fontPath)
            }
            //处理背景色
            handleCustomViewBackground(
                this,
                _rippleColor = R.styleable.BaseEditText_rippleColor,
                _unboundedRipple = R.styleable.BaseEditText_unboundedRipple,
                _backgroundNormal = R.styleable.BaseEditText_backgroundNormal,
                _backgroundPressed = R.styleable.BaseEditText_backgroundPressed,
                _backgroundUnEnable = R.styleable.BaseEditText_backgroundUnEnable,
                _cornerRadius = R.styleable.BaseEditText_cornerRadius,
                _cornerSizeTopLeft = R.styleable.BaseEditText_cornerSizeTopLeft,
                _cornerSizeTopRight = R.styleable.BaseEditText_cornerSizeTopRight,
                _cornerSizeBottomLeft = R.styleable.BaseEditText_cornerSizeBottomLeft,
                _cornerSizeBottomRight = R.styleable.BaseEditText_cornerSizeBottomRight,
                _strokeColor = R.styleable.BaseEditText_strokeColor,
                _strokeWidth = R.styleable.BaseEditText_strokeWidth,
                _backgroundShape = R.styleable.BaseEditText_backgroundShape,
            )
        }.recycle()
    }
}
package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import androidx.core.widget.NestedScrollView
import com.weilele.mvvm.R
import com.weilele.mvvm.widget.helper.handleCustomViewBackground

/**
 * 描述：
 */
open class BaseNestedScrollView:NestedScrollView {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
        handleAttr(attrs)
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        handleAttr(attrs)
    }

    private fun handleAttr(attributes: AttributeSet?) {
        context.obtainStyledAttributes(attributes, R.styleable.BaseNestedScrollView).apply {
            //处理背景色
            handleCustomViewBackground(
                this,
                _rippleColor = R.styleable.BaseNestedScrollView_rippleColor,
                _unboundedRipple = R.styleable.BaseNestedScrollView_unboundedRipple,
                _backgroundNormal = R.styleable.BaseNestedScrollView_backgroundNormal,
                _backgroundPressed = R.styleable.BaseNestedScrollView_backgroundPressed,
                _backgroundUnEnable = R.styleable.BaseNestedScrollView_backgroundUnEnable,
                _cornerRadius = R.styleable.BaseNestedScrollView_cornerRadius,
                _cornerSizeTopLeft = R.styleable.BaseNestedScrollView_cornerSizeTopLeft,
                _cornerSizeTopRight = R.styleable.BaseNestedScrollView_cornerSizeTopRight,
                _cornerSizeBottomLeft = R.styleable.BaseNestedScrollView_cornerSizeBottomLeft,
                _cornerSizeBottomRight = R.styleable.BaseNestedScrollView_cornerSizeBottomRight,
                _strokeColor = R.styleable.BaseNestedScrollView_strokeColor,
                _strokeWidth = R.styleable.BaseNestedScrollView_strokeWidth,
                _backgroundShape = R.styleable.BaseNestedScrollView_backgroundShape,
            )
        }.recycle()
    }
}
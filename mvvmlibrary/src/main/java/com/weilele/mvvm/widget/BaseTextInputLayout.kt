package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputLayout
import com.weilele.mvvm.R
import com.weilele.mvvm.widget.helper.handleCustomViewBackground

open class BaseTextInputLayout : TextInputLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs){
        handleAttr(attrs)
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        handleAttr(attrs)
    }

    private fun handleAttr(attributes: AttributeSet?) {
        context.obtainStyledAttributes(attributes, R.styleable.BaseTextInputLayout).apply {
            //处理背景色
            handleCustomViewBackground(
                this,
                _rippleColor = R.styleable.BaseTextInputLayout_rippleColor,
                _unboundedRipple = R.styleable.BaseTextInputLayout_unboundedRipple,
                _backgroundNormal = R.styleable.BaseTextInputLayout_backgroundNormal,
                _backgroundPressed = R.styleable.BaseTextInputLayout_backgroundPressed,
                _backgroundUnEnable = R.styleable.BaseTextInputLayout_backgroundUnEnable,
                _cornerRadius = R.styleable.BaseTextInputLayout_cornerRadius,
                _cornerSizeTopLeft = R.styleable.BaseTextInputLayout_cornerSizeTopLeft,
                _cornerSizeTopRight = R.styleable.BaseTextInputLayout_cornerSizeTopRight,
                _cornerSizeBottomLeft = R.styleable.BaseTextInputLayout_cornerSizeBottomLeft,
                _cornerSizeBottomRight = R.styleable.BaseTextInputLayout_cornerSizeBottomRight,
                _strokeColor = R.styleable.BaseTextInputLayout_strokeColor,
                _strokeWidth = R.styleable.BaseTextInputLayout_strokeWidth,
                _backgroundShape = R.styleable.BaseTextInputLayout_backgroundShape,
            )
        }.recycle()
    }
}
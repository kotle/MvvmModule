package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.slider.Slider

/**
 * android:valueFrom	进度起点
 * android:valueTo	进度终点
 * android:value	当前进度点
 * android:stepSize	步长（必须大于0）
 * app:values	配置多个slider节点
 * app:labelBehavior	slider 滑动时顶部是否显示变化效果
 * app:labelStyle	配置slider节点顶部view style
 */
open class BaseSlider : Slider {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
}
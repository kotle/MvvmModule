package com.weilele.mvvm.widget

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.button.MaterialButton
import com.weilele.mvvm.R
import com.weilele.mvvm.widget.helper.setFontFromAssets

/**
 * 描述：
 * app:backgroundTint	背景着色
 * app:backgroundTintMode	着色模式
 * app:strokeColor	描边颜色
 * app:strokeWidth	描边宽度
 * app:cornerRadius	圆角大小
 * app:rippleColor	按压水波纹颜色
 * app:icon	图标icon
 * app:iconSize	图标大小
 * app:iconGravity	图标重心
 * app:iconTint	图标着色
 * app:iconTintMode	图标着色模式
 * app:iconPadding	图标和文本之间的间距
 * android:stateListAnimator="@null" 移除button阴影
 */
open class BaseButton : MaterialButton {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        handleTextViewAttribute(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        handleTextViewAttribute(attrs)
    }

    private fun handleTextViewAttribute(attributes: AttributeSet?) {
        context.obtainStyledAttributes(attributes, R.styleable.BaseButton).apply {
            val fontPath = getString(R.styleable.BaseButton_fontAssetsPath)
            if (!fontPath.isNullOrBlank()) {
                setFontFromAssets(fontPath)
            }
        }.recycle()
    }
}
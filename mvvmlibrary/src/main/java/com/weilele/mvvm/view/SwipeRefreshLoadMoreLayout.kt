package com.weilele.mvvm.view

import android.content.Context
import android.util.AttributeSet
import androidx.core.view.children

open class SwipeRefreshLoadMoreLayout : ChildUnConsumedView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        canDragTop = true
        canDragBottom = true
        canDragStart = false
        canDragEnd = false
        isAlwaysEnableDrag = true
    }
}
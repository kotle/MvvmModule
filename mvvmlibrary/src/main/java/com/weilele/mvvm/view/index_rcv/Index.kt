package com.weilele.mvvm.view.index_rcv

/**
 * Created by Tomlezen.
 * Data: 2018/7/6.
 * Time: 10:39.
 * @param index 索引.
 */
open class Index(var index: String)
package com.weilele.mvvm.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.view.NestedScrollingChildHelper

/**
 * 当触摸的时候，若子view还能自己消耗滚动，则整个流程都不将滚动分发给父view
 */
open class RecyclerViewLayout : BaseNestedScrollingParentLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private val helper by lazy {
        NestedScrollingChildHelper(this).apply {
            isNestedScrollingEnabled = true
        }
    }

    /**
     * 是否允许分发子view未消耗的值
     */
    private var enableDispatchNestedScroll: Boolean? = null

    override fun onStartNestedScroll(child: View, target: View, axes: Int, type: Int): Boolean {
        enableDispatchNestedScroll = null
        return true
    }

    override fun onNestedPreScroll(target: View, dx: Int, dy: Int, consumed: IntArray, type: Int) {
        super.onNestedPreScroll(target, dx, dy, consumed, type)
    }

    override fun onNestedScroll(
        target: View/*滚动的子view*/,
        dxConsumed: Int/*子view已经消耗*/,
        dyConsumed: Int/*子view已经消耗*/,
        dxUnconsumed: Int/*子view未消耗*/,
        dyUnconsumed: Int/*子view未消耗*/,
        type: Int,
        consumed: IntArray
    ) {
        super.onNestedScroll(
            target,
            dxConsumed,
            dyConsumed,
            dxUnconsumed,
            dyUnconsumed,
            type,
            consumed
        )
        if (enableDispatchNestedScroll == null) {

        }
    }
}
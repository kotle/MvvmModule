package com.weilele.mvvm.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.viewpager2.widget.ViewPager2
import com.weilele.mvvm.utils.activity.dp
import com.weilele.mvvm.widget.BaseView
import java.lang.IllegalStateException

/**
 * ViewPager2滚动指示器
 * 可以根据需求扩展
 */
open class ViewPager2IndicatorView : BaseView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var orientation: Int = ViewPager2.ORIENTATION_HORIZONTAL

    /**
     * 指示器宽度
     */
    var indicatorSize = 16f.dp

    /**
     * 指示器颜色
     */
    var indicatorColor = Color.parseColor("#003333")

    /**
     * 指示器圆角
     */
    var indicatorRadius = 5f.dp

    /**
     * 每一页的变化指示器需要移动的值
     */
    private var itemOffSet = 0f

    private var itemCount = 0
        set(value) {
            field = value
            if (itemCount > 1) {
                itemOffSet = when (orientation) {
                    ViewPager2.ORIENTATION_VERTICAL -> {
                        (height - indicatorSize) / (itemCount - 1)
                    }
                    ViewPager2.ORIENTATION_HORIZONTAL -> {
                        (width - indicatorSize) / (itemCount - 1)
                    }
                    else -> {
                        throw IllegalStateException("orientation is unknow")
                    }
                }
            }
        }

    fun setUpWith(vp2: ViewPager2) {
        vp2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                orientation = vp2.orientation
                vp2.adapter?.itemCount?.let {
                    itemCount = it
                }
                updateIndicator(position, positionOffset)
            }
        })
    }

    private var currentPosition: Int = 0

    private var currentPositionOffset: Float = 0f

    /**
     * 更新指示器位置
     */
    fun updateIndicator(position: Int, positionOffset: Float) {
        currentPosition = position
        currentPositionOffset = positionOffset
        invalidate()
    }

    private val paint = Paint().also {
        it.isAntiAlias = true
    }

    private val rect = RectF()

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (itemCount > 1) {
            when (orientation) {
                ViewPager2.ORIENTATION_VERTICAL -> {
                    rect.top = itemOffSet * (currentPositionOffset + currentPosition)
                    rect.bottom = rect.top + indicatorSize
                    rect.left = 0f
                    rect.right = width.toFloat()
                }
                ViewPager2.ORIENTATION_HORIZONTAL -> {
                    rect.top = 0f
                    rect.bottom = height.toFloat()
                    rect.left = itemOffSet * (currentPositionOffset + currentPosition)
                    rect.right = rect.left + indicatorSize
                }
                else -> {
                    return
                }
            }
            paint.color = indicatorColor
            canvas.drawRoundRect(rect, indicatorRadius, indicatorRadius, paint)
        }
    }
}
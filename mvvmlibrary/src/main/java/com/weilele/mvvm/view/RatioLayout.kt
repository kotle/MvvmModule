package com.weilele.mvvm.view

import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.core.animation.doOnEnd
import androidx.core.view.children
import com.weilele.mvvm.R
import com.weilele.mvvm.adapter.ignoreError
import com.weilele.mvvm.widget.BaseViewGroup

/**
 * 此布局必须有固定的宽高
 * 根据比例布局子view
 */
open class RatioLayout : BaseViewGroup {

    private var lastChildRatio = 1f

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        attrs?.let {
            context.obtainStyledAttributes(attrs, R.styleable.RatioLayout).apply {
                getString(R.styleable.RatioLayout_childRatio)?.let { childRatioStr ->
                    if (childRatioStr.contains(":")) {
                        val splits = childRatioStr.split(":")
                        if (splits.count() == 2) {
                            ignoreError {
                                setChildRatio(splits[0].toFloat() / splits[1].toFloat(), false)
                            }
                        }
                    }
                    if (childRatioStr.contains(".")) {
                        setChildRatio(childRatioStr.toFloat(), false)
                    }
                }
                this.recycle()
            }
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        if (!changed) {
            return
        }
        val width = right - left
        val height = bottom - top
        getChildParams(width, height, lastChildRatio)
    }

    private fun getChildParams(width: Int, height: Int, childRatio: Float) {
        val parentRatio = width.toFloat() / height
        if (childRatio <= parentRatio) {
            //child 高度不变
            val childWidth = height * childRatio
            val childTop = 0
            val off = (width - childWidth) / 2
            val childLeft = off.toInt()
            val childRight = (width - off).toInt()
            childLayout(childLeft, childTop, childRight, height)
        } else {
            //宽度不变
            val childHeight = width / childRatio
            val off = (height - childHeight) / 2
            val childTop = off.toInt()
            val childLeft = 0
            val childBottom = (height - off).toInt()
            childLayout(childLeft, childTop, width, childBottom)
        }
    }

    private fun childLayout(left: Int, top: Int, right: Int, bottom: Int) {
        children.forEach {
            it.layout(left, top, right, bottom)
        }
    }

    private fun setChildRatio(childRatio: Float, isReLayout: Boolean) {
        if (lastChildRatio == childRatio) {
            return
        }
        lastChildRatio = childRatio
        if (isReLayout) {
            if (width > 0 && height > 0) {
                getChildParams(width, height, lastChildRatio)
            }
        }
    }

    /*********************************************************************/
    private var lastAnim: ValueAnimator? = null
    fun smoothChangeChildRatio(childRatio: Float, animDuration: Long = 200) {
        if (lastChildRatio == childRatio) {
            return
        }
        val viewWidth = width
        val viewHeight = height
        if (viewWidth > 0 && viewHeight > 0) {
            lastAnim?.cancel()
            lastAnim = ValueAnimator.ofFloat(lastChildRatio, childRatio).apply {
                duration = animDuration
                doOnEnd {
                    setChildRatio(childRatio, false)
                    lastAnim = null
                }
                interpolator = LinearInterpolator()
                addUpdateListener {
                    val ratio = it.animatedValue as Float
                    getChildParams(viewWidth, viewHeight, ratio)
                }
                start()
            }
        }
    }

    fun setChildRatio(childRatio: Float) {
        setChildRatio(childRatio, true)
    }
}
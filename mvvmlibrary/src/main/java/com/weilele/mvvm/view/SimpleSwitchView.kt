package com.weilele.mvvm.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.weilele.mvvm.R
import com.weilele.mvvm.utils.activity.invisible
import com.weilele.mvvm.utils.activity.visible
import com.weilele.mvvm.widget.BaseFrameLayout

/**
 * 支持切换view的SwipeRefreshLayout
 * 不要直接使用此类，应该使用他的子类
 */
abstract class SimpleSwitchView : SwipeRefreshLayout {
    /**
     * 展示错误信息传入的额外参数
     */
    open class OtherInfo

    /**
     * 展示loading传入的额外参数
     */
    open class LoadingInfo

    /**
     *  展示content传入的额外参数
     */
    open class ContentInfo

    private var _contentView: View? = null
    private var _loadingView: View? = null
    private var _otherView: View? = null

    //是否拦截子view
    var isEnableChildView = true

    val contentView: View?
        get() = _contentView
    val loadingView: View?
        get() = _loadingView
    val otherView: View?
        get() = _otherView

    //是否可以下拉刷新
    var isCanSwipeRefresh = false
        set(value) {
            field = value
            isEnabled = value
        }
    private val contentParentView by lazy {
        object : BaseFrameLayout(context) {
            override fun requestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
                val view = parent?.parent
                if (view == null) {
                    super.requestDisallowInterceptTouchEvent(disallowIntercept)
                } else {
                    view.requestDisallowInterceptTouchEvent(disallowIntercept)
                }
            }
        }
    }

    constructor(context: Context) : super(context)

    //xml中创建
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        context.apply {
            val typedArray = obtainStyledAttributes(attrs, R.styleable.SimpleSwitchView)
            //显示内容的布局
            val contentLayoutRes =
                typedArray.getResourceId(R.styleable.SimpleSwitchView_contentLayout, View.NO_ID)
            if (contentLayoutRes != View.NO_ID) {
                _contentView =
                    LayoutInflater.from(context).inflate(contentLayoutRes, contentParentView, false)
            }
            //loading布局
            val loadingLayoutRes =
                typedArray.getResourceId(R.styleable.SimpleSwitchView_loadingLayout, View.NO_ID)
            if (loadingLayoutRes != View.NO_ID) {
                _loadingView =
                    LayoutInflater.from(context).inflate(loadingLayoutRes, contentParentView, false)
            }
            //其他布局
            val otherLayoutRes =
                typedArray.getResourceId(R.styleable.SimpleSwitchView_otherLayout, View.NO_ID)
            if (otherLayoutRes != View.NO_ID) {
                _otherView =
                    LayoutInflater.from(context).inflate(otherLayoutRes, contentParentView, false)
            }
            isCanSwipeRefresh =
                typedArray.getBoolean(R.styleable.SimpleSwitchView_canSwipeRefresh, false)
            typedArray.recycle()
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        //默认会有一个旋转的imageView
        when (childCount) {
            1 -> {
                //和在代码中创建一样，不作处理
                initSwitchView()
            }
            2 -> {
                val view = getChildAt(1)
                if (view != contentParentView) {
                    _contentView = view
                    //移除这个view，然后添加到viewSwtich
                    removeView(view)
                    initSwitchView()
                }
            }
            else -> {
                throw IllegalArgumentException("最多只能有1个子view")
            }
        }
    }

    //添加子view
    private var isInitView = false

    private fun initSwitchView() {
        if (isInitView) {
            return
        }
        isInitView = true
        isEnabled = isCanSwipeRefresh
        addView(contentParentView, getChildLayoutParams())
        //添加默认的otherView
        val other = otherView ?: getDefaultOtherView(contentParentView)
        hideIfShow(other)
        _otherView = other
        if (other != null && other.parent == null) {
            contentParentView.addView(other, other.layoutParams ?: getChildLayoutParams())
        }
        //添加content
        val content = contentView
        showIfHide(content)
        if (content != null) {
            contentParentView.addView(content)
        }
        //添加默认loading
        val loading = loadingView ?: getDefaultLoadingView(contentParentView)
        _loadingView = loading
        hideIfShow(loading)
        if (loading != null && loading.parent == null) {
            contentParentView.addView(loading, loading.layoutParams ?: getChildLayoutParams())
        }
    }

    /**
     * 替换contentView
     */
    fun replaceContentView(view: View) {
        if (!isInitView) {
            //没有执行过初始化代码
            _contentView = view
            initSwitchView()
            return
        }
        val oldContent = contentView
        _contentView = view
        if (oldContent != null) {
            contentParentView.removeView(oldContent)
        }
        contentParentView.addView(view)
    }

    /**
     * 默认充满父布局
     */
    private fun getChildLayoutParams(): FrameLayout.LayoutParams {
        return FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.MATCH_PARENT
        )
    }


    /**
     * 显示包含的布局view
     */
    open fun showContentView(info: ContentInfo?) {
        isEnableChildView = true
        hideIfShow(otherView)
        hideIfShow(loadingView)
        showIfHide(contentView)
    }

    /**
     * 显示其他view
     */
    open fun showOtherView(info: OtherInfo?) {
        isEnableChildView = true
        hideIfShow(contentView)
        hideIfShow(loadingView)
        showIfHide(otherView)
    }

    /**
     * 显示正在加载的view
     */
    open fun showLoadingView(info: LoadingInfo?) {
        isEnableChildView = false
        hideIfShow(otherView)
        showIfHide(contentView)
        showIfHide(loadingView)
    }

    /**
     * 可以重写这个方法设置动画效果
     */
    open fun hideIfShow(view: View?) {
        view.invisible()
    }

    /**
     * 可以重写这个方法设置动画效果
     */
    open fun showIfHide(view: View?) {
        view.visible()
    }


    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (!isEnableChildView) {
            return true
        }
        return super.onInterceptTouchEvent(ev)
    }


    fun startRefresh() {
        isRefreshing = true
    }

    fun stopRefresh() {
        isRefreshing = false
    }

    /**---------------------------------------------**/

    /**
     * 默认加载数据的view
     */
    abstract fun getDefaultLoadingView(parent: FrameLayout): View?

    /**
     * 默认显示其他view
     */
    abstract fun getDefaultOtherView(parent: FrameLayout): View?
}
package com.weilele.mvvm.view

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewConfiguration
import android.view.ViewParent
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.weilele.mvvm.widget.BaseFrameLayout
import kotlin.math.abs

/**
 * 子view需要一个viewpager2
 * 处理ViewPager滑动冲突
 * 处理viewpager2 上下滑动的时候还可以左右滑动
 * 和[NestedScrollableHost]效果相同，[NestedScrollableHost]为官方写法
 * 建议优先尝试[NestedScrollableCompat]
 */
@Deprecated("建议优先尝试[NestedScrollableCompat]", replaceWith = ReplaceWith("NestedScrollableCompat"))
open class ViewPager2Layout : BaseFrameLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    private var mStartX = 0f
    private var mStartY = 0f
    private var mIsViewPager2Drag = false
    private val mTouchSlop by lazy { ViewConfiguration.get(context).scaledTouchSlop }

    private val viewPager2 by lazy {
        var view: ViewPager2? = null
        repeat(childCount) {
            val child = getChildAt(it)
            if (child is ViewPager2) {
                view = child
            }
        }
        view
    }

    private val parentView by lazy { getParentView(parent) }

    private tailrec fun getParentView(parent: ViewParent?): ViewParent? {
        if (parent == null) {
            return null
        }
        return when (parent) {
            is ViewPager2, is ViewPager -> {
                parent
            }
            is SimpleSwitchView -> {
                parent.parent
            }
            else -> getParentView(parent.parent)
        }
    }

    override fun onInterceptTouchEvent(event: MotionEvent?): Boolean {
        val viewPager2 = this.viewPager2 ?: return super.onInterceptTouchEvent(event)
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                mStartX = event.x
                mStartY = event.y
                parent.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_MOVE -> {
                val endX = event.x
                val endY = event.y
                val distanceX: Float = abs(endX - mStartX)
                val distanceY: Float = abs(endY - mStartY)
                mIsViewPager2Drag =
                    if (viewPager2.orientation == ViewPager2.ORIENTATION_HORIZONTAL) {
                        distanceX > mTouchSlop && distanceX > distanceY
                    } else {
                        distanceY > mTouchSlop && distanceY > distanceX
                    }
                parentView?.requestDisallowInterceptTouchEvent(mIsViewPager2Drag)
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> parent.requestDisallowInterceptTouchEvent(
                false
            )
        }
        return super.onInterceptTouchEvent(event)
    }
}
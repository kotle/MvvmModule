package com.weilele.mvvm.sdk

import android.app.Activity
import com.umeng.analytics.MobclickAgent
import com.umeng.commonsdk.UMConfigure
import com.umeng.commonsdk.statistics.common.DeviceConfig
import com.weilele.mvvm.MvvmConf
import com.weilele.mvvm.app
import com.weilele.mvvm.utils.printStackTrace


object UmengHelper {
    private var isCanUmeng: Boolean = false
    /**
     * 给外部调用
     */
    fun init(appKey: String) {
        isCanUmeng = true
        UMConfigure.setLogEnabled(MvvmConf.isDebug)
        UMConfigure.init(
            app,
            appKey,
            MvvmConf.channel,
            UMConfigure.DEVICE_TYPE_PHONE,
            null
        )
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO)
    }

    /**
     * 针对非activity
     */
    fun onPageStart(pageName: String) {
        if (!isCanUmeng) {
            return
        }
        MobclickAgent.onPageStart(pageName)
    }

    /**
     * 针对非activity
     */
    fun onPageEnd(pageName: String) {
        if (!isCanUmeng) {
            return
        }
        MobclickAgent.onPageEnd(pageName)
    }

    fun onResume(activity: Activity) {
        if (!isCanUmeng) {
            return
        }
        MobclickAgent.onResume(activity)
    }

    fun onPause(activity: Activity) {
        if (!isCanUmeng) {
            return
        }
        MobclickAgent.onPause(activity)
    }


    /**
     * 获取测试设备信息
     */
    private fun getTestDeviceInfo(): String? {
        val context = app
        return try {
            "{\"device_id\":\"${DeviceConfig.getDeviceIdForGeneral(context)}\"," +
                    "\"mac\":\"${DeviceConfig.getMac(context)}\"}"
        } catch (e: Exception) {
            printStackTrace { e }
            null
        }

    }
}
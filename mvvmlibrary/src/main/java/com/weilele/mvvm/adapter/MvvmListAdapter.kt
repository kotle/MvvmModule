package com.weilele.mvvm.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

open class MvvmListAdapter<Data>(
    private val areItemTheSame: ((old: Data?, new: Data?) -> Boolean)? = null/*是否是同一组数据*/,
    private val areContentsTheSame: ((old: Data?, new: Data?) -> Boolean)? = null/*是否内容相同*/
) : ListAdapter<Data, MvvmHolder<Data, *>>(object : DiffUtil.ItemCallback<Data>() {

    override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
        return areItemTheSame?.invoke(oldItem, newItem) ?: (oldItem == newItem)
    }

    override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
        return areContentsTheSame?.invoke(oldItem, newItem) ?: false
    }

}), IMvvmAdapter<Data> by DefaultMvvmAdapterImpl() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MvvmHolder<Data, *> {
        return onMvvmCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: MvvmHolder<Data, *>, position: Int) {
    }

    override fun getCurrentList(): List<Data> {
        return super.getCurrentList()
    }

    override fun onBindViewHolder(
        holder: MvvmHolder<Data, *>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
        onMvvmBindViewHolder(
            holder,
            position,
            payloads,
            itemCount - 1 == position,
            getItem(position)
        )
    }

    override fun getItemViewType(position: Int): Int {
        return getMvvmItemViewType(position, getItem(position))
    }

    override fun onViewAttachedToWindow(holder: MvvmHolder<Data, *>) {
        super.onViewAttachedToWindow(holder)
        onMvvmViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: MvvmHolder<Data, *>) {
        super.onViewDetachedFromWindow(holder)
        onMvvmViewDetachedFromWindow(holder)
    }

    override fun onViewRecycled(holder: MvvmHolder<Data, *>) {
        super.onViewRecycled(holder)
        onMvvmViewRecycled(holder)
    }

    override fun onFailedToRecycleView(holder: MvvmHolder<Data, *>): Boolean {
        onMvvmFailedToRecycleView(holder)
        return super.onFailedToRecycleView(holder)
    }

    override fun getItemData(position: Int): Data? {
        return getItem(position)
    }
}
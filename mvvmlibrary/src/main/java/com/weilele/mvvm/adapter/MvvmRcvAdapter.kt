package com.weilele.mvvm.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

open class MvvmRcvAdapter<Data>(
    private val areItemTheSame: ((old: Data?, new: Data?) -> Boolean)? = null/*是否是同一组数据*/,
    private val areContentsTheSame: ((old: Data?, new: Data?) -> Boolean)? = null/*是否内容相同*/
) : RecyclerView.Adapter<MvvmHolder<Data, *>>(),
    IMvvmAdapter<Data> by DefaultMvvmAdapterImpl() {
    private var datas = mutableListOf<Data>()

    /**
     * 当前对象
     */
    val currentList: List<Data>
        get() = datas

    private val _adapterDiffCall by lazy {
        AdapterDiffCall<Data>(areItemTheSame, areContentsTheSame)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MvvmHolder<Data, *> {
        return onMvvmCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: MvvmHolder<Data, *>, position: Int) {

    }


    override fun onBindViewHolder(
        holder: MvvmHolder<Data, *>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
        onMvvmBindViewHolder(
            holder,
            position,
            payloads,
            position == itemCount - 1,
            getItem(position)
        )
    }

    override fun getItemCount(): Int {
        return datas.count()
    }

    override fun getItemViewType(position: Int): Int {
        return getMvvmItemViewType(position, getItem(position))
    }

    open fun getItem(position: Int): Data? {
        return ignoreError { datas[position] }
    }

    override fun onViewAttachedToWindow(holder: MvvmHolder<Data, *>) {
        super.onViewAttachedToWindow(holder)
        onMvvmViewAttachedToWindow(holder)
    }

    override fun onViewDetachedFromWindow(holder: MvvmHolder<Data, *>) {
        super.onViewDetachedFromWindow(holder)
        onMvvmViewDetachedFromWindow(holder)
    }

    override fun onViewRecycled(holder: MvvmHolder<Data, *>) {
        super.onViewRecycled(holder)
        onMvvmViewRecycled(holder)
    }

    override fun onFailedToRecycleView(holder: MvvmHolder<Data, *>): Boolean {
        onMvvmFailedToRecycleView(holder)
        return super.onFailedToRecycleView(holder)
    }

    fun submitList(
        newList: MutableList<Data>?,
        isReplaceDatas: Boolean,
    ) {
        if (newList == null) {
            val count = itemCount
            datas.clear()
            notifyItemRangeRemoved(0, count)
            return
        }
        val oldData = datas.toMutableList()
        if (isReplaceDatas) {
            onCurrentListChanged(datas, newList)
            datas = newList
        } else {
            datas.clear()
            datas.addAll(newList)
        }
        _adapterDiffCall.old = oldData
        _adapterDiffCall.new = datas
        val result = DiffUtil.calculateDiff(_adapterDiffCall, true)
        result.dispatchUpdatesTo(this)
    }

    open fun onCurrentListChanged(previousList: List<Data>, currentList: List<Data>) {}

    override fun getItemData(position: Int): Data? {
        return getItem(position)
    }

    /**
     * 移除item
     */
    fun removeItem(position: Int, notify: Boolean = true) {
        datas.removeAt(position)
        if (notify) {
            notifyItemRemoved(position)
        }
    }

    /**
     * 移除item
     */
    fun removeItem(startPosition: Int, count: Int, notify: Boolean = true) {
        repeat(count) {
            datas.removeAt(startPosition + it)
        }
        if (notify) {
            notifyItemRangeRemoved(startPosition, count)
        }
    }

    /**
     * 移除item
     */
    fun removeItem(data: Data, notify: Boolean = true) {
        val index = datas.indexOf(data)
        if (index >= 0) {
            removeItem(index, notify)
        }
    }

    /**
     * 移除item
     */
    fun removeItems(data: List<Data>?, notify: Boolean = true) {
        data?.forEach {
            removeItem(it, notify)
        }
    }

    /**
     * 更新item
     */
    fun updateItem(index: Int, data: Data, payload: Any? = null, notify: Boolean = true) {
        datas[index] = data
        if (notify) {
            notifyItemChanged(index, payload)
        }
    }

    /**
     * 添加item
     */
    fun addItem(data: Data, index: Int? = null, notify: Boolean = true) {
        val insertIndex = index ?: datas.count()
        datas.add(insertIndex, data)
        if (notify) {
            notifyItemInserted(insertIndex)
        }
    }

    /**
     * 添加item
     */
    fun addItems(data: List<Data>?, index: Int? = null, notify: Boolean = true) {
        if (data.isNullOrEmpty()) {
            return
        }
        val insertIndex = index ?: datas.count()
        datas.addAll(insertIndex, data)
        if (notify) {
            notifyItemRangeInserted(insertIndex, data.count())
        }
    }
}
package com.weilele.mvvm.adapter

import androidx.recyclerview.widget.DiffUtil

/**
 * 高效刷新
 */
open class AdapterDiffCall<DataType>(
    val areItemSame: ((old: DataType?, new: DataType?) -> Boolean)?,
    val areContentSame: ((old: DataType?, new: DataType?) -> Boolean)?
) : DiffUtil.Callback() {
    var old: MutableList<DataType>? = null
    var new: MutableList<DataType>? = null


    override fun getOldListSize(): Int {
        return old?.count() ?: 0
    }

    override fun getNewListSize(): Int {
        return new?.count() ?: 0
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldData = old?.get(oldItemPosition)
        val newData = new?.get(newItemPosition)
        return areItemSame?.invoke(oldData, newData) ?: (oldData == newData)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areContentSame?.invoke(old?.get(oldItemPosition), new?.get(newItemPosition)) ?: false
    }
}
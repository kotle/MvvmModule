package com.weilele.mvvm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


interface IMvvmAdapter<Data> {
    fun onMvvmCreateViewHolder(parent: ViewGroup, viewType: Int): MvvmHolder<Data, *>

    fun onMvvmBindViewHolder(
        holder: MvvmHolder<Data, *>, position: Int, payloads: MutableList<Any>,
        isScrollEnd: Boolean,
        item: Data?
    )

    fun getMvvmItemViewType(position: Int, item: Data?): Int

    fun onMvvmViewAttachedToWindow(holder: MvvmHolder<Data, *>)

    fun onMvvmViewDetachedFromWindow(holder: MvvmHolder<Data, *>)

    fun onMvvmViewRecycled(holder: MvvmHolder<Data, *>)

    fun onMvvmFailedToRecycleView(holder: MvvmHolder<Data, *>)

    fun <DataType : Data, Holder : MvvmHolder<DataType, *>> register(holderCls: Class<Holder>)

    fun <DataType : Data> register(
        dataCls: Class<DataType>,
        block: Function2<LayoutInflater, ViewGroup, MvvmHolder<DataType, *>>
    )

    fun <DataType : Data, Holder : MvvmHolder<DataType, *>> unRegister(holderCls: Class<Holder>)

    fun setOnHolderClickListener(
        clickId: Int? = null,
        listener: ((holder: MvvmHolder<Data, *>, clickView: View) -> Unit)
    )

    fun setOnHolderLongClickListener(
        clickId: Int? = null,
        listener: ((holder: MvvmHolder<Data, *>, clickView: View) -> Unit)
    )

    fun setOnScrollEndListener(l: Function1<Int, Unit>?)

    fun getItemData(position: Int): Data?
}
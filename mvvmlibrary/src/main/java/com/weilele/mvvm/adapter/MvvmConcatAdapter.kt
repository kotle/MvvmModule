package com.weilele.mvvm.adapter

import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView

fun mvvmConcatAdapter(
    vararg adapters: RecyclerView.Adapter<out RecyclerView.ViewHolder>,
    config: ConcatAdapter.Config = ConcatAdapter.Config.DEFAULT,
) = ConcatAdapter(
    config,
    *adapters
)
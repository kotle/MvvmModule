package com.weilele

import android.app.Application
import android.content.Context
import androidx.startup.Initializer
import com.weilele.mvvm.initMvvmLib

/**
 * 初始化Mvvm库
 */
class MvvmInit : Initializer<Application> {
    override fun create(context: Context): Application {
        val app = context as Application
        initMvvmLib(app)
        return app
    }

    override fun dependencies(): List<Class<out Initializer<*>>> {
        return emptyList()
    }
}
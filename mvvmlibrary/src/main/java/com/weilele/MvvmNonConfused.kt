package com.weilele

/**
 * 实现此接口的类所有内容（类名，类里面的变量，函数等）将不会混淆
 *
 */
interface MvvmNonConfused {
}